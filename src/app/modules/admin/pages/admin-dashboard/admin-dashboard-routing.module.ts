import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkingPageGuard } from 'src/app/core/guards/working-page.guard';

import { QrScannerComponent } from 'src/app/shared/components';
import { HomeAdminComponent } from '../../components/home-admin/home-admin.component';
import { RegisterSectionComponent } from '../../components/register-section/register-section.component';
import {
  AdminDashboardComponent
} from './admin-dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: AdminDashboardComponent,
    canActivateChild: [WorkingPageGuard],
    children: [
      { path: 'home', component: HomeAdminComponent },
      { path: 'registros', component: RegisterSectionComponent },
      {
        path: 'bus', loadChildren: () => import('./bus/bus.module')
        .then(m => m.BusModule)
      },
      {
        path: 'ruta', loadChildren: () => import('./travel-route/travel-route.module')
        .then(m => m.TravelRouteModule)
      },
      {
        path: 'banco', loadChildren: () => import('./bank/bank.module')
        .then(m => m.BankModule)
      },
      {
        path: 'boletero', loadChildren: () => import('./ticket-holder/ticket-holder.module')
          .then(m => m.TicketHolderModule)
      },
      {
        path: 'chofer', loadChildren: () => import('./driver/driver.module')
          .then(m => m.DriverModule)
      },
      {
        path: 'parada', loadChildren: () => import('./bus-stop/bus-stop.module')
          .then(m => m.BusStopModule)
      },
      {
        path: 'salida', loadChildren: () => import('./departure/departure.module')
          .then(m => m.DepartureModule)
      },
      {
        path: 'venta', loadChildren: () => import('./sales/sales.module')
          .then(m => m.SalesModule)
      },
      {
        path: 'empresa', loadChildren: () => import('./enterprise/enterprise.module')
        .then(m => m.EnterpriseModule)
      },
      {
        path: 'administrador-oficina', loadChildren: () => import('./office-admin/office-admin.module')
        .then(m => m.OfficeAdminModule)
      },
      {
        path: 'oficina', loadChildren: () => import('./office/office.module')
          .then(m => m.OfficeModule)
      },
      {
        path: 'plantilla-bus', loadChildren: () => import('./bus-template/bus-template.module')
        .then(m => m.BusTemplateModule)
      },
      {
        path: 'configuracion', loadChildren: () => import('./ovj-company-settings/ovj-company-settings.module')
        .then(m => m.OvjCompanySettingsModule)
      },
      {
        path: 'configuracion-empresarial', loadChildren: () => import('./company-settings/company-settings.module')
        .then(m => m.CompanySettingsModule)
      },
      {
        path: 'reporte-ventas', loadChildren: () => import('./ticket-sales-report/ticket-sales-report.module')
        .then(m => m.TicketSalesReportModule)
      },
      {
        path: 'reporte-salidas', loadChildren: () => import('./departure-sales-report/departure-sales-report.module')
        .then(m => m.DepartureSalesReportModule)
      },
      {
        path: 'perfil', loadChildren: () => import('./profile/profile.module')
          .then(m => m.ProfileModule)
      },
      {
        path: 'qr-scanner', component: QrScannerComponent
      },
      {
        path: 'profile-company', loadChildren: () => import('./profile-component/profile-component.module')
        .then(m => m.ProfileComponentModule)
      },
      {
        path: 'encomienda', loadChildren: () => import('./assignment/assignment.module')
        .then(m => m.AssignmentModule)
      },
      {
        path: 'pago-de-servicios', loadChildren: () => import('./service-payment/service-payment.module')
        .then(m => m.servicePaymentModule)
      },
      {
        path: 'estado-cobros', loadChildren: () => import('./client-collection/client-collection.module')
        .then(m => m.ClientCollectionModule)
      }
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AdminDashboardRoutingModule { }
