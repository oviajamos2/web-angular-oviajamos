import { Component, OnInit, ViewChild } from '@angular/core';
import { Departure, DepartureSearchParams } from 'src/app/core/http/departure';
import { TicketSaleService } from 'src/app/core/services/ticket-sale.service';
import { DepartureSalesFilterComponent } from 'src/app/shared/components';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: []
})
export class SalesComponent implements OnInit {

  @ViewChild(DepartureSalesFilterComponent) departureSalesFilter: DepartureSalesFilterComponent;

  destinationValueSubs: Subscription;

  get departure(): Departure {
    return this.ticketSaleService.selectedDeparture;
  }

  get departures(): Departure[] {
    return this.ticketSaleService.filteredDepartures;
  }

  get selectedTickets(): number[] {
    return this.ticketSaleService.selectedTickets;
  }

  get initialized(): boolean {
    return this.ticketSaleService.initialized;
  }

  get loadingDepartures(): boolean {
    return this.ticketSaleService.loadingDepartures;
  }

  get departureSearchParams(): DepartureSearchParams {
    return this.ticketSaleService.searchParams;
  }

  constructor(
    private ticketSaleService: TicketSaleService
  ) {}

  ngOnInit(): void {
    this.initDestinationValueSubs();
  }

  ngOnDestroy(): void {
    this.destinationValueSubs?.unsubscribe();
  }

  initDestinationValueSubs() {
    this.destinationValueSubs = this.ticketSaleService.newDeparture$.subscribe( (value: any) => {
      if(value.busStopId)
        this.departureSalesFilter.travelForm.destination.setValue(value.busStopId);
    });
  }

  filterDeparturesEvent(event) {
    this.ticketSaleService.loadDepartures(event);
  }

  selectDepartureEvent(event: Departure) {
    this.ticketSaleService.setNewDeparture(event);
  }
}