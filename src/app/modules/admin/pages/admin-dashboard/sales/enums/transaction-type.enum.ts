export enum TransactionTypeEnum {
  INVOICE = 'Executing Invoice',
  QUOTE = 'Executing Quote',
  RESERVE = 'Executing Reserve'
}