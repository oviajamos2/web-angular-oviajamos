import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesComponent } from './sales.component';
import { FilteredDeparturesComponent } from './pages/filtered-departures/filtered-departures.component';
import { SeatReservationComponent } from './pages/seat-reservation/seat-reservation.component'
import { SalesRoutingModule } from './sales-routing.module ';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from "../../../../../core/pipes/pipes.module";

@NgModule({
  declarations: [
    SalesComponent,
    FilteredDeparturesComponent,
    SeatReservationComponent
  ],
  imports: [
    CommonModule,
    SalesRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class SalesModule { }
