import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketHolderComponent } from './ticket-holder.component'

// Pages
import {
  TicketHoldersComponent,
  TicketHolderCreateComponent,
  TicketHolderEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'boleteros', pathMatch: 'full' },
  {
    path: '',
    component: TicketHolderComponent,
    children: [
      { path: 'boleteros', component: TicketHoldersComponent },
      { path: 'create', component: TicketHolderCreateComponent },
      { path: 'edit/:id', component: TicketHolderEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TicketHolderRoutingModule { }
