import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { ConfirmationComponent } from 'src/app/shared/modals/confirmation/confirmation.component';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { TicketHolder } from 'src/app/core/http/ticket-holder/models/ticket-holder.model';
import { DELETED_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-ticket-holders',
  templateUrl: './ticket-holders.component.html',
  styleUrls: ['./ticket-holders.component.scss'],
  host: {
    '(window:click)': 'onWindowClick($event)'
  }
})
export class TicketHoldersComponent implements OnInit {
  
  colsTable = [
    { name: 'Nombre y Apellido', orderBy: true, extraImg: 'assets/icons/user-image-icon-3.svg' },
    { name: 'Celular', orderBy: false },
    { name: 'Dirección', orderBy: false }
  ];

  tHolders: TicketHolder[] = [
    {
      id: 1,
      photo: 'assets/images/people-example-2.svg',
      names: 'Armando',
      secondNames: 'Almanza',
      docType: 'ci',
      docNumber: '1234567',
      countryPhoneCode: '+591',
      cellphone: 77777777,
      address: 'Calle Luis Paz Arce, Sucre',
      showOptions: false,
      showDetail: false,
      showMoreDetail:false,
      contacts: [
        {
          id: 1,
          photo: 'assets/icons/person-circle-1.svg',
          names: 'Armando Almanza 1',
          address: 'Calle Luis Paz Arce, Sucre',
          countryPhoneCode: '+591',
          cellphone: 77777777
        },
        {
          id: 2,
          photo: 'assets/icons/person-circle-1.svg',
          names: 'Armando Almanza 2',
          address: 'Calle Luis Paz Arce, Sucre',
          countryPhoneCode: '+591',
          cellphone: 77777777
        },
        {
          id: 3,
          photo: 'assets/icons/person-circle-1.svg',
          names: 'Armando Almanza 3',
          address: 'Calle Luis Paz Arce, Sucre',
          countryPhoneCode: '+591',
          cellphone: 77777777
        },
        {
          id: 4,
          photo: 'assets/icons/person-circle-1.svg',
          names: 'Armando Almanza 4',
          address: 'Calle Luis Paz Arce, Sucre',
          countryPhoneCode: '+591',
          cellphone: 77777777
        }
      ]
    },
    {
      id: 2,
      photo: 'assets/images/people-example-1.svg',
      names: 'Mario',
      secondNames: 'Barrionuevo',
      docType: 'pasaporte',
      docNumber: '7654321',
      countryPhoneCode: '+591',
      cellphone: 77777777,
      address: 'Obispo Anaya #224 esq. Néstor Galindo, Cochabamba',
      showDetail: false,
      showMoreDetail:false,
      contacts: [
        {
          id: 5,
          photo: 'assets/icons/person-circle-1.svg',
          names: 'Armando Almanza',
          address: 'Calle Luis Paz Arce, Sucre',
          countryPhoneCode: '+591',
          cellphone: 77777777
        },
        {
          id: 6,
          photo: 'assets/icons/person-circle-1.svg',
          names: 'Armando Almanza',
          address: 'Calle Luis Paz Arce, Sucre',
          countryPhoneCode: '+591',
          cellphone: 77777777
        }
      ]
    },
    {
      id: 3,
      photo: 'assets/images/people-example-1.svg',
      names: 'Sergio',
      secondNames: 'Zapata',
      docType: 'ci',
      docNumber: '9876543',
      countryPhoneCode: '+591',
      cellphone: 77777777,
      address: 'Obispo Anaya #224 esq. Néstor Galindo, Cochabamba',
      showDetail: false,
      showMoreDetail:false,
      contacts: []
    }
  ];

  modalRef: NgbModalRef;
  loadingTicketHolders = true;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {}

  onWindowClick(){
    this.tHolders.forEach( tHolder => {
      tHolder.showOptions = false;
    });
  }

  public selectUser(index: number) {}

  public addTicketHolder(event: boolean) {
    this.router.navigateByUrl('/dashboard/boletero/create');
  }

  public editTicketHolder(tHolder: TicketHolder) {
    this.onWindowClick();
    this.router.navigateByUrl(`/dashboard/boletero/edit/${ tHolder.id }`, {state: { tHolder } });
  }

  public deleteTicketHolder( tHolder: TicketHolder ) {
    this.onWindowClick();
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Esta seguro que desea eliminar al vendedor ${tHolder.names} ${tHolder.secondNames}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.notificationService.notify({
            ...DELETED_NOTIFICATION,
            objectType: 'ticketHolder',
            objectId: tHolder.id,
          });
        }
      });
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showTHolderOptions(index: number) {
    for(let i=0 ; i<this.tHolders.length ; i++) {
      if(i != index) this.tHolders[i].showOptions = false;
    }
    this.tHolders[index].showOptions = !this.tHolders[index].showOptions;
  }

}
