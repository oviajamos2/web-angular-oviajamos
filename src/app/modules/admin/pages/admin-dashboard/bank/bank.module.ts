import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from 'src/app/shared/shared.module';
import { BankRoutingModule } from './bank-routing.module';
import { BankComponent } from './bank.component';

import {
  BankCreateComponent,
  BankEditComponent,
  BanksComponent
} from './pages';

@NgModule({
  declarations: [
    BankComponent,
    BankCreateComponent,
    BankEditComponent,
    BanksComponent
  ],
  imports: [
    CommonModule,
    BankRoutingModule,
    SharedModule,
    NgbModule,
    PipesModule
  ]
})
export class BankModule { }
