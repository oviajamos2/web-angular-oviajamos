import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { BankFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { Bank, BankService } from 'src/app/core/http/bank';

@Component({
  selector: 'app-bank-create',
  templateUrl: './bank-create.component.html',
  styleUrls: []
})
export class BankCreateComponent implements OnInit {

  @ViewChild(BankFormComponent) bankForm: BankFormComponent;

  burgerButton: boolean;
  submitting: boolean;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private bankService: BankService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {}

  public async onSubmit() {
    let bank: Bank;
    await this.bankForm.onSubmit().then((response: Bank) => {
      bank = response;
    });
    if( bank ) {
      this.submitting = true;
      this.bankForm.bankGroup.disable();
      this.bankService.create(bank).pipe(
        take(1)
      ).subscribe( response => {
        this.submitting = false;
        this.bankForm.bankGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl("/dashboard/banco/bancos")
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.bankForm.bankGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    }
  }

  public onDiscard() {
    this.router.navigateByUrl("/dashboard/banco")
  }
}
