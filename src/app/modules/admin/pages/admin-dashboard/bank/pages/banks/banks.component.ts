import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Bank, BankService } from 'src/app/core/http/bank';
import { ConfirmationComponent} from 'src/app/shared/modals/index';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse, PaginationMeta } from 'src/app/core/services/base-service/pagination-response.interface';
import { BankInfoComponent } from 'src/app/shared/modals/mobile';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.scss']
})
export class BanksComponent implements OnInit, OnDestroy {

  colsTable = [
    { name: 'Nombre de banco', orderBy: true}
  ];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredBankSubs: Subscription;
  banks: Bank[] = [];
  selectedBank: Bank;
  showButtom: boolean = true;
  loadingBanks= true;
  
  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private bankService: BankService,
  ) { }

  ngOnInit(): void {
    this.loadBanks();
    this.subscribeToRecoveredBanks();
  }

  ngOnDestroy(): void {
    this.recoveredBankSubs?.unsubscribe();
  }

  public addBank() {
    this.router.navigateByUrl('/dashboard/banco/create');
  }

  editBank(bank: Bank) {
    this.router.navigateByUrl(`/dashboard/banco/edit/${ bank.bankId }`, {state: { bank } });
  }

  deleteBank(bank: Bank) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Esta seguro en eliminar el banco ${bank.name}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent );
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(bank.bankId);
          this.bankService.delete( bank.bankId).pipe(take(1)).subscribe( response => {
            this.selectedBank = null;
            this.removeFromArray(bank.bankId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bank.bankId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'bank',
              objectId: bank.bankId
            });
            if(this.banks.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadBanks(this.paginationMeta.currentPage - 1);
            } else {
              this.loadBanks(this.paginationMeta.currentPage);
            }
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bank.bankId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
        }
      });
  }

  removeFromArray(bankId: number) {
    this.banks.splice(
      this.banks.findIndex((bankInArray: Bank) => bankId === bankInArray.bankId), 1
    );
  }

  loadBanks(page?: number) {
    this.loadingBanks = true;
    this.bankService.getAll({ page })
      .pipe(take(1)).subscribe( (response: PaginationResponse<Bank>) => {
      if(this.paginationMeta?.currentPage !== response.meta.currentPage) this.selectedBank = null;
      this.loadingBanks = false;
      this.paginationMeta = response.meta;
      this.banks = response.items;
      this.banks = this.banks.reverse();
    }, (error) => {
      console.warn(error);
      this.loadingBanks= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadBanks(event);
  }

  subscribeToRecoveredBanks() {
    this.recoveredBankSubs = this.bankService.recoveredBank$.subscribe( response => {
      this.loadBanks(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showBankDetail(index: number) {
    let actualvalue = this.banks[index].showDetail;
    this.banks[index].showDetail = !actualvalue;
  }

  selectBank(bank: Bank) {
    this.selectedBank = bank;
  }

  showDetail(bank: Bank) {
    let modalData: ModalData = {
      state: {
        bank
      }
    }
    this.modalRef = this.modalService.open(BankInfoComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let bankInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Bank, number]) => {
        bankInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteBank(result[0]);
            break;
          case 2:
            this.editBank(result[0]);
            break;
        }
    });
  }
}