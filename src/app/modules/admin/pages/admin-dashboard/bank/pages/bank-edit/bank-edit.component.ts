import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Bank, BankService } from 'src/app/core/http/bank';
import { BankFormComponent } from 'src/app/shared/forms';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bank-edit',
  templateUrl: './bank-edit.component.html',
  styleUrls: []
})
export class BankEditComponent implements OnInit {

  @ViewChild(BankFormComponent) bankForm: BankFormComponent;

  loadingBank: boolean = true;
  modalRef: NgbModalRef;
  submitting: boolean;
  public bank: Bank;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private bankService: BankService,
  ) { }

  ngOnInit(): void {
    if(history.state.bank) {
      this.bank = history.state.bank;
      this.loadBank();
    } else {
      this.router.navigateByUrl('/dashboard/banco');
    }
  }

  async loadBank(): Promise<void> {
    this.bankService.get( this.bank.bankId ).pipe(take(1)).subscribe( (response: Bank) => {
      this.bank = response;
      this.loadingBank = false;
      return;
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      this.router.navigateByUrl('/dashboard/banco');
      return;
    });
  }

  public async onSubmit() {
    let bank: Bank;
    await this.bankForm.onSubmit().then((response: Bank) => {
      bank = response;
    });
    if( bank ) {
      this.submitting = true;
      this.bankForm.bankGroup.disable();
      this.bankService.patch(bank.bankId, bank).pipe(take(1)).subscribe( response  => {
        this.submitting = false;
        this.bankForm.bankGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/banco/bancos');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.bankForm.bankGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }) ;
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
      discardModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.onSubmit();
      } else {
        this.router.navigateByUrl('/dashboard/banco/bancos');
      }
    });
  }
}
