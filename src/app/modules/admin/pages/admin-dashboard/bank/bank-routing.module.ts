import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankComponent } from './bank.component'

import {
  BankCreateComponent,
  BankEditComponent,
  BanksComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'bancos', pathMatch: 'full' },
  {
    path: '',
    component: BankComponent,
    children: [
      { path: 'bancos', component: BanksComponent },
      { path: 'create', component: BankCreateComponent },
      { path: 'edit/:id', component: BankEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BankRoutingModule { }
