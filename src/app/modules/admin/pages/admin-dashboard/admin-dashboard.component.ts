import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { NotifierComponent } from 'src/app/shared/components';
import { NotificationData } from 'src/app/core/services/notification/notificationData.interface';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { User } from 'src/app/core/http/user';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: [
    './admin-dashboard.component.scss'
  ]
})
export class AdminDashboardComponent implements OnInit, OnDestroy {

  @ViewChild(NotifierComponent) notifier: NotifierComponent;
  
  public notificationsSubs: Subscription;

  get loggedUser(): User {
    return this.authService.loggedUser;
  }

  constructor(
    private router:Router,
    private notificationService: NotificationService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.initNotificationsSubs();
  }

  ngOnDestroy(): void {
    this.notificationsSubs?.unsubscribe();
  }

  public burgerEvent(event:boolean) {
    this.toggleSideBar();
  }

  navigateToSection( url:string ) {
    this.router.navigateByUrl(url);
  }

  toggleSideBar() {
    let adminMainContainer = document.querySelector( '.admin-main-container' );
    if( adminMainContainer.classList.contains( 'toggled' ) ) {
      adminMainContainer.classList.remove( 'toggled' );
    } else {
      adminMainContainer.classList.add( 'toggled' );
    }
  }

  initNotificationsSubs() {
    this.notificationsSubs = this.notificationService.notifications$.subscribe(
    (notificationData: NotificationData) => {
      this.notifier.notify(notificationData);
    });
  }
}
