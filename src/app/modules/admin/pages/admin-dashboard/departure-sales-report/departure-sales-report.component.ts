import { Component, OnInit } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';

@Component({
  selector: 'app-departure',
  templateUrl: './departure-sales-report.component.html',
  styleUrls: []
})
export class DepartureSalesReportComponent implements OnInit {

  seeingDetail: boolean;
  departure: Departure;

  constructor() { }

  ngOnInit(): void {}

  seeDetail(event: Departure) {
    this.departure = event;
    this.seeingDetail = true;
  }

  backToDepartures(event) {
    this.departure = null;
    this.seeingDetail = false;
  }
}