import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { DepartureSalesReportComponent } from './departure-sales-report.component';
import { DepartureSalesReportRoutingModule } from './departure-sales-report-routing.module';

import {
  ClientListReportComponent,
  DepartureListReportComponent
} from './pages';

@NgModule({
  declarations: [
    DepartureSalesReportComponent,
    ClientListReportComponent,
    DepartureListReportComponent
  ],
  imports: [
    CommonModule,
    DepartureSalesReportRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class DepartureSalesReportModule { }
