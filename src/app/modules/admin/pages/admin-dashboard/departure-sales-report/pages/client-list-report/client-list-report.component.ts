import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Departure } from 'src/app/core/http/departure';

@Component({
  selector: 'app-client-list-report',
  templateUrl: './client-list-report.component.html',
  styleUrls: ['./client-list-report.component.scss']
})
export class ClientListReportComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Output('backToDeparturesEvent') backToDeparturesEvent = new EventEmitter<boolean>();

  ShowAddDepartureButton: boolean = false;
  colsTable = [
    { name: 'Pasajes', orderBy: true },
    { name: 'Cliente', orderBy: false },
    { name: 'Origen', orderBy: false },
    { name: 'Destino', orderBy: false },
    { name: 'Total', orderBy: false }
  ];
  paymentMethods: any[] = [1, 2, 3, 4, 5, 6, 7];
  recoveredDeparture: Subscription;
  pageActual = 1;
  total: number;

  constructor(
    private router: Router,
  ) {}

  ngOnInit(): void {}

  addDeparture() {
    this.router.navigateByUrl('/dashboard/salida/create');
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  backToDepartures() {
    this.backToDeparturesEvent.emit(true);
  }

  print(){}
}