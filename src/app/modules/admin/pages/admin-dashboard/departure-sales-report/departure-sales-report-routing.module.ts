import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DepartureSalesReportComponent } from './departure-sales-report.component';

const routes: Routes = [
  {
    path: '',
    component: DepartureSalesReportComponent
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DepartureSalesReportRoutingModule { }
