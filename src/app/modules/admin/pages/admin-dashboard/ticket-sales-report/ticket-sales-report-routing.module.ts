import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketSalesReportComponent } from './ticket-sales-report.component';

const routes: Routes = [
  {
    path: '',
    component: TicketSalesReportComponent
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TicketSalesReportRoutingModule { }
