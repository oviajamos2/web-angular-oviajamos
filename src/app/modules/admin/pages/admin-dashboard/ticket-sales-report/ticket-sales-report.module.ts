import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { TicketSalesReportComponent } from './ticket-sales-report.component';
import { TicketSalesReportRoutingModule } from './ticket-sales-report-routing.module';

import {
  OnlineSalesComponent,
  PhysicalSalesComponent,
  TotalSalesComponent
} from './pages';

@NgModule({
  declarations: [
    TicketSalesReportComponent,
    OnlineSalesComponent,
    PhysicalSalesComponent,
    TotalSalesComponent
  ],
  imports: [
    CommonModule,
    TicketSalesReportRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class TicketSalesReportModule { }
