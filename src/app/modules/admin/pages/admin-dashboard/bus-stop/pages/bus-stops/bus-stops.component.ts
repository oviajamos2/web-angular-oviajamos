import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { ConfirmationComponent } from 'src/app/shared/modals/confirmation/confirmation.component';
import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import {
  SERVER_ERROR_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  DELETED_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-stops',
  templateUrl: './bus-stops.component.html',
  styleUrls: ['./bus-stops.component.scss']
})
export class BusStopsComponent implements OnInit, OnDestroy {

  colsTable = [
    {name: 'Nombre de Parada', orderBy: true, extraImg: 'person'},
    {name: 'Departamento', orderBy: false},
    {name: 'Dirección', orderBy: false}
  ];
  bStops: BusStop[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredBusStopSubs: Subscription;
  loadingBusStops = true;

  constructor(
    private busStopService: BusStopService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit(): void {
    this.loadBusStops();
    this.subscribeToRecoveredBusStops();
  }

  ngOnDestroy() {
    this.recoveredBusStopSubs.unsubscribe();
  }

  loadBusStops(page?: number) {
    this.loadingBusStops = true;
    this.busStopService.getAll({ page }).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.loadingBusStops = false;
      this.paginationMeta = response.meta;
      this.bStops = response.items;
    },(error) => {
      console.warn(error);
      this.loadingBusStops= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });

  }

  pageChange(event: number) {
    this.loadBusStops(event);
  }

  subscribeToRecoveredBusStops() {
    this.recoveredBusStopSubs = this.busStopService.recoveredBusStop$.subscribe(response => {
      this.loadBusStops(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  public addBusStop(event: boolean) {
    this.router.navigateByUrl('/dashboard/parada/create');
  }

  public editBusStop(bStop: BusStop) {
    this.router.navigateByUrl(`/dashboard/parada/edit/${bStop.busStopId}`, {state: {bStop}});
  }

  public deleteBusStop(bStop: BusStop) {
    const modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar la parada ${bStop.name}.`,
        type: 'delete'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(bStop.busStopId);
          this.busStopService.delete( bStop.busStopId).pipe(take(1)).subscribe( response => {
            this.removeFromArray(bStop.busStopId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bStop.busStopId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'busStop',
              objectId: bStop.busStopId
            });
            if(this.bStops.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadBusStops(this.paginationMeta.currentPage - 1);
            } else {
              this.loadBusStops(this.paginationMeta.currentPage);
            }
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bStop.busStopId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
        }
      });
  }

  removeFromArray(busStopId: number) {
    this.bStops.splice(
      this.bStops.findIndex((busStopInArray: BusStop) => busStopId === busStopInArray.busStopId), 1
    );
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }
}
