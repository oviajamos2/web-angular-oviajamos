import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { BusStopRoutingModule } from './bus-stop-routing.module';
import { NgbButtonsModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BusStopComponent } from './bus-stop.component';

import {
  BusStopsComponent,
  BusStopCreateComponent,
  BusStopEditComponent,
} from './pages';

@NgModule({
  declarations: [
    BusStopComponent,
    BusStopsComponent,
    BusStopCreateComponent,
    BusStopEditComponent
  ],
  imports: [
    CommonModule,
    BusStopRoutingModule,
    SharedModule,
    NgbButtonsModule,
    NgbModule,
    PipesModule
  ]
})
export class BusStopModule { }
