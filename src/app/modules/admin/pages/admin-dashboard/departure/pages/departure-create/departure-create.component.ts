import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { DepartureFormComponent } from 'src/app/shared/forms';
import { DepartureService } from 'src/app/core/http/departure/departure.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-departure-create',
  templateUrl: './departure-create.component.html',
  styleUrls: []
})
export class DepartureCreateComponent implements OnInit {

  @ViewChild(DepartureFormComponent) departureForm: DepartureFormComponent;

  submitting: boolean;
  type: string;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private notificationService: NotificationService,
    private departureService: DepartureService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    if(history.state.type) {
      this.type = history.state.type;
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/salida/salidas');
  }

  notifySuccessSave() {
    this.notificationService.notify(SAVED_NOTIFICATION);
  }

  onSubmit() {
    const departureForm = this.departureForm.onSubmit();
    if (departureForm) {
      this.submitting = true;
      this.departureForm.departureGroup.disable();
      this.departureService.create(departureForm).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.departureForm.departureGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/salida/salidas');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.departureForm.departureGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    }
  }
}