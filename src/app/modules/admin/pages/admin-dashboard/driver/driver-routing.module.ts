import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverComponent } from './driver.component'

// Pages
import {
  DriversComponent,
  DriverCreateComponent,
  DriverEditComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'choferes', pathMatch: 'full' },
  {
    path: '',
    component: DriverComponent,
    children: [
      { path: 'choferes', component: DriversComponent },
      { path: 'create', component: DriverCreateComponent },
      { path: 'edit/:id', component: DriverEditComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DriverRoutingModule { }
