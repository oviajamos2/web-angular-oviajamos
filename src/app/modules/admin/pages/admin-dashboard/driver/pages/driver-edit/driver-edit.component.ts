import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { Driver, DriverService } from 'src/app/core/http/driver';
import { DriverFormComponent } from 'src/app/shared/forms';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-driver-edit',
  templateUrl: './driver-edit.component.html',
  styleUrls: []
})
export class DriverEditComponent implements OnInit {

  @ViewChild(DriverFormComponent) driverForm: DriverFormComponent;

  public driver: Driver;
  loadingDriver: boolean = true;
  modalRef: NgbModalRef;
  submitting: boolean;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private driverService: DriverService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if(history.state.driver) {
      this.driver = history.state.driver;
      this.loadDriver();
    } else {
      this.router.navigateByUrl('/dashboard/chofer');
    }
  }

  async loadDriver(): Promise<void> {
    this.driverService.get( this.driver.driverId ).pipe(take(1)).subscribe( (response: Driver) => {
      this.driver = response;
      this.loadingDriver = false;
      return;
    }, (error) => {
      this.router.navigateByUrl('/dashboard/chofer');
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      return;
    });
  }

  public async onSubmit() {
    this.submitting = true;
    this.driverForm.driverGroup.disable();
    let driver: Driver;
    await this.driverForm.onSubmit().then( driverForm => {
      driver = driverForm;
    });
    if( driver ) {
      this.driverService.patch(driver.driverId, driver).pipe(take(1)).subscribe( response  => {
        this.submitting = false;
        this.driverForm.enableDriverForm();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/chofer/choferes');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.driverForm.enableDriverForm();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.driverForm.enableDriverForm();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let discardModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        discardModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard/chofer/choferes');
        }
      })
  }
}

