import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Shared
import { SharedModule } from 'src/app/shared/shared.module';
import { DriverRoutingModule } from './driver-routing.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { DriverComponent } from './driver.component';

// Pages
import {
  DriversComponent,
  DriverCreateComponent,
  DriverEditComponent,
} from './pages';

@NgModule({
  declarations: [
    DriverComponent,
    DriversComponent,
    DriverCreateComponent,
    DriverEditComponent
  ],
  imports: [
    CommonModule,
    DriverRoutingModule,
    SharedModule,
    NgbModule,
    PipesModule
  ]
})
export class DriverModule { }
