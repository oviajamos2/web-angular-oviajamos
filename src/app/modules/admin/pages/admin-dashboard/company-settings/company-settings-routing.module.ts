import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanySettingsComponent } from './company-settings.component';

import {
  DosageComponent,
  SettingsComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'oficina', pathMatch: 'full' },
  {
    path: '',
    component: CompanySettingsComponent,
    children: [
      { path: 'oficina', component: SettingsComponent },
      { path: 'dosificacion', component: DosageComponent },
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CompanySettingsRoutingModule { }
