import { Component, OnInit } from '@angular/core';
import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { take } from 'rxjs/operators';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { ModalService } from 'src/app/core/services';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { Subscription } from 'rxjs';
import { Office, OfficeService } from 'src/app/core/http/office';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { OfficeDataFormModal } from 'src/app/shared/modals';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-dosage',
  templateUrl: './dosage.component.html',
  styleUrls: ['./dosage.component.scss']
})
export class DosageComponent implements OnInit {

  colsTable = [
    { name: 'Nombre', orderBy: true },
    { name: 'Celular', orderBy: false },
  ];
  loadingCompanies: boolean;
  modalRef: NgbModalRef;
  offices: Office[] = [];
  pageActual = 1;

  get company(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private enterpriseService: EnterpriseService,
    private modalService: ModalService,
    private officeService: OfficeService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadOffices();
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  loadOffices() {
    this.enterpriseService.get(this.company.companyId).pipe(take(1)).subscribe( (enterprise: Enterprise) => {
      this.offices = enterprise.offices;
    }, (error) => {
      console.warn(error);
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  editOfficeData(event: Office) {
    let modalData: ModalData = {
      state: {
        office: event,
        enterprise: this.company
      }
    };
    this.modalRef = this.modalService.open(OfficeDataFormModal, 'modal-lg' );
    this.modalRef.componentInstance.setValuesLikeModal(modalData);
    let editOfficeModalSubs: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Office, number]) => {
        if(result) {
          if(result[1] === 1) {
            this.officeService.create(result[0]).pipe(take(1)).subscribe( (response: Office) => {
              this.modalRef.componentInstance.submitting$.next(false);
              this.loadOffices();
              editOfficeModalSubs.unsubscribe();
              this.modalRef.close();
              this.notificationService.notify(SAVED_NOTIFICATION);
            }, (error) => {
              this.modalRef.componentInstance.submitting$.next(false);
              console.warn( error );
              this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
            });
          } else if(result[1] === 2) {
            this.officeService.patch(result[0].officeId, result[0]).pipe(take(1)).subscribe( (response: Office) => {
              this.modalRef.componentInstance.submitting$.next(false);
              this.loadOffices();
              editOfficeModalSubs.unsubscribe();
              this.modalRef.close();
              this.notificationService.notify(SAVED_NOTIFICATION);
            }, (error) => {
              this.modalRef.componentInstance.submitting$.next(false);
              console.warn( error );
              this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
            });
          }
        } else {
          editOfficeModalSubs.unsubscribe();
          this.modalRef.close();
        }
      });
  }

  checkTimeLimit(office: Office): string {
    if(!office.dosage) return '';
    let dosageDate = moment(office.dosage.emissionTimeLimit);
    let today = moment();
    let remainingDays: number = dosageDate.diff(today, 'days') + 1;
    let response = '';
    if(remainingDays <= 7) {
      response = 'danger';
    } else if(remainingDays > 7 && remainingDays <= 14) {
      response = 'warning';
    }
    return response;
  }
}