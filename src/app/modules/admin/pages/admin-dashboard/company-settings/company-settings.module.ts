import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { CompanySettingsRoutingModule } from './company-settings-routing.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { CompanySettingsComponent } from './company-settings.component';

import {
  DosageComponent,
  SettingsComponent
} from './pages';

@NgModule({
  declarations: [
    CompanySettingsComponent,
    SettingsComponent,
    DosageComponent
  ],
  imports: [
    CommonModule,
    CompanySettingsRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class CompanySettingsModule { }
