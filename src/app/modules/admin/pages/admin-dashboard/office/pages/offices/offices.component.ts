import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals/confirmation/confirmation.component';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData} from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office, OfficeService } from 'src/app/core/http/office';
import { OfficeInfoModalComponent } from 'src/app/shared/modals/mobile';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-offices',
  templateUrl: './offices.component.html',
  styleUrls: ['./offices.component.scss']
})
export class OfficesComponent implements OnInit, OnDestroy {

  colsTable = [
    {name: 'Nombre de Oficina', orderBy: true, extraImg: 'assets/icons/bus-outline.svg'},
    {name: 'Tipo', orderBy: false},
    {name: 'Departamento', orderBy: false},
    {name: 'Dirección', orderBy: false}
  ];
  offices: Office[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredOfficeSubs: Subscription;
  selectedOffice: Office;
  loadingOffices = true;

  get company(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private officeService: OfficeService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadOffices();
    this.subscribeToRecoveredOffices();
  }

  ngOnDestroy() {
    this.recoveredOfficeSubs.unsubscribe();
  }

  loadOffices(page?: number) {
    this.loadingOffices = true;
    this.officeService.getAll({companyId: this.company.companyId, page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
      if(this.paginationMeta?.currentPage !== response.meta.currentPage) this.selectedOffice = null;
      this.loadingOffices = false;
      this.paginationMeta = response.meta;
      this.offices = response.items;
    }, (error) => {
      this.loadingOffices= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadOffices(event);
  }

  subscribeToRecoveredOffices() {
    this.recoveredOfficeSubs = this.officeService.recoveredOffice$.subscribe(response => {
      this.loadOffices(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  selectOffice(office: Office) {
    this.selectedOffice = office;
  }

  public addOffice() {
    this.router.navigateByUrl('/dashboard/oficina/create');
  }

  public editOffice(office: Office) {
    this.router.navigateByUrl(`/dashboard/oficina/edit/${office.officeId}`, {state: {office}});
  }

  public deleteOffice(office: Office) {
    const modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar la parada ${office.name}.`,
        type: 'delete'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(office.officeId);
          this.officeService.delete( office.officeId).pipe(take(1)).subscribe( response => {
            this.selectedOffice = null;
            this.removeFromArray(office.officeId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => office.officeId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'office',
              objectId: office.officeId
            });
            if(this.offices.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadOffices(this.paginationMeta.currentPage - 1);
            } else {
              this.loadOffices(this.paginationMeta.currentPage);
            }
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => office.officeId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        }
      });
  }

  removeFromArray(officeId: number) {
    this.offices.splice(
      this.offices.findIndex((officeInArray: Office) => officeId === officeInArray.officeId), 1
    );
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showDetail(office: Office) {
    let modalData: ModalData = {
      state: {
        office
      }
    }
    this.modalRef = this.modalService.open(OfficeInfoModalComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let officeInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Office, number]) => {
        officeInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteOffice(result[0]);
            break;
          case 2:
            this.editOffice(result[0]);
            break;
        }
    });
  }
}
