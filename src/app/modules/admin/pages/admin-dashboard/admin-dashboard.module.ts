import { NgModule } from '@angular/core';
import { AdminDashboardRoutingModule } from './admin-dashboard-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
import { PipesModule } from 'src/app/core/pipes/pipes.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminDashboardRoutingModule,
    SharedModule,
    PipesModule
  ]
})
export class AdminDashboardModule { }
