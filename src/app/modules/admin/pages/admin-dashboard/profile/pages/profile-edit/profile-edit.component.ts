import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Profile, ProfileService } from 'src/app/core/http/profile';
import { ProfileFormComponent } from 'src/app/shared/forms';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { User } from 'src/app/core/http/user';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  @ViewChild(ProfileFormComponent) profileForm: ProfileFormComponent;

  modalRef: NgbModalRef;
  public profile: Profile;
  submitting: boolean;

  get loggedUser(): User {
    return this.authService.loggedUser;
  }

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    if(history.state.prof) {
      this.profile = history.state.prof;
    } else {
      this.router.navigateByUrl('/dashboard/perfil');
    }
  }

  public async onSubmit() {
    this.submitting = true;
    this.profileForm.profileGroup.disable();
    let profile: Profile;
    await this.profileForm.onSubmit().then( profileForm => {
      profile = profileForm;
    });
    if (profile) {
      this.profileService.update(profile.profileId, profile ).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.profileForm.profileGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/perfil/perfiles');
      }, (error) => {
        console.warn(error);
        this.submitting = false;
        this.profileForm.profileGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.profileForm.profileGroup.enable();
    }
  }

  public onDiscard() {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea salir sin guardar?',
        description: 'No se guardarán los cambios realizados y se perderá la información.',
        type: 'save'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        if( result ) {
          this.onSubmit();
        } else {
          this.router.navigateByUrl('/dashboard');
        }
      });
  }
}
