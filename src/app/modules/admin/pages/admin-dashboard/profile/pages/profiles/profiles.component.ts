import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { ConfirmationComponent } from 'src/app/shared/modals/confirmation/confirmation.component';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit, OnDestroy {

  colsTable = [
    {name: 'Nombre de Parada', orderBy: true, extraImg: 'assets/icons/bus-outline.svg'},
    {name: 'Dirección', orderBy: false}
  ];
  bStops: BusStop[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  recoveredBusStopSubs: Subscription;
  loadingBusStops = true;

  constructor(
    private busStopService: BusStopService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit(): void {
    this.loadBusStops();
    this.subscribeToRecoveredBusStops();
  }

  ngOnDestroy() {
    this.recoveredBusStopSubs.unsubscribe();
  }

  loadBusStops() {
    this.busStopService.getAll().pipe(take(1)).subscribe((response: any) => {
      this.loadingBusStops = false;
      this.bStops = (response.items as BusStop[]);
    }, (error) => {
      console.warn(error);
      this.loadingBusStops = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  subscribeToRecoveredBusStops() {
    this.recoveredBusStopSubs = this.busStopService.recoveredBusStop$.subscribe(response => {
      this.loadBusStops();
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  public addBusStop(event: boolean) {
    this.router.navigateByUrl('/dashboard/perfil/create');
  }

  public editBusStop(bStop: BusStop) {
    this.router.navigateByUrl(`/dashboard/perfil/edit/${bStop.busStopId}`, {state: {bStop}});
  }

  public deleteBusStop(bStop: BusStop) {
    const modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar la parada ${bStop.name}.`,
        type: 'delete',

      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(bStop.busStopId);
          this.busStopService.delete( bStop.busStopId).pipe(take(1)).subscribe( response => {
            this.bStops.splice(
              this.bStops.findIndex((busStopInArray: BusStop) => bStop.busStopId === busStopInArray.busStopId), 1
            );
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bStop.busStopId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'busStop',
              objectId: bStop.busStopId
            });
            this.loadBusStops();
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bStop.busStopId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
        }
      });
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }
}
