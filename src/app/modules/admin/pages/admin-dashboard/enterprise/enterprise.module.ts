import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EnterpriseRoutingModule } from './enterprise-routing.module';
import { EnterpriseComponent } from './enterprise.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { PipesModule } from 'src/app/core/pipes/pipes.module';

// Shared
import { SharedModule } from 'src/app/shared/shared.module';

// Pages
import {
  EnterprisesComponent,
  EnterpriseCreateOldComponent,
  EnterpriseEditOldComponent
} from './pages';

@NgModule({
  declarations: [
    EnterprisesComponent,
    EnterpriseComponent,
    EnterpriseCreateOldComponent,
    EnterpriseEditOldComponent
  ],
  imports: [
    CommonModule,
    EnterpriseRoutingModule,
    SharedModule,
    NgbModule,
    TooltipModule,
    PipesModule
  ]
})
export class EnterpriseModule { }
