import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { ChangeAdminPasswordFormComponent } from 'src/app/shared/forms';
import { ENTERPRISE_BASE_ROUTE, ENTERPRISE_HOME_ROUTE } from '../../base-route';

@Component({
  selector: 'app-change-admin-password',
  templateUrl: './change-admin-password.component.html',
  styleUrls: []
})
export class ChangeAdminPasswordComponent implements OnInit, OnDestroy {

  @ViewChild(ChangeAdminPasswordFormComponent) changeAdminPasswordForm: ChangeAdminPasswordFormComponent;

  nextStepSubs: Subscription;
  saveStepSubs: Subscription;
  step: number = 5;

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  set submitting(value: boolean) {
    this.steperFormService.submitting = value;
  }

  constructor(
    private steperFormService: SteperFormService,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.steperFormService.steper.currentStep = this.step;
    this.initSubs();
  }

  ngOnDestroy(): void {
    this.saveStepSubs?.unsubscribe();
    this.nextStepSubs?.unsubscribe();
  }

  initSubs() {
    this.saveStepSubs = this.steperFormService.saveStep$.subscribe( () => {
      this.onSubmit();
    });
    this.nextStepSubs = this.steperFormService.nextStep$.subscribe( () => {
      this.onSubmit(true)
    });
  }

  public async onSubmit(nextEvent?: boolean) {
    this.submitting = true;
    this.changeAdminPasswordForm.changePasswordGroup.disable();
    const password = await this.changeAdminPasswordForm.onSubmit();
    if(password) {
      this.resolveForm();
      this.steperFormService.toHome(ENTERPRISE_HOME_ROUTE);
    } else {
      this.resolveForm();
    }
  }

  resolveForm() {
    this.submitting = false;
    this.changeAdminPasswordForm.changePasswordGroup.enable();
  }
}