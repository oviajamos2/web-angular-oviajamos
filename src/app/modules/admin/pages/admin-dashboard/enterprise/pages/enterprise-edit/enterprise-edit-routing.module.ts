import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnterpriseEditComponent } from './enterprise-edit.component';

// Pages
import {
  EnterpriseAdminComponent,
  EnterpriseCommissionComponent,
  EnterpriseFirstOfficeComponent,
  EnterpriseInfoComponent,
  ChangeAdminPasswordComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: '1', pathMatch: 'full' },
  {
    path: '',
    component: EnterpriseEditComponent,
    children: [
      { path: '1', component: EnterpriseInfoComponent },
      { path: '2', component: EnterpriseAdminComponent },
      { path: '3', component: EnterpriseCommissionComponent },
      { path: '4', component: EnterpriseFirstOfficeComponent },
      { path: '5', component: ChangeAdminPasswordComponent },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EnterpriseEditRoutingModule { }
