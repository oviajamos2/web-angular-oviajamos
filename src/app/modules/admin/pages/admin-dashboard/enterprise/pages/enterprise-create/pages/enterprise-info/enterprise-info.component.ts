import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { EnterpriseInfoFormComponent } from 'src/app/shared/forms';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { ENTERPRISE_BASE_ROUTE } from '../../base-route';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprise-info',
  templateUrl: './enterprise-info.component.html',
  styleUrls: []
})
export class EnterpriseInfoComponent implements OnInit, OnDestroy {

  @ViewChild(EnterpriseInfoFormComponent) enterpriseInfoForm: EnterpriseInfoFormComponent;

  nextStepSubs: Subscription;
  saveStepSubs: Subscription;
  step: number = 1;

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  set submitting(value: boolean) {
    this.steperFormService.submitting = value;
  }

  get savedForm(): Enterprise {
    return this.steperFormService.stepObjects[this.step-1] || null;
  }

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  constructor(
    private steperFormService: SteperFormService,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.steperFormService.steper.currentStep = this.step;
    this.initSubs();
  }

  ngOnDestroy(): void {
    this.saveStepSubs?.unsubscribe();
    this.nextStepSubs?.unsubscribe();
  }

  initSubs() {
    this.saveStepSubs = this.steperFormService.saveStep$.subscribe( () => {
      this.onSubmit();
    });
    this.nextStepSubs = this.steperFormService.nextStep$.subscribe( () => {
      this.onSubmit(true)
    });
  }

  public async onSubmit(nextEvent?: boolean) {
    this.submitting = true;
    this.enterpriseInfoForm.enterpriseGroup.disable();
    let enterprise: Enterprise;
    await this.enterpriseInfoForm.onSubmit().then( enterpriseForm => {
      enterprise = enterpriseForm;
    });
    if (enterprise) {
      if(!enterprise.users) enterprise.users = [];
      if(!enterprise.imageUrl) enterprise.imageUrl = this.savedForm.imageUrl;
      if(!enterprise.companyId) {
        if(!this.createdEnterprise?.lastCreatedStep || this.createdEnterprise?.lastCreatedStep < this.step) {
          enterprise.lastCreatedStep = this.step;
        }
        this.enterpriseService.create(enterprise).pipe(take(1)).subscribe( (response: Enterprise) => {
          this.submitting = false;
          this.steperFormService.steper.lastCreatedStep = enterprise.lastCreatedStep;
          enterprise.companyId = response.companyId;
          this.steperFormService.createdObject = response;
          this.steperFormService.saveOnStorage(enterprise);
          this.enterpriseInfoForm.enterpriseGroup.enable();
          this.enterpriseInfoForm.enterprise = enterprise;
          this.enterpriseInfoForm.buildForm();
          this.enterpriseInfoForm.checkDefaultImage();
          this.notificationService.notify(SAVED_NOTIFICATION);
          if(nextEvent) this.steperFormService.next(ENTERPRISE_BASE_ROUTE);
        }, (error) => {
          this.submitting = false;
          this.enterpriseInfoForm.enterpriseGroup.enable();
          console.warn(error);
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        });
      } else {
        if(!enterprise.lastCreatedStep || enterprise.lastCreatedStep < this.step) {
          enterprise.lastCreatedStep = this.step;
        }
        this.enterpriseService.patch(enterprise.companyId, enterprise).pipe(take(1)).subscribe( (response: Enterprise) => {
          this.submitting = false;
          this.steperFormService.steper.lastCreatedStep = enterprise.lastCreatedStep;
          this.steperFormService.createdObject = response;
          this.steperFormService.saveOnStorage(enterprise);
          this.enterpriseInfoForm.enterpriseGroup.enable();
          this.enterpriseInfoForm.enterprise = enterprise;
          this.enterpriseInfoForm.buildForm();
          this.enterpriseInfoForm.checkDefaultImage();
          this.notificationService.notify(SAVED_NOTIFICATION);
          if(nextEvent) this.steperFormService.next(ENTERPRISE_BASE_ROUTE);
        }, (error) => {
          this.submitting = false;
          this.enterpriseInfoForm.enterpriseGroup.enable();
          console.warn(error);
          this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        });
      }
    } else {
      this.submitting = false;
      this.enterpriseInfoForm.enterpriseGroup.enable();
    }
  }
}