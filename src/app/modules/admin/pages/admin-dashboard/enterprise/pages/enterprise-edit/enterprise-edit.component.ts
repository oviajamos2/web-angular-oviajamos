import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { CompanyStatusEnum, Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { ENTERPRISE_BASE_ROUTE, ENTERPRISE_CRETE_STEPS, ENTERPRISE_HOME_ROUTE } from './base-route';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { SteperForm } from 'src/app/core/services/steper-form/steper-form';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { User, UserRoleEnum } from 'src/app/core/http/user';

@Component({
  selector: 'app-enterprise-edit',
  templateUrl: './enterprise-edit.component.html',
  styleUrls: ['./enterprise-edit.component.scss']
})
export class EnterpriseEditComponent implements OnInit, OnDestroy {

  loadingEnterprise: boolean = true;
  userRoleEnum = UserRoleEnum;

  get loggedUser(): User {
    return this.authService.loggedUser;
  }

  get steper(): SteperForm {
    return this.steperFormService.steper;
  }

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  constructor(
    private router: Router,
    private authService: AuthService,
    private steperFormService: SteperFormService,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) {
    this.steperFormService.steperValue = {
      completed: false,
      currentStep: 1,
      steps: 4,
      progressSteps: ENTERPRISE_CRETE_STEPS
    };
  }

  ngOnInit(): void {
    if(history.state.enterprise || this.steperFormService.createdObject) {
      this.steperFormService.createdObject = history.state.enterprise || this.steperFormService.createdObject;
      this.steperFormService.saveOnStorage(this.createdEnterprise);
      this.loadEnterprise();
    } else {
      this.router.navigateByUrl(ENTERPRISE_HOME_ROUTE);
    }
  }

  async loadEnterprise(): Promise<void> {
    this.enterpriseService.get( this.createdEnterprise.companyId ).pipe(take(1)).subscribe( (response: Enterprise) => {
      this.steperFormService.createdObject = response;
      this.steperFormService.saveOnStorage(this.createdEnterprise);
      this.checkPreviousData();
      this.loadingEnterprise = false;
      return;
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
      this.router.navigateByUrl('/dashboard/empresa');
      return;
    });
  }

  ngOnDestroy(): void {
    this.steperFormService.resetSteper();
  }

  checkPreviousData() {
    if(!this.steperFormService.createdObject?.lastCreatedStep) {
      this.router.navigateByUrl(
        `/dashboard/empresa/edit/${ this.steperFormService.createdObject.companyId }`
      );
    } else if(this.steperFormService.createdObject?.lastCreatedStep < this.steperFormService.steper.steps) {
      this.router.navigateByUrl(
        `/dashboard/empresa/edit/${ this.steperFormService.createdObject.companyId }/${ this.steperFormService.createdObject.lastCreatedStep + 1 }`
      );
    } else if(this.steperFormService.createdObject?.lastCreatedStep === this.steperFormService.steper.steps) {
      this.router.navigateByUrl(
        `/dashboard/empresa/edit/${ this.steperFormService.createdObject.companyId }/${ this.steperFormService.steper.steps }`
      );
    } else {
      this.router.navigateByUrl(`/dashboard/empresa/edit/${ this.steperFormService.createdObject.companyId }`);
    }
    if(this.createdEnterprise.administrator &&
        this.createdEnterprise.status !== CompanyStatusEnum.INCOMPLETE_RECORD) {
      this.steper.steps = this.steper.steps + 1;
      this.router.navigateByUrl(
        `/dashboard/empresa/edit/${ this.steperFormService.createdObject.companyId }/${ this.steperFormService.steper.steps }`
      );
    }
  }

  public onToHome(event?) {
    this.steperFormService.resetSteper();
    this.router.navigateByUrl(`/dashboard/empresa`);
  }

  public onSubmit(event?) {
    this.steperFormService.save();
  }

  public onNext(event?) {
    this.steperFormService.nextStep$.next(true);
  }

  public onPrev(event?) {
    this.steperFormService.back(`${ ENTERPRISE_BASE_ROUTE }/${ this.createdEnterprise.companyId }`);
  }
}