import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnterpriseCreateComponent } from './enterprise-create.component';

// Pages
import {
  EnterpriseAdminComponent,
  EnterpriseCommissionComponent,
  EnterpriseFirstOfficeComponent,
  EnterpriseInfoComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: '1', pathMatch: 'full' },
  {
    path: '',
    component: EnterpriseCreateComponent,
    children: [
      { path: '1', component: EnterpriseInfoComponent },
      { path: '2', component: EnterpriseAdminComponent },
      { path: '3', component: EnterpriseCommissionComponent },
      { path: '4', component: EnterpriseFirstOfficeComponent },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EnterpriseCreateRoutingModule { }
