import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { EnterpriseOldFormComponent } from 'src/app/shared/forms/index';
import { Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprise-create-old',
  templateUrl: './enterprise-create-old.component.html',
  styleUrls: ['./enterprise-create-old.component.scss']
})
export class EnterpriseCreateOldComponent implements OnInit {

  @ViewChild(EnterpriseOldFormComponent) enterpriseForm: EnterpriseOldFormComponent;

  submitting: boolean;

  constructor(
    private router: Router,
    private enterpriseService: EnterpriseService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {}

  public async onSubmit() {
    this.submitting = true;
    this.enterpriseForm.enterpriseGroup.disable();
    let enterprise: Enterprise;
    await this.enterpriseForm.onSubmit().then( enterpriseForm => {
      enterprise = enterpriseForm;
    });
    if (enterprise) {
      this.enterpriseService.create(enterprise).pipe(take(1)).subscribe( response => {
        this.submitting = false;
        this.enterpriseForm.enterpriseGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/empresa/empresas');
      }, (error) => {
        this.submitting = false;
        this.enterpriseForm.enterpriseGroup.enable();
        console.warn(error);
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = false;
      this.enterpriseForm.enterpriseGroup.enable();
    }
  }

  public onDiscard() {
    this.router.navigateByUrl('/dashboard/empresa/empresas');
  }
}
