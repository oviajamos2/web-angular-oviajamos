import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Enterprise } from 'src/app/core/http/enterprise';
import { User, UserRoleEnum } from 'src/app/core/http/user';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { SteperForm } from 'src/app/core/services/steper-form/steper-form';
import { SteperFormService } from 'src/app/core/services/steper-form/steper-form.service';
import { ENTERPRISE_CRETE_STEPS } from '../enterprise-edit/base-route';
import { ENTERPRISE_BASE_ROUTE, ENTERPRISE_HOME_ROUTE } from './base-route';

@Component({
  selector: 'app-enterprise-create',
  templateUrl: './enterprise-create.component.html',
  styleUrls: ['./enterprise-create.component.scss']
})
export class EnterpriseCreateComponent implements OnInit, OnDestroy {

  userRoleEnum = UserRoleEnum;

  get loggedUser(): User {
    return this.authService.loggedUser;
  }

  get steper(): SteperForm {
    return this.steperFormService.steper;
  }

  get submitting(): boolean {
    return this.steperFormService.submitting;
  }

  get createdEnterprise(): Enterprise {
    return this.steperFormService.createdObject;
  }

  constructor(
    private router: Router,
    private authService: AuthService,
    private steperFormService: SteperFormService
  ) {
    this.steperFormService.steperValue = {
      completed: false,
      currentStep: 1,
      steps: 4,
      progressSteps: ENTERPRISE_CRETE_STEPS
    };
  }

  ngOnInit(): void {
    this.checkPreviousData();
  }

  ngOnDestroy(): void {
    this.steperFormService.resetSteper();
  }

  checkPreviousData() {
    if(!this.steperFormService.createdObject?.lastCreatedStep) {
      this.router.navigateByUrl('/dashboard/empresa/create');
    } else if(this.steperFormService.createdObject?.lastCreatedStep < this.steperFormService.steper.steps) {
      this.router.navigateByUrl(`/dashboard/empresa/create/${ this.steperFormService.createdObject.lastCreatedStep + 1 }`);
    } else if(this.steperFormService.createdObject?.lastCreatedStep === this.steperFormService.steper.steps) {
      this.router.navigateByUrl(`/dashboard/empresa/create/${ this.steperFormService.steper.steps }`);
    } else {
      this.router.navigateByUrl('/dashboard/empresa/create');
    }
  }

  public onToHome(event?) {
    this.steperFormService.resetSteper();
    this.router.navigateByUrl(ENTERPRISE_HOME_ROUTE);
  }

  public onSubmit(event?) {
    this.steperFormService.save();
  }

  public onNext(event?) {
    this.steperFormService.nextStep$.next(true);
  }

  public onPrev(event?) {
    this.steperFormService.back(ENTERPRISE_BASE_ROUTE);
  }
}