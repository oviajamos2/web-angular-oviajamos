import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnterpriseCreateRoutingModule } from './enterprise-create-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EnterpriseCreateComponent } from './enterprise-create.component';

import {
  EnterpriseAdminComponent,
  EnterpriseCommissionComponent,
  EnterpriseFirstOfficeComponent,
  EnterpriseInfoComponent
} from './pages';


@NgModule({
  declarations: [
    EnterpriseCreateComponent,
    EnterpriseInfoComponent,
    EnterpriseAdminComponent,
    EnterpriseCommissionComponent,
    EnterpriseFirstOfficeComponent
  ],
  imports: [
    CommonModule,
    EnterpriseCreateRoutingModule,
    SharedModule
  ]
})
export class EnterpriseCreateModule { }
