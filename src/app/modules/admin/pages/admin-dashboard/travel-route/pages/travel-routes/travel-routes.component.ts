import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent} from 'src/app/shared/modals/index';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse, PaginationMeta } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { TravelRouteInfoComponent } from 'src/app/shared/modals/mobile';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-travel-routes',
  templateUrl: './travel-routes.component.html',
  styleUrls: ['./travel-routes.component.scss']
})
export class TravelRoutesComponent implements OnInit, OnDestroy {

  colsTable = [
    { name: 'Nombre de ruta', orderBy: true},
    { name: 'Estimación', orderBy: false }
  ];
  intermediateRoutes: TravelRoute[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredTravelRoute: Subscription;
  routes: TravelRoute[] = [];
  selectedRoute: TravelRoute;
  showButtom: boolean = true;
  title: string = 'Administración';
  loadingTravelRoutes= true;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }
  
  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private travelRouteService: TravelRouteService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.loadTravelRoutes();
    this.subscribeToRecoveredTravelRoutes();
  }

  ngOnDestroy(): void {
    this.recoveredTravelRoute.unsubscribe();
  }

  public addRoute() {
    this.router.navigateByUrl('/dashboard/ruta/create');
  }

  editRoute(travelRoute: TravelRoute) {
    this.router.navigateByUrl(`/dashboard/ruta/edit/${ travelRoute.routeId }`, {state: { travelRoute } });
  }

  deleteRoute(travelRoute: TravelRoute) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Esta seguro en eliminar la ruta con origen ${travelRoute.source} y destino a ${travelRoute.destination}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent );
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(travelRoute.routeId);
          this.travelRouteService.delete( travelRoute.routeId).pipe(take(1)).subscribe( response => {
            this.selectedRoute = null;
            this.removeFromArray(travelRoute.routeId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => travelRoute.routeId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'travelRoute',
              objectId: travelRoute.routeId
            });
            if(this.routes.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadTravelRoutes(this.paginationMeta.currentPage - 1);
            } else {
              this.loadTravelRoutes(this.paginationMeta.currentPage);
            }
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => travelRoute.routeId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
        }
      });
  }

  removeFromArray(routeId: number) {
    this.routes.splice(
      this.routes.findIndex((routeInArray: TravelRoute) => routeId === routeInArray.routeId), 1
    ); 
  }

  loadTravelRoutes(page?: number) {
    this.loadingTravelRoutes = true;
    this.travelRouteService.getAll({companyId: this.enterprise.companyId, page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
      if(this.paginationMeta?.currentPage !== response.meta.currentPage) this.selectedRoute = null;
      this.loadingTravelRoutes = false;
      this.paginationMeta = response.meta;
      this.routes = response.items;
      this.routes = this.routes.reverse();
    }, (error) => {
      console.warn(error);
      this.loadingTravelRoutes= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadTravelRoutes(event);
  }

  subscribeToRecoveredTravelRoutes() {
    this.recoveredTravelRoute = this.travelRouteService.recoveredTravelRoute$.subscribe( response => {
      this.loadTravelRoutes(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showRouteDetail(index: number) {
    let actualvalue = this.routes[index].showDetail;
    this.routes[index].showDetail = !actualvalue;
  }

  selectRoute(route: TravelRoute) {
    this.selectedRoute = route;
  }

  showDetail(route: TravelRoute) {
    let modalData: ModalData = {
      state: {
        route
      }
    }
    this.modalRef = this.modalService.open(TravelRouteInfoComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let routeInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [TravelRoute, number]) => {
        routeInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteRoute(result[0]);
            break;
          case 2:
            this.editRoute(result[0]);
            break;
        }
    });
  }
}