import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-company',
  templateUrl: './profile-company.component.html',
  styleUrls: ['./profile-company.component.scss']
})

export class ProfileCompanyComponent implements OnInit {
  
  public menu:string;
  
  constructor() { 
    this.menu = "info-company";
  }

  ngOnInit(): void {
  }

  changeOption(name:string) :void {
    this.menu = name;
  }
  
  optionValid(nameOption:string):boolean{
    return this.menu === nameOption;
  }
  
}
