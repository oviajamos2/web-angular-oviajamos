import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-company',
  templateUrl: './profile-company.component.html',
  styleUrls: ['./profile-company.component.scss']
})
export class ProfileCompanyComponent implements OnInit {
  
  list = ["infoCompany", "infoAdmin", "changePassword"];
  optionForm:String;
  
  constructor() { 

  }

  ngOnInit(): void {
  }

}
