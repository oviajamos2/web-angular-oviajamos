import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-info-company',
  templateUrl: './info-company.component.html',
  styleUrls: ['./info-company.component.scss']
})
export class InfoCompanyComponent implements OnInit {
  
  busStopGroup: FormGroup;
  dafaultImage = 'assets/icons/home-outline.svg';
  public typeCompany = ['srl, anonima'];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
  }

  public setImage(event:Event) {
    console.log('cambiar imagen');
  }
}
