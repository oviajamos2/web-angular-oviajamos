import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { ConfirmationComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { OfficeAdmin, OfficeAdminService } from 'src/app/core/http/office-admin';
import { OfficeAdminInfoModalComponent } from 'src/app/shared/modals/mobile';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-admins',
  templateUrl: './office-admins.component.html',
  styleUrls: ['./office-admins.component.scss']
})
export class OfficeAdminsComponent implements OnInit, OnDestroy {

  colsTable = [
    { name: 'Nombre y Apellido', orderBy: true, extraImg: 'assets/icons/user-image-icon-3.svg' },
    { name: 'Celular', orderBy: false },
    { name: 'Dirección', orderBy: false }
  ];
  officeAdmins: OfficeAdmin[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredOfficeAdmin: Subscription;
  selectedOfficeAdmin: OfficeAdmin = null;
  loadingOfficeAdmins = true;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private officeAdminService: OfficeAdminService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.loadOfficeAdmins();
    this.subscribeToRecoveredOfficeAdmins();
  }

  ngOnDestroy(): void {
    this.recoveredOfficeAdmin.unsubscribe();
  }

  loadOfficeAdmins(page?: number) {
    this.loadingOfficeAdmins = true;
    this.officeAdminService.getAll({companyId: this.enterprise.companyId, page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<OfficeAdmin>) => {
      if(this.paginationMeta?.currentPage !== response.meta.currentPage) this.selectedOfficeAdmin = null;
      this.loadingOfficeAdmins = false;
      this.paginationMeta = response.meta;
      this.officeAdmins = response.items;
    }, (error) => {
      console.warn(error);
      this.loadingOfficeAdmins= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadOfficeAdmins(event);
  }

  subscribeToRecoveredOfficeAdmins() {
    this.recoveredOfficeAdmin = this.officeAdminService.recoveredOfficeAdmin$.subscribe( (response: OfficeAdmin) => {
      this.loadOfficeAdmins(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  selectOfficeAdmin(officeAdmin: OfficeAdmin) {
    this.selectedOfficeAdmin = officeAdmin;
  }

  public selectUser(index: number) {}

  public addOfficeAdmin() {
    this.router.navigateByUrl('/dashboard/administrador-oficina/create');
  }

  public editOfficeAdmin(officeAdmin: OfficeAdmin) {
    this.router.navigateByUrl(`/dashboard/administrador-oficina/edit/${officeAdmin.secretaryId}`, { state: { officeAdmin } });
  }

  public deleteOfficeAdmin( officeAdmin: OfficeAdmin ) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Esta seguro que quiere eliminar al conductor ${officeAdmin.user.name} ${officeAdmin.user.lastName}.`,
        type: 'delete'
      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent );
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.modifying.push(officeAdmin.secretaryId);
          this.officeAdminService.delete( officeAdmin.secretaryId ).pipe(take(1)).subscribe( (response: OfficeAdmin) => {
            this.selectedOfficeAdmin = null;
            this.removeFromArray(officeAdmin.secretaryId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => officeAdmin.secretaryId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'officeAdmin',
              objectId: officeAdmin.secretaryId
            });
            if(this.modifying.length > 0) return;
            if(this.officeAdmins.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadOfficeAdmins(this.paginationMeta.currentPage - 1);
            } else {
              this.loadOfficeAdmins(this.paginationMeta.currentPage);
            }
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => officeAdmin.secretaryId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        }
      });
  }

  removeFromArray(officeAdminId: number) {
    this.officeAdmins.splice(
      this.officeAdmins.findIndex((officeAdminInArray: OfficeAdmin) => officeAdminId === officeAdminInArray.secretaryId), 1
    );
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showDetail(officeAdmin: OfficeAdmin) {
    let modalData: ModalData = {
      state: {
        officeAdmin
      }
    }
    this.modalRef = this.modalService.open(OfficeAdminInfoModalComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let officeAdminInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [OfficeAdmin, number]) => {
        officeAdminInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteOfficeAdmin(result[0]);
            break;
          case 2:
            this.editOfficeAdmin(result[0]);
            break;
        }
    });
  }
}
