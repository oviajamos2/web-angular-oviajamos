import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficeAdminComponent } from './office-admin.component';

// Pages
import {
  OfficeAdminsComponent,
  OfficeAdminCreateComponent,
  OfficeAdminEditComponent
} from './pages';

const routes: Routes = [
  { path: '', redirectTo: 'administradores', pathMatch: 'full' },
  {
    path: '',
    component: OfficeAdminComponent,
    children: [
      { path: 'administradores', component: OfficeAdminsComponent },
      { path: 'create', component: OfficeAdminCreateComponent },
      { path: 'edit/:id', component: OfficeAdminEditComponent },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class OfficeAdminRoutingModule { }
