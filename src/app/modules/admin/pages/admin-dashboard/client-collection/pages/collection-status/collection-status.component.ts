import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router'
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePickerComponent } from 'ng2-date-picker';
import { Subscription, Subject } from 'rxjs';
import { take, auditTime } from 'rxjs/operators';

import { CompanyStatusEnum, Enterprise, EnterpriseService } from 'src/app/core/http/enterprise';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { EnterprisePaymentStatusModalComponent } from 'src/app/shared/modals/mobile';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services'
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-collection-status',
  templateUrl: './collection-status.component.html',
  styleUrls: ['./collection-status.component.scss']
})
export class CollectionStatusComponent implements OnInit, OnDestroy {

  @ViewChild('datepicker') datepicker: DatePickerComponent;

  colsTable = [
    {name: 'Nombre', orderBy: true},
    {name: 'Fecha de último pago', orderBy: false},
    {name: 'Estado', orderBy: false}
  ];
  companyStatusEnum = CompanyStatusEnum;
  companyStatusEnumKeys = Object.keys(this.companyStatusEnum);
  datePickerConf = { ...defaultConf };
  enterprises: Enterprise[] = [];
  enterpriseFilterGroup: FormGroup;
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  selectedEnterprise: Enterprise;
  submit$ = new Subject();
  submitSubs: Subscription;
  loadingEnterprises = true;

  get enterpriseFilterForm() {
    return this.enterpriseFilterGroup.controls;
  }

  get day() { return moment(this.enterpriseFilterForm.date.value, [DATE_FORMAT]).format('dddd')}
  get numberDay() { return moment(this.enterpriseFilterForm.date.value, [DATE_FORMAT]).format('D')}
  get month() { return moment(this.enterpriseFilterForm.date.value, [DATE_FORMAT]).format('MMMM')}
  get year() { return moment(this.enterpriseFilterForm.date.value, [DATE_FORMAT]).format('YYYY')}

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private enterpriseService: EnterpriseService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.loadEnterprises();
    this.initSubmitSubs();
  }

  ngOnDestroy(): void {
    this.submitSubs?.unsubscribe();
  }

  buildForm() {
    this.enterpriseFilterGroup = this.formBuilder.group({
      date: [moment().format(DATE_FORMAT)]
    });
  }

  loadEnterprises(page?: number) {
    this.loadingEnterprises = true;
    this.enterpriseService.getAll({page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Enterprise>) => {
      this.loadingEnterprises = false;
      this.paginationMeta = response.meta;
      this.enterprises = response.items;
    }, (error) => {
      console.warn(error);
      this.loadingEnterprises= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadEnterprises(event);
  }

  selectEnterprise(enterprise: Enterprise) {
    this.selectedEnterprise = enterprise;
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showDetail(enterprise: Enterprise) {
    let modalData: ModalData = {
      state: {
        enterprise
      }
    }
    this.modalRef = this.modalService.open(EnterprisePaymentStatusModalComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let enterpriseInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Enterprise, number]) => {
        enterpriseInfoModal.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.editPaymentData(result[0]);
            break;
          case 2:
            this.paymentDetail(result[0]);
            break;
        }
    });
  }

  paymentDetail(enterprise?: Enterprise) {
    this.router.navigateByUrl(
      `/dashboard/estado-cobros/detalle/${enterprise?.companyId || this.selectedEnterprise.companyId}`
    );
  }

  editPaymentData(enterprise?: Enterprise) {}

  onSubmitFilter(event?) {}

  initSubmitSubs() {
    this.submitSubs = this.submit$.pipe(
      auditTime(100)
    ).subscribe((event) => {
      this.onSubmitFilter(event);
    });
  }

  prevMonth() {
    this.enterpriseFilterForm.date.setValue(
      moment(this.enterpriseFilterForm.date.value, [DATE_FORMAT]).subtract(1, 'month').format(DATE_FORMAT)
    );
  }

  nextMonth() {
    this.enterpriseFilterForm.date.setValue(
      moment(this.enterpriseFilterForm.date.value, [DATE_FORMAT]).add(1, 'month').format(DATE_FORMAT)
    );
  }

  capitalize( val: string ) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  }
}
