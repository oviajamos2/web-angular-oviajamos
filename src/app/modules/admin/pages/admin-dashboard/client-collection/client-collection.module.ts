import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientCollectionRoutingModule } from './client-collection-routing.module';
import { ClientCollectionComponent } from './client-collection.component';
import { DpDatePickerModule } from 'ng2-date-picker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { SharedModule } from 'src/app/shared/shared.module';

import {
  CollectionStatusComponent,
  CollectionInfoComponent
} from './pages';

@NgModule({
  declarations: [
    ClientCollectionComponent,
    CollectionStatusComponent,
    CollectionInfoComponent
  ],
  imports: [
    CommonModule,
    ClientCollectionRoutingModule,
    SharedModule,
    NgbModule,
    PipesModule,
    DpDatePickerModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class ClientCollectionModule { }
