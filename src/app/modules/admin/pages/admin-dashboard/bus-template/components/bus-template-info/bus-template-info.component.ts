import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

import { SeatingScheme, SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-template-info',
  templateUrl: './bus-template-info.component.html',
  styleUrls: ['./bus-template-info.component.scss']
})
export class BusTemplateInfoComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  editingTemplate: boolean = false;
  seatingSchemes: SeatingScheme[] = [];
  selectedSeatingScheme: SeatingScheme;
  state: SeatingScheme

  get seatingScheme(): SeatingScheme {
    return this.modalData?.state;
  }

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private router: Router,
    private seatingSchemeService: SeatingSchemeService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadSeatingSchemes();
  }

  submitTemplate() {}

  editTemplate() {
    let seatingScheme = this.selectedSeatingScheme
    this.action.next(false);
    this.router.navigateByUrl(`/dashboard/plantilla-bus/edit/${this.selectedSeatingScheme.seatingSchemeId}`, {
      state: {seatingScheme}
    });
  }

  selectTemplate(seatingScheme: SeatingScheme) {
    this.selectedSeatingScheme = seatingScheme;
  }

  createTemplate() {
    this.action.next(false);
    this.router.navigateByUrl( '/dashboard/plantilla-bus/create' );
  }

  loadSeatingSchemes() {
    this.seatingSchemeService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (seatingSchemes: PaginationResponse<SeatingScheme>) => {
      this.seatingSchemes = seatingSchemes.items;
    }, (error) => {
      console.warn(error);
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }
}