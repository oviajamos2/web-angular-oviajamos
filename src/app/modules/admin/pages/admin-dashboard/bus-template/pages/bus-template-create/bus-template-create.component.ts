import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { BusTemplateFormComponent } from 'src/app/shared/forms';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SeatingScheme, SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import { take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-template-create',
  templateUrl: './bus-template-create.component.html',
  styleUrls: ['./bus-template-create.component.scss']
})
export class BusTemplateCreateComponent implements OnInit {

  @Input() modalData: ModalData;
  @ViewChild(BusTemplateFormComponent) busTemplateForm: BusTemplateFormComponent;

  action = new Subject();
  submitting: boolean;

  get modalType(): boolean {
    return this.modalData?.state.modalType;
  }

  get openTemplatesModal$(): Subject<boolean> {
    return this.modalData?.state.openTemplatesModal;
  }

  constructor(
    private router: Router,
    private seatingSchemeService: SeatingSchemeService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {}

  discardTemplate() {
    if(!this.modalType) {
      this.router.navigateByUrl( '/dashboard/plantilla-bus' );
    } else {
      this.action.next(false);
      this.openTemplatesModal$.next(true);
    }
  }

  submitTemplate() {
    let busTemplate = this.busTemplateForm.onSubmit()
    if(busTemplate) {
      this.seatingSchemeService.create(busTemplate).pipe(take(1)).subscribe( (seatinScheme: SeatingScheme) => {
        this.notificationService.notify(SAVED_NOTIFICATION);
        if(!this.modalType) {
          this.router.navigateByUrl( '/dashboard/plantilla-bus' );
        } else {
          this.action.next(false);
          this.openTemplatesModal$.next(true);
        }
      }, (error) => {
        console.warn( error );
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    }
  }
}