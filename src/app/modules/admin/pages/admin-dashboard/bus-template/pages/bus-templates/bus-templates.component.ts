import { Component, OnDestroy, OnInit} from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router} from '@angular/router';
import { Subscription} from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { BusTemplateComponent } from 'src/app/shared/modals/mobile/bus-template/bus-template.component';
import { BusTemplateInfoComponent } from '../../components';
import { ConfirmationComponent} from 'src/app/shared/modals/confirmation/confirmation.component';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData} from 'src/app/core/interfaces/modal-data.interface';
import { ModalService} from 'src/app/core/services';
import { NotificationService} from 'src/app/core/services/notification/notification.service';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { SeatingScheme, SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-templates',
  templateUrl: './bus-templates.component.html',
  styleUrls: ['./bus-templates.component.scss']
})
export class BusTemplatesComponent implements OnInit, OnDestroy {

  colsTable = [
    {name: 'Nombre de bus', orderBy: true},
    {name: 'Asientos', orderBy: false},
  ];
  seatingSchemes: SeatingScheme[] = [];
  modalRef: NgbModalRef;
  modifying: number[] = [];
  paginationMeta: PaginationMeta;
  recoveredSeatingSchemeSubs: Subscription;
  loadingSeatingSchemes = true;
  selectedSeatingScheme: SeatingScheme = null;
  modalMovile: NgbModalRef;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }
  
  constructor(
    private seatingSchemeService: SeatingSchemeService,
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.loadSeatingSchemes();
    this.subscribeToRecoveredSeatingSchemes();
  }

  ngOnDestroy() {
    this.recoveredSeatingSchemeSubs.unsubscribe();
  }
 
  selectBus(seatingScheme: SeatingScheme): void {
    this.selectedSeatingScheme = seatingScheme;
  }

  loadSeatingSchemes(page?: number) {
    this.loadingSeatingSchemes = true;
    this.seatingSchemeService.getAll({ companyId: this.enterprise.companyId, page })
      .pipe(take(1)).subscribe( (response: PaginationResponse<SeatingScheme>) => {
      this.loadingSeatingSchemes = false;
      this.paginationMeta = response.meta;
      this.seatingSchemes = response.items;
    },(error) => {
      console.warn(error);
      this.loadingSeatingSchemes= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadSeatingSchemes(event);
  }

  subscribeToRecoveredSeatingSchemes() {
    this.recoveredSeatingSchemeSubs = this.seatingSchemeService.recoveredSeatingScheme$.subscribe(response => {
      this.loadSeatingSchemes(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  public addBusTemplate() {
    this.router.navigateByUrl('/dashboard/plantilla-bus/create');
  }

  showBus(): void {
    this.templateInfo(this.selectedSeatingScheme);
  }
  
  public editSeatingScheme(seatingScheme: SeatingScheme) {
    this.router.navigateByUrl(`/dashboard/plantilla-bus/edit/${seatingScheme.seatingSchemeId}`, {state: {seatingScheme}});
  }

  public deleteSeatingScheme(seatingScheme: SeatingScheme) {
    const modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar la parada ${seatingScheme.busName}.`,
        type: 'delete',

      }
    };
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if ( result ) {
          this.modifying.push(seatingScheme.seatingSchemeId);
          this.seatingSchemeService.delete( seatingScheme.seatingSchemeId).pipe(take(1)).subscribe( response => {
            this.selectedSeatingScheme = null;
            this.removeFromArray(seatingScheme.seatingSchemeId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => seatingScheme.seatingSchemeId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'seatingScheme',
              objectId: seatingScheme.seatingSchemeId
            });
            if(this.seatingSchemes.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadSeatingSchemes(this.paginationMeta.currentPage - 1);
            } else {
              this.loadSeatingSchemes(this.paginationMeta.currentPage);
            }
          }, error => {
            this.modifying.splice(
              this.modifying.findIndex((id: number) => seatingScheme.seatingSchemeId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
          });
        } else {
        }
      });
  }

  removeFromArray(seatingSchemeId: number) {
    this.seatingSchemes.splice(
      this.seatingSchemes.findIndex((seatingSchemeInArray: SeatingScheme) => seatingSchemeId === seatingSchemeInArray.seatingSchemeId), 1
    );
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  templateInfo(seatingScheme: SeatingScheme) {
    if(!event.defaultPrevented) {
      let modalData: ModalData = {
        state: seatingScheme
      }
      this.modalRef = this.modalService.open(BusTemplateInfoComponent, 'modal-lg' );
      this.modalRef.componentInstance.modalData = modalData;
      let templateModal: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
        this.modalRef.close();
        templateModal.unsubscribe();
      });
    }
  }

  public showMovilDetail(seatingScheme: SeatingScheme):void {
    this.selectBus(seatingScheme);
    if ( screen.width < 1024) {
      this.modalMovile = this.modalService.open(BusTemplateComponent);
      this.modalMovile.componentInstance.modalData = seatingScheme;
      let templateModal:Subscription = this.modalMovile.componentInstance.action.subscribe((result: any) => {
        templateModal.unsubscribe();
        this.modalMovile.close();
        switch(result) {
          case 'show':
            this.showBus();
            break;
          case 'edit':
            this.editSeatingScheme(this.selectedSeatingScheme);
          break;
          case 'delete':
            this.deleteSeatingScheme(this.selectedSeatingScheme);
          break;
          case 'close':
            this.modalMovile.close();
            break;
        }

      });
    }
  } 
}