import { Component, Input, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { SeatingScheme, SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalService } from 'src/app/core/services';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { BusTemplateCreateComponent, BusTemplateEditComponent } from '..';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-template-selection',
  templateUrl: './bus-template-selection.component.html',
  styleUrls: ['./bus-template-selection.component.scss']
})
export class BusTemplateSelectionComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  editingTemplate: boolean = false;
  modalRef: NgbModalRef;
  seatingSchemes: SeatingScheme[] = [];
  selectedSeatingScheme: SeatingScheme;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  get openTemplatesModal$(): Subject<boolean> {
    return this.modalData?.state.openTemplatesModal$;
  }

  constructor(
    private seatingSchemeService: SeatingSchemeService,
    private notificationService: NotificationService,
    private authService: AuthService,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    this.loadSeatingSchemes();
  }

  submitTemplate() {
    if(!this.selectedSeatingScheme) return;
    this.action.next(this.selectedSeatingScheme);
  }

  editTemplate() {
    this.action.next(false);
    const modalData: ModalData = {
      state: {
        modalType: true,
        openTemplatesModal: this.openTemplatesModal$,
        seatingScheme: this.selectedSeatingScheme
      }
    };
    this.modalRef = this.modalService.open(BusTemplateEditComponent, "modal-lg-2x", 'static');
    this.modalRef.componentInstance.modalData = modalData;
    let editTemplateModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        editTemplateModal.unsubscribe();
        this.modalRef.close();
      }); 
  }

  selectTemplate(seatingScheme: SeatingScheme) {
    this.selectedSeatingScheme = seatingScheme;
  }

  createTemplate() {
    this.action.next(false);
    const modalData: ModalData = {
      state: {
        modalType: true,
        openTemplatesModal: this.openTemplatesModal$
      }
    };
    this.modalRef = this.modalService.open(BusTemplateCreateComponent, "modal-lg-2x", 'static');
    this.modalRef.componentInstance.modalData = modalData;
    let createTemplateModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        createTemplateModal.unsubscribe();
        this.modalRef.close();
      }); 
  }

  loadSeatingSchemes() {
    this.seatingSchemeService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (seatingSchemes: PaginationResponse<SeatingScheme>) => {
      this.seatingSchemes = seatingSchemes.items;
    }, (error) => {
      console.warn(error);
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }
}