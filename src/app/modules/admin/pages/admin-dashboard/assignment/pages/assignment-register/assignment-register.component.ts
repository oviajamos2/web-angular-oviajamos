import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { DestinationPaymentModalComponent } from 'src/app/shared/modals';
import { ModalService } from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { ShipmentFormComponent } from 'src/app/shared/forms';
import { ShipmentService, Shipping, ShippingStatusEnum } from 'src/app/core/http/shipment';

@Component({
  selector: 'app-assignment-register',
  templateUrl: './assignment-register.component.html',
  styleUrls: ['./assignment-register.component.scss']
})
export class AssignmentRegisterComponent implements OnInit {

  @ViewChild(ShipmentFormComponent) shipmentForm: ShipmentFormComponent;

  modalRef: NgbModalRef;
  submitting: ShippingStatusEnum = null;
  shippingStatusEnum = ShippingStatusEnum;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private authService: AuthService,
    private modalService: ModalService,
    private shipmentService: ShipmentService,
    private notificationService: NotificationService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  destinationPayment() {
    this.modalRef = this.modalService.open(DestinationPaymentModalComponent);
    let destinationPaymentModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result: {advancePayment: number}) => {
        destinationPaymentModalSubs.unsubscribe();
        this.modalRef.close();
      });
  }

  public async submitShipment(shipmentType: ShippingStatusEnum) {
    this.submitting = shipmentType;
    this.shipmentForm.shipmentGroup.disable();
    let shipment: Shipping;
    await this.shipmentForm.onSubmit().then( shipmentForm => {
      shipment = shipmentForm;
    });
    if (shipment) {
      shipment.status = shipmentType;
      this.shipmentService.create(shipment).pipe(take(1)).subscribe( (response: Shipping) => {
        this.submitting = null;
        this.shipmentForm.shipmentGroup.enable();
        this.notificationService.notify(SAVED_NOTIFICATION);
        this.router.navigateByUrl('/dashboard/encomienda/envio-encomiendas');
      }, (error) => {
        console.warn(error);
        this.submitting = null;
        this.shipmentForm.shipmentGroup.enable();
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      });
    } else {
      this.submitting = null;
      this.shipmentForm.shipmentGroup.enable();
    }
  }
}
