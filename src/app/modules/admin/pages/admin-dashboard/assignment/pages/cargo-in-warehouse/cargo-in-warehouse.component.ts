import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { BoxcarShipemntAssifnmentModalComponent } from 'src/app/shared/modals';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { ShipmentAssignmentComponent } from 'src/app/shared/components';
import { ShipmentService, Shipping } from 'src/app/core/http/shipment';

type ShipmentSearchParams = {
  destinationOfficeId?: number
}

@Component({
  selector: 'app-cargo-in-warehouse',
  templateUrl: './cargo-in-warehouse.component.html',
  styleUrls: ['./cargo-in-warehouse.component.scss']
})
export class CargoInWarehouseComponent implements OnInit {

  checkedShipments: Shipping[] = [];
  modalRef: NgbModalRef;
  selectedShipment: Shipping = null;
  shipments: Shipping[] = [];

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  constructor(
    private authService: AuthService,
    private shipmentService: ShipmentService,
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  filterPackages(event: ShipmentSearchParams) {
    this.selectedShipment = null;
    this.checkedShipments = [];
    this.shipments = [];
    this.shipmentService.getAll({
      companyId: this.enterprise.companyId,
      ...event
    }).pipe(
      take(1)
    ).subscribe((response: PaginationResponse<Shipping>) => {
      this.shipments = response.items;
    });
  }

  selectShipment(event: Shipping) {
    this.selectedShipment = event;
  }

  getTotalPackages(shipment: Shipping): number {
    let amount = 0;
    shipment.packages.forEach(pack => {
      amount = amount + parseInt(pack.quantity.toString());
    });
    return amount;
  }

  checkShipment(shipment: Shipping) {
    if(this.checkedShipments.find(a => a.shippingId === shipment.shippingId)) return;
    this.checkedShipments.push(shipment);
    if(this.checkedShipments.length === this.shipments.length) {
      (<any>document.querySelector('.header-package-checkbox')).checked = true
    }
  }

  uncheckShipment(shipment: Shipping) {
    this.checkedShipments.splice(
      this.checkedShipments.findIndex( a => a.shippingId === shipment.shippingId ), 1
    )
  }

  checkAll() {
    this.shipments.forEach(shipment => {
      if(!this.checkedShipments.find(a => a.shippingId === shipment.shippingId)) {
        this.checkedShipments.push(shipment);
      }
    });
  }

  uncheckAll() {
    this.checkedShipments = [];
  }

  assignToBus() {
    let modalData: ModalData = {
      state: { enterprise: this.enterprise }
    };
    this.modalRef = this.modalService.open(ShipmentAssignmentComponent, 'modal-xl', 'static');
    this.modalRef.componentInstance.modalData = modalData;
    let shipemntAssignmentModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result) => {
        shipemntAssignmentModalSubs.unsubscribe();
        this.modalRef.close();
      });
  }

  assignToBoxcar() {
    let modalData: ModalData = {
      state: { enterprise: this.enterprise }
    };
    this.modalRef = this.modalService.open(BoxcarShipemntAssifnmentModalComponent, 'modal-lg', 'static');
    this.modalRef.componentInstance.modalData = modalData;
    let boxCarShipemntAssignmentModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe(
      (result) => {
        boxCarShipemntAssignmentModalSubs.unsubscribe();
        this.modalRef.close();
        if(result) {}
      });
  }
}