import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { AssignmentRoutingModule } from './assignment-routing.module';
import { AssignmentComponent } from './assignment.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {
  AssignmentRegisterComponent,
  CargoInWarehouseComponent
} from './pages';

@NgModule({
  declarations: [
    AssignmentComponent,
    AssignmentRegisterComponent,
    CargoInWarehouseComponent
  ],
  imports: [
    CommonModule,
    AssignmentRoutingModule ,
    SharedModule,
    NgbModule,
    PipesModule
  ]
})
export class AssignmentModule { }
