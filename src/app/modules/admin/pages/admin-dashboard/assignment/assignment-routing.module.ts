import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssignmentComponent } from './assignment.component';

// Pages
import {
  AssignmentRegisterComponent,
  CargoInWarehouseComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'registrar', pathMatch: 'full' },
  {
    path: '',
    component: AssignmentComponent,
    children: [
      { path: 'registrar', component: AssignmentRegisterComponent },
      { path: 'envio-encomiendas', component: CargoInWarehouseComponent }
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AssignmentRoutingModule { }
