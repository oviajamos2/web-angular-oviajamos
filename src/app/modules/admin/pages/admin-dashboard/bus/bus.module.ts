import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BusRoutingModule } from './bus-routing.module';
import { BusComponent } from './bus.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { PipesModule } from 'src/app/core/pipes/pipes.module';

// Shared
import { SharedModule } from 'src/app/shared/shared.module';

// Pages
import {
  BusesComponent,
  BusEditComponent,
  BusCreateComponent
} from './pages';

@NgModule({
  declarations: [
    BusesComponent,
    BusComponent,
    BusEditComponent,
    BusCreateComponent
  ],
  imports: [
    CommonModule,
    BusRoutingModule,
    SharedModule,
    NgbModule,
    TooltipModule,
    PipesModule
  ]
})
export class BusModule { }
