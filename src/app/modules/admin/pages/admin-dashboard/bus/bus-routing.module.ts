import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusComponent } from './bus.component'

// Pages
import {
  BusesComponent,
  BusEditComponent,
  BusCreateComponent
} from './pages'

const routes: Routes = [
  { path: '', redirectTo: 'buses', pathMatch: 'full' },
  {
    path: '',
    component: BusComponent,
    children: [
      { path: 'buses', component: BusesComponent },
      { path: 'edit/:id', component: BusEditComponent },
      { path: 'create', component: BusCreateComponent }
    ],
  },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BusRoutingModule { }
