import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/core/services';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { Bus, BusService } from 'src/app/core/http/bus';
import { BusStatusEnum, BusTypeEnum, SeatingSchemeEnum } from 'src/app/core/http/bus';
import { BusDetailComponent, ConfirmationComponent } from 'src/app/shared/modals';
import { BusnfoModalComponent } from 'src/app/shared/modals/mobile';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationMeta, PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Enterprise } from 'src/app/core/http/enterprise';
import { BusTemplateInfoComponent } from '../../../bus-template/components';
import {
  DELETED_NOTIFICATION,
  RECOVERED_NOTIFICATION,
  SAVED_NOTIFICATION,
  SERVER_ERROR_NOTIFICATION
} from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-buses',
  templateUrl: './buses.component.html',
  styleUrls: ['./buses.component.scss']
})
export class BusesComponent implements OnInit, OnDestroy {

  buses: Bus[] = [];
  busTypeEnum = BusTypeEnum;
  busTypeEnumKeys = Object.keys(this.busTypeEnum);
  busStatusEnum = BusStatusEnum;
  busStatusEnumKeys = Object.keys(this.busStatusEnum);
  colsTable = [
    {name: 'Matrícula', orderBy: true},
    {name: 'Tipo', orderBy: false},
    {name: 'Propietario', orderBy: false},
    {name: 'Asientos', orderBy: false},
    {name: 'Estado', orderBy: false}
  ];
  modalRef: NgbModalRef;
  modalRef2: NgbModalRef;
  modalRef3: NgbModalRef;
  modifying: number[] = [];
  recoveredBus: Subscription;
  loadingBuses = true;
  paginationMeta: PaginationMeta;
  seating = [1, 2];
  seatinSchemeEnum = SeatingSchemeEnum;
  seatinSchemeEnumKeys = Object.keys(this.seatinSchemeEnum);
  selectedBus: Bus = null;

  get enterprise(): Enterprise {
    return this.authService.loggedUser.company;
  }

  get selectedBusImage(): string {
    try {
      return JSON.parse(this.selectedBus?.imageUrl)[0];
    } catch (e) {
      return this.selectedBus?.imageUrl
    }
  }

  constructor(
    private router: Router,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private busService: BusService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadBuses();
    this.subscribeToRecoveredBuses();
  }

  ngOnDestroy(): void {
    this.recoveredBus.unsubscribe();
  }

  loadBuses(page?: number) {
    this.loadingBuses = true;
    this.busService.getAll({companyId: this.enterprise.companyId, page})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Bus>) => {
      if(this.paginationMeta?.currentPage !== response.meta.currentPage) this.selectedBus = null;
      this.loadingBuses = false;
      this.paginationMeta = response.meta;
      this.buses = response.items;
    }, (error) => {
      console.warn(error);
      this.loadingBuses= false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  pageChange(event: number) {
    this.loadBuses(event);
  }

  subscribeToRecoveredBuses() {
    this.recoveredBus = this.busService.recoveredBus$.subscribe( response => {
      this.loadBuses(this.paginationMeta.currentPage);
      if(response) {
        this.notificationService.notify(RECOVERED_NOTIFICATION);
      } else {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
      }
    });
  }

  selectBus(bus: Bus) {
    this.selectedBus = bus;
  }

  public addBus() {
    this.router.navigateByUrl('/dashboard/bus/create');
  }

  public editBus(bus: Bus) {
    this.router.navigateByUrl(`/dashboard/bus/edit/${bus.busId}`, {state: {bus}});
  }

  public deleteBus(bus: Bus) {
    let modalData: ModalData = {
      heading: 'Advertencia',
      content: {
        heading: '¿Desea eliminar?',
        description: `Estas seguro que quieres eliminar el bus con matricula ${bus.licensePlate} de tipo ${bus.type}.`,
        type: 'delete'
      }
    }
    this.modalRef = this.modalService.open(ConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let deleteModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: any) => {
        deleteModal.unsubscribe();
        this.modalRef.close();
        if( result ) {
          this.modifying.push(bus.busId);
          this.busService.delete( bus.busId ).pipe(take(1)).subscribe( response => {
            this.selectedBus = null;
            this.removeFromArray(bus.busId);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bus.busId === id), 1
            );
            this.notificationService.notify({
              ...DELETED_NOTIFICATION,
              objectType: 'bus',
              objectId: bus.busId
            });
            if(this.buses.length < 1 && this.paginationMeta.currentPage > 1 ) {
              this.loadBuses(this.paginationMeta.currentPage - 1);
            } else {
              this.loadBuses(this.paginationMeta.currentPage);
            }
          }, (error) => {
            console.warn(error);
            this.modifying.splice(
              this.modifying.findIndex((id: number) => bus.busId === id), 1
            );
            this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
          });
        }
      })
  }

  removeFromArray(busId: number) {
    this.buses.splice(
      this.buses.findIndex((busInArray: Bus) => busId === busInArray.busId), 1
    );
  }

  setBusStatus(bus: Bus, statusKey: BusStatusEnum) {
    this.modifying.push(bus.busId);
    let busToPatch: Bus = JSON.parse(JSON.stringify(bus));
    busToPatch.status = this.busStatusEnum[statusKey];
    this.busService.patch(busToPatch.busId, busToPatch).pipe(take(1)).subscribe( (response: Bus) => {
      this.buses.find((busInArray: Bus) => response.busId === busInArray.busId).status = response.status;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => bus.busId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
      if(this.modifying.length > 0) return;
      this.loadBuses();
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => bus.busId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
  }

  orderTableBy(index: number) {
    this.colsTable.forEach(col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  showDetail(bus: Bus) {
    let modalData: ModalData = {
      state: {
        bus
      }
    }
    this.modalRef = this.modalService.open(BusnfoModalComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let busInfoModal: Subscription = this.modalRef.componentInstance.action.subscribe(
      (result: [Bus, number]) => {
        busInfoModal.unsubscribe();
        showTemplateModalSubs.unsubscribe();
        showDetailModalSubs.unsubscribe();
        this.modalRef.close();
        switch (result[1]) {
          case 1:
            this.deleteBus(result[0]);
            break;
          case 2:
            this.editBus(result[0]);
            break;
        }
    });
    let showTemplateModalSubs: Subscription = this.modalRef.componentInstance.showTemplate$.subscribe( (bus: Bus) => {
      this.showSeatingScheme(bus);
    });
    let showDetailModalSubs: Subscription = this.modalRef.componentInstance.showDetail$.subscribe( value => {
      this.showBusDetail();
    });
  }

  showSeatingScheme(bus?: Bus) {
    let modalData: ModalData = {
      state: bus?.seatingScheme || this.selectedBus.seatingScheme
    }
    this.modalRef2 = this.modalService.open(BusTemplateInfoComponent, 'modal-lg' );
    this.modalRef2.componentInstance.modalData = modalData;
    let templateModal: Subscription =  this.modalRef2.componentInstance.action.subscribe( (result: any) => {
      this.modalRef2.close();
      templateModal.unsubscribe();
    });
  }

  showBusDetail() {
    let modalData: ModalData = {
      state: {
        bus: this.selectedBus
      }
    }
    this.modalRef3 = this.modalService.open(BusDetailComponent, 'modal-md-2x');
    this.modalRef3.componentInstance.modalData = modalData;
    let busDetailSubs: Subscription =  this.modalRef3.componentInstance.action.subscribe( (result: [number, number]) => {
      if(result) {
        this.modalRef3.close();
        busDetailSubs.unsubscribe();
      } else {
        this.modalRef3.close();
        busDetailSubs.unsubscribe();
      }
    });
  }
}
