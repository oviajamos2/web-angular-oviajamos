import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-section',
  templateUrl: './home-section.component.html',
  styleUrls: ['./home-section.component.scss']
})
export class HomeSectionComponent implements OnInit {

  constructor(
    private router: Router) {}

  ngOnInit(): void {}

  public goTo(namePath: string) {
    switch (namePath) {
      case 'boletero':
        this.router.navigateByUrl('/dashboard/boletero');
        break;
      case 'ruta':
        this.router.navigateByUrl('/dashboard/ruta');
        break;
      case 'chofer':
        this.router.navigateByUrl('/dashboard/chofer');
        break;
      case 'parada':
        this.router.navigateByUrl('/dashboard/parada');
        break;
      case 'bus':
        this.router.navigateByUrl('/dashboard/bus');
        break;
      case 'sale':
        this.router.navigateByUrl('/dashboard/venta');
        break;
      default:
        this.router.navigateByUrl('/');
    }
  }
}