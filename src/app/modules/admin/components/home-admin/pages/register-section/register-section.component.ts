import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/http/user';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Component({
  selector: 'app-register-section',
  templateUrl: './register-section.component.html',
  styleUrls: ['./register-section.component.scss']
})
export class RegisterSectionComponent implements OnInit {

  userOviajamos: boolean = false;

  constructor(private router: Router,  
              private userService: UserService, 
              private localStorageService: LocalStorageService, ) { }

  ngOnInit(): void {
    this.name();
  }

  public name():void{
    const rol = this.localStorageService.getItem('rol');
    (rol == 'Oviajamos')? this.userOviajamos = true: this.userOviajamos = false;
  }

  public goTo(namePath: string) {
    switch (namePath) {
      case 'boletero':
        this.router.navigateByUrl('/dashboard/boletero');
        break;
      case 'ruta':
        this.router.navigateByUrl('/dashboard/ruta');
        break;
      case 'chofer':
        this.router.navigateByUrl('/dashboard/chofer');
        break;
      case 'parada':
        this.router.navigateByUrl('/dashboard/parada');
        break;
      case 'bus':
        this.router.navigateByUrl('/dashboard/bus');
        break;
      case 'sale':
        this.router.navigateByUrl('/dashboard/venta');
        break;
      case 'salida':
        this.router.navigateByUrl('/dashboard/salida');
        break;
      default:
        this.router.navigateByUrl('/');
    }
  }
}