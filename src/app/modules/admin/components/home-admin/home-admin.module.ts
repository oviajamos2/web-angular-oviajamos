import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from 'src/app/shared/shared.module';
import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { HomeAdminComponent } from './home-admin.component';
import { HomeAdminRoutingModule } from './home-admin-routing.module';

import {
  RegisterSectionComponent,
  TotalSalesComponent
} from './pages';

@NgModule({
  declarations: [
    HomeAdminComponent,
    TotalSalesComponent,
    RegisterSectionComponent
  ],
  imports: [
    CommonModule,
    HomeAdminRoutingModule,
    SharedModule,
    PipesModule,
    NgbModule
  ]
})
export class HomeAdminModule { }
