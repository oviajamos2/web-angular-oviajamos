import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/http/user';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: []
})
export class AdminComponent implements OnInit {

  get loggedUser(): User {
    return this.authService.loggedUser;
  }

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit(): void {}
}
