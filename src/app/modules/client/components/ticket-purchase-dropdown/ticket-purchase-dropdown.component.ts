import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { AlertComponent } from 'src/app/shared/modals';
import { Departure, DepartureSearchParams } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { TravelForm2Component } from 'src/app/shared/forms';

@Component({
  selector: 'app-ticket-purchase-dropdown',
  templateUrl: './ticket-purchase-dropdown.component.html',
  styleUrls: ['./ticket-purchase-dropdown.component.scss']
})
export class TicketPurchaseDropdownComponent implements OnInit {

  @ViewChild(TravelForm2Component) travelForm: TravelForm2Component;

  filterDepartureSubs: Subscription;
  modalRef: NgbModalRef
  showPurchaseInfo: boolean;

  constructor(
    private modalService: ModalService,
    public ticketPurchaseService: TicketPurchaseService
  ) {}

  get step(): number {
    return  this.ticketPurchaseService.step;
  }

  get selectedTickets(): number[] {
    return this.ticketPurchaseService.selectedTickets;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  get selectedPositions() {
    return this.ticketPurchaseService.selectedPositions;
  }

  ngOnInit(): void {
    this.initFilterDepartureSubs();
  }

  initFilterDepartureSubs() {
    this.ticketPurchaseService.filterDeparture$.subscribe( (departureSearchParams: DepartureSearchParams) => {
      this.travelForm?.travelGroup.patchValue(departureSearchParams);
    });
  }

  onSubmitTravelForm() {
    const travelForm: DepartureSearchParams = this.travelForm.onSubmit();
    if(travelForm) {
      this.ticketPurchaseService.filterDeparture$.next(travelForm);
    }
    else {
      let modalData: ModalData = {
        heading: 'Advertencia',
        content: {
          description: `Es necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      this.modalRef.componentInstance.action.subscribe( (result: any) => {
        this.modalRef.close();
      });
    }
  }
}