import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { AlertComponent } from 'src/app/shared/modals';
import { DepartureSearchParams } from 'src/app/core/http/departure/models/departure-search-params.interface';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';
import { TravelFormComponent } from 'src/app/shared/forms';

@Component({
  selector: 'app-home-section',
  templateUrl: './home-section.component.html',
  styleUrls: ['./home-section.component.scss']
})
export class HomeSectionComponent implements OnInit {
  
  @ViewChild(TravelFormComponent) travelForm: TravelFormComponent;

  modalRef: NgbModalRef;

  constructor(
    private modalService: ModalService,
    private router: Router,
    private ticketPurchaseService: TicketPurchaseService
  ) {
    this.ticketPurchaseService.resetPaymentData();
  }

  ngOnInit(): void {}

  onSubmit() {
    const travelParams: DepartureSearchParams = this.travelForm.onSubmit();
    if( travelParams ) {
      this.router.navigateByUrl(`/client/home/compra-boleto/1`, {state: {travelParams}});
    } else {
      let modalData: ModalData = {
        content: {
          description: `Es necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      this.modalRef.componentInstance.action.subscribe( (result: any) => {
        this.modalRef.close();
      });
    }
  }
}