import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PipesModule } from 'src/app/core/pipes/pipes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { TicketPurchaseRoutingModule } from './ticket-purchase-routing.module';
import { TooltipModule } from 'ng2-tooltip-directive';

import { TicketPurchaseComponent } from './ticket-purchase.component';

import {
  TravelSelectionComponent,
  SeatSelectionComponent,
  PassengerDataComponent,
  PaymentDataComponent,
  PaymentMethodComponent,
  TicketDownloadComponent,
  PaymentProcessComponent
} from "./pages";

import {
  PurchaseProgressComponent,
  TicketPurchaseDropdownComponent
} from "../../../components";

@NgModule({
  declarations: [
    TicketPurchaseComponent,
    PurchaseProgressComponent,
    TicketPurchaseDropdownComponent,
    TravelSelectionComponent,
    SeatSelectionComponent,
    PassengerDataComponent,
    PaymentDataComponent,
    PaymentMethodComponent,
    TicketDownloadComponent,
    PaymentProcessComponent
  ],
  imports: [
    CommonModule,
    TooltipModule,
    SharedModule,
    PipesModule,
    TicketPurchaseRoutingModule
  ]
})
export class TicketPurchaseModule { }
