import { AfterViewInit, Component, OnInit } from '@angular/core';
import { asapScheduler } from 'rxjs';

import { Departure } from 'src/app/core/http/departure';
import { PaymentMethodTypeEnum } from 'src/app/core/http/payment-method';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

type CashPayment = {
  amountPaid: number;
  amountReturn: number;
}
@Component({
  selector: 'app-payment-process',
  templateUrl: './payment-process.component.html',
  styleUrls: ['./payment-process.component.scss']
})
export class PaymentProcessComponent implements OnInit, AfterViewInit {

  paymentMethodTypeEnum = PaymentMethodTypeEnum;
  paymentMethodTypeEnumKeys = Object.keys(this.paymentMethodTypeEnum);

  constructor(
    public ticketPurchaseService: TicketPurchaseService
  ) {
    this.ticketPurchaseService.step = 5;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  get ticketPurchaseForm() {
    return this.ticketPurchaseService.ticketPurchaseGroup.controls;
  }

  get paymentMethod(): PaymentMethodTypeEnum {
    return this.ticketPurchaseForm.type.value;
  }

  get paymentError(): boolean {
    return this.ticketPurchaseService.paymentError;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    asapScheduler.schedule(() => this.ticketPurchaseService.reloadSelectedDeparture());
  }

  verifyPayment(event) {
    this.ticketPurchaseService.steper$.next(true);
  }

  downloadQr(event) {}

  payWithCard(event) {
    this.ticketPurchaseService.steper$.next(true);
  }
  
  async submitPayment(event: CashPayment) {
    this.ticketPurchaseService.loading = true;
    this.ticketPurchaseForm.amountPaid.setValue(this.ticketPurchaseService.selectedPositions.length * this.selectedDeparture.price);
    this.ticketPurchaseService.submitPayment();
  }

  retry(event) {
    this.ticketPurchaseService.paymentError = false;
  }
}