import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { asapScheduler } from 'rxjs';

import { AlertComponent } from 'src/app/shared/modals';
import { Departure } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { MultiPassengerDataFormComponent } from 'src/app/shared/forms';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-passenger-data',
  templateUrl: './passenger-data.component.html',
  styleUrls: ['./passenger-data.component.scss']
})
export class PassengerDataComponent implements OnInit, AfterViewInit {

  @ViewChild(MultiPassengerDataFormComponent) multiPassengerData: MultiPassengerDataFormComponent;

  modalRef: NgbModalRef;

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  get selectedPositions(): number[] {
    return this.ticketPurchaseService.selectedPositions;
  }

  get ticketsPaidFormArray(): FormArray {
    return this.ticketPurchaseService.ticketsPaidArray;
  }

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private modalService: ModalService
  ) {
    this.ticketPurchaseService.step = 3;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    asapScheduler.schedule(() => this.ticketPurchaseService.reloadSelectedDeparture());
  }

  onSubmit(event) {
    const passengerData: FormArray = this.multiPassengerData.onSubmit();
    if(passengerData) {
      this.ticketPurchaseService.steper$.next(true);
      this.ticketPurchaseService.savePurchaseDataOnStorage();
    } else {
      let modalData: ModalData = {
        content: {
          description: `Es necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      this.modalRef.componentInstance.action.subscribe( (result: any) => {
        this.modalRef.close();
      });
    }
  }
}