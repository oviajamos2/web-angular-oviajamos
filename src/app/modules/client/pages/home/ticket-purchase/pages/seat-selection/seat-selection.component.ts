import { Component, OnInit } from '@angular/core';
import { FormArray } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { asapScheduler } from 'rxjs';

import { AlertComponent } from 'src/app/shared/modals';
import { Departure } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { SeatingSchemeEnum } from 'src/app/core/http/bus';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-seat-selection',
  templateUrl: './seat-selection.component.html',
  styleUrls: ['./seat-selection.component.scss']
})
export class SeatSelectionComponent implements OnInit {

  modalRef: NgbModalRef;
  seatingSchemeEnum = SeatingSchemeEnum;
  seatingSchemeEnumKeys = Object.keys(this.seatingSchemeEnum);
  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private modalService: ModalService
  ) {
    this.ticketPurchaseService.step = 2;
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get ticketPurchaseForm() {
    return this.ticketPurchaseService.ticketPurchaseForm;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  get selectedTickets(): number[] {
    return this.ticketPurchaseService.selectedTickets;
  }

  get selected(): string {
    if(this.selectedTickets.length == 0) {
      return 'Ninguno';
    } else {
      return this.ticketPurchaseService.selectedPositions.toString().replace(/,/g, ', ');
    }
  }

  get tickets(): Ticket[] {
    return this.ticketPurchaseService.selectedDeparture.tickets;
  }

  get ticketsPaidArray(): FormArray {
    return this.ticketPurchaseService.ticketsPaidArray;
  }

  ngOnInit(): void {}
  
  ngAfterViewInit(): void {
    asapScheduler.schedule(() => this.ticketPurchaseService.reloadSelectedDeparture());
  }

  selectSeat(event: Ticket) {
    this.ticketPurchaseService.addTicketForm(event);
  }

  unselectSeat(event: Ticket) {
    let unselectedSeat = (<Ticket[]> this.ticketPurchaseService.ticketsPaidArray.value)
      .map((ticket: Ticket) => ticket.position)
      .indexOf(event.position);
    this.ticketsPaidArray.removeAt(unselectedSeat);
    this.ticketPurchaseService.sortTicketsPaid();
  }

  onSubmit() {
    if(this.selectedTickets.length > 0) {
      this.ticketPurchaseService.steper$.next(true);
      this.ticketPurchaseService.savePurchaseDataOnStorage();
    } else {
      let modalData: ModalData = {
        content: {
          description: `Selecciona tus asientos para viajar`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      this.modalRef.componentInstance.action.subscribe( (result: any) => {
        this.modalRef.close();
      });
    }
  }
}