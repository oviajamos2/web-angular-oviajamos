import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { asapScheduler, Subscription } from 'rxjs';
import { auditTime } from 'rxjs/operators';
import * as moment from 'moment';

import { Departure, DepartureService, DepartureSearchParams } from 'src/app/core/http/departure';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-travel-selection',
  templateUrl: './travel-selection.component.html',
  styleUrls: []
})
export class TravelSelectionComponent implements OnInit, OnDestroy, AfterViewInit {

  filterDepartureSubs: Subscription;

  get filteredDepartures(): Departure[] {
    return this.ticketPurchaseService.filteredDepartures;
  }

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private departureService: DepartureService
  ) {
    this.ticketPurchaseService.resetPaymentData();
    this.ticketPurchaseService.step = 1;
    this.ticketPurchaseService.loading = true;
  }

  ngOnInit(): void {
    this.initFilterDeparturesSubs();
    this.ticketPurchaseService.clearPurchaseDataFromStorage();
  }
  
  ngAfterViewInit(): void {
    asapScheduler.schedule(() => {
      if(this.ticketPurchaseService.departureSearchParams) {
        this.ticketPurchaseService.filterDeparture$.next(this.ticketPurchaseService.departureSearchParams);
      } else {
        if(history.state.travelParams) {
          this.ticketPurchaseService.filterDeparture$.next(history.state.travelParams);
        } else {
          this.loadDefaultDepartures();
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.filterDepartureSubs?.unsubscribe();
  }

  initFilterDeparturesSubs() {
    this.filterDepartureSubs = this.ticketPurchaseService.filterDeparture$.pipe(
      auditTime(100)
    ).subscribe((searchParams: DepartureSearchParams) => {
      this.ticketPurchaseService.loading = true;
      this.ticketPurchaseService.departureSearchParams = searchParams;
      this.departureService.getAll(searchParams).subscribe( (response: PaginationResponse<Departure>) => {
        this.ticketPurchaseService.loading = false;
        this.ticketPurchaseService.filteredDepartures = response.items;
      }, (error) => {
        console.warn(error)
        this.ticketPurchaseService.loading = false;
      });
    });
  }

  loadDefaultDepartures() {
    this.ticketPurchaseService.loading = true;
    this.departureService.getAll({
      departureDate: moment().format('YYYY-MM-DD'),
      availableSeats: 1,
      onSale: true
    }).subscribe( (response: PaginationResponse<Departure>) => {
      this.ticketPurchaseService.filteredDepartures = response.items;
      this.ticketPurchaseService.loading = false;
    }, (error) => {
      console.warn(error);
      this.ticketPurchaseService.loading = false;
    })
  }

  selectTravel(event: Departure) {
    this.ticketPurchaseService.selectedDeparture = event;
    this.ticketPurchaseService.steper$.next(true);
    this.ticketPurchaseService.savePurchaseDataOnStorage();
  }
}