import { Component, OnInit } from '@angular/core';
import { RecaptchaComponent } from 'ng-recaptcha';

import { Departure } from 'src/app/core/http/departure';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

RecaptchaComponent.prototype.ngOnDestroy = function() {
  if (this.subscription) {
    this.subscription.unsubscribe();
  }
}

@Component({
  selector: 'app-ticket-purchase',
  templateUrl: './ticket-purchase.component.html',
  styleUrls: ['./ticket-purchase.component.scss']
})
export class TicketPurchaseComponent implements OnInit {

  constructor(
    public ticketPurchaseService: TicketPurchaseService
  ) {}

  get step(): number {
    return this.ticketPurchaseService.step;
  }

  get filteredDepartures(): Departure[] {
    return this.ticketPurchaseService.filteredDepartures;
  }

  ngOnInit(): void {}
}