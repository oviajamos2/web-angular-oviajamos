import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module'
import { SharedModule } from 'src/app/shared/shared.module';

import { HomeComponent } from './pages';
import { ClientComponent } from './client.component';

@NgModule({
  declarations: [
    HomeComponent,
    ClientComponent
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    SharedModule
  ]
})
export class ClientModule { }