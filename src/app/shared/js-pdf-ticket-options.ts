import { jsPDFOptions } from "jspdf";

export const INVOICE_TICKET: jsPDFOptions = {
  orientation: "portrait",
  unit: "pt",
  format: [226.7717, 841.8898]
}

export const QUOTE_TICKET: jsPDFOptions = {
  orientation: "portrait",
  unit: "mm",
  format: [80, 148.3]
}

export const MANIFEST: jsPDFOptions = {
  orientation: "portrait",
  unit: 'pt',
  format: [590.276, 772]
}