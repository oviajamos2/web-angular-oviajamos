import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-reserved-seat-warning',
  templateUrl: './reserved-seat-warning.component.html',
  styleUrls: ['./reserved-seat-warning.component.scss']
})
export class ReservedSeatWarningComponent implements OnInit {

  action = new Subject();

  constructor() {}

  ngOnInit(): void {}
}