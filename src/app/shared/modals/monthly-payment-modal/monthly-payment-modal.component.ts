import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

enum MonthlyPaymentTypeEnum {
  WIRE_TRANSFER = 'Tranferencia bancaria',
  DEPOSIT = 'Depósito'
}

@Component({
  selector: 'app-monthly-payment-modal',
  templateUrl: './monthly-payment-modal.component.html',
  styleUrls: ['./monthly-payment-modal.component.scss']
})
export class MonthlyPaymentModal implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  monthlyPaymentTypeSelected: MonthlyPaymentTypeEnum = MonthlyPaymentTypeEnum.WIRE_TRANSFER;
  monthlyPaymentTypeEnum = MonthlyPaymentTypeEnum;
  monthlyPaymentTypeEnumKeys = Object.keys(this.monthlyPaymentTypeEnum);

  constructor() {}

  ngOnInit(): void {}

  onSubmit() {}

  setMonthlyPaymentType(type: MonthlyPaymentTypeEnum) {
    this.monthlyPaymentTypeSelected = this.monthlyPaymentTypeEnum[type];
  }
}