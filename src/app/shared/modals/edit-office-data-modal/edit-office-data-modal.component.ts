import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { Enterprise } from 'src/app/core/http/enterprise';
import { Office } from 'src/app/core/http/office';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { OfficeDataFormComponent } from '../../forms/enterprise-steps/office-data-form/office-data-form.component';

@Component({
  selector: 'app-edit-office-data-modal',
  templateUrl: './edit-office-data-modal.component.html',
  styleUrls: ['./edit-office-data-modal.component.scss']
})
export class EditOfficeDataModalComponent implements OnInit, OnDestroy {

  @Input() modalData: ModalData;
  @ViewChild(OfficeDataFormComponent) officeDataForm: OfficeDataFormComponent
  
  action = new Subject();
  submitting$ = new Subject();
  submittingSubs: Subscription;
  submitting: boolean = false;

  get office(): Office {
    return this.modalData.state.office;
  }

  get enterprise(): Enterprise {
    return this.modalData.state.enterprise;
  }

  constructor() {}

  ngOnInit(): void {
    this.initSubmittingSubs();
  }

  ngOnDestroy(): void {
    this.submittingSubs?.unsubscribe();
  }

  initSubmittingSubs() {
    this.submittingSubs = this.submitting$.subscribe( (value: boolean) => {
      this.submitting = value;
    });
  }

  async onSubmit() {
    let office;
    await this.officeDataForm.onSubmit().then( (officeForm: Office) => {
      office = officeForm;
    });
    if(office) {
      this.action.next(office);
    } else {
      this.resolveForm();
    }
  }

  resolveForm() {
    this.submitting = false;
    this.officeDataForm.officeGroup.enable();
  }
}