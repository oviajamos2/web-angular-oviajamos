import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, Subscription, fromEvent } from 'rxjs';
import { auditTime, debounceTime, distinctUntilChanged, take, pluck } from 'rxjs/operators';

import { ALERT_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { Bus, BusService } from 'src/app/core/http/bus';
import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Office, OfficeService } from 'src/app/core/http/office';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-boxcar-shipemnt-assifnment-modal',
  templateUrl: './boxcar-shipemnt-assifnment-modal.component.html',
  styleUrls: ['./boxcar-shipemnt-assifnment-modal.component.scss']
})
export class BoxcarShipemntAssifnmentModalComponent implements OnInit, AfterViewInit, OnDestroy {

  action = new Subject();
  buses: Bus[] = [];
  busSelectSubs: Subscription;
  boxcarAssignmentGroup: FormGroup;
  departmentEnum = DepartmentEnum;
  departmentEnumKeys = Object.keys(this.departmentEnum);
  destinationDepartment: DepartmentEnum;
  destinationDepartmentSubs: Subscription;
  destinationOffices: Office[] = [];
  destinationOfficeSelectSubs: Subscription;
  loadingBuses:boolean = false;
  loadingDestinationOffices: boolean = false;
  modalData: ModalData;
  submitted: boolean = false;

  get boxcarAssignmentForm() {
    return this.boxcarAssignmentGroup.controls;
  }

  get enterprise(): Enterprise {
    return this.modalData?.state?.enterprise;
  }

  constructor(
    private formBuilder: FormBuilder,
    private officeService: OfficeService,
    private notificationService: NotificationService,
    private busService: BusService
    ) {}

  ngOnInit(): void {
    this.buildForm();
    this.loadBuses();
  }

   ngAfterViewInit(): void {
     this.initDeparmentSubs();
     this.initBusSelectSubs();
   }

  ngOnDestroy(): void {
    this.destinationDepartmentSubs?.unsubscribe();
  }

  buildForm() {
    this.boxcarAssignmentGroup = this.formBuilder.group({
      destinationDepartment: ['', [Validators.required]],
      destinationOfficeId: ['', [Validators.required]],
      licensePlate: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.submitted = true;
    if(this.boxcarAssignmentGroup.valid) {
      this.action.next(this.boxcarAssignmentGroup.value);
    }
  }

  initDeparmentSubs() {
    this.destinationDepartmentSubs = this.boxcarAssignmentForm.destinationDepartment.valueChanges.pipe(
      auditTime(100),
      distinctUntilChanged()
    ).subscribe((value: DepartmentEnum) => {
      this.boxcarAssignmentForm.destinationOfficeId.setValue('');
      if(value?.length > 0) {
        this.destinationDepartment = value;
        this.loadDestinationOffices();
      } else {
        this.destinationDepartment = null;
        this.destinationOffices = [];
      }
    });
  }

  loadDestinationOffices() {
    this.officeService.getAll(
      {
        department: this.destinationDepartment,
        companyId: this.enterprise.companyId
      },
      true
    ).pipe(
      take(1)
    ).subscribe((response: PaginationResponse<Office>) => {
      this.loadingDestinationOffices = false;
      this.destinationOffices = response.items;
      if(this.destinationOffices.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: `Se requiere de oficinas en ${ this.destinationDepartment } para continuar`,
          link: ['Crear oficinas', '/dashboard/oficina/create']
        });
      }
    }, (error) => {
      console.warn(error);
      this.loadingDestinationOffices = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initDestinationOfficeSelectSubs() {
    this.destinationOfficeSelectSubs = fromEvent(document.querySelectorAll('.destination-office-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.officeService.getAll(
        {
          search: value,
          companyId: this.enterprise.companyId,
          department: this.destinationDepartment
        },
        true
      ).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.destinationOffices = response.items;
      })
    });
  }

  loadBuses() {
    this.loadingBuses = true;
    this.busService.getAll({ companyId: this.enterprise.companyId}, true ).pipe(
      take(1)
    ).subscribe((response: PaginationResponse<Bus>) => {
      this.loadingBuses = false;
      this.buses = response.items;
      if(this.buses.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: `Se requiere de buses o furgones para continuar`,
          link: ['Crear buses', '/dashboard/bus/create']
        });
      }
    }, (error) => {
      console.warn(error);
      this.loadingBuses = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initBusSelectSubs() {
    this.destinationOfficeSelectSubs = fromEvent(document.querySelectorAll('.bus-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busService.getAll({ search: value, companyId: this.enterprise.companyId }, true ).pipe(
        take(1)
      ).subscribe( (response: PaginationResponse<Bus>) => {
        this.buses = response.items;
      })
    });
  }
}
