import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { asapScheduler, Subject } from 'rxjs';

import { MAX_IMAGE_SIZE } from 'src/app/shared/constants';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { Bus } from 'src/app/core/http/bus';

@Component({
  selector: 'app-bus-upload-image',
  templateUrl: './bus-upload-image.component.html',
  styleUrls: ['./bus-upload-image.component.scss']
})
export class BusUploadImageComponent implements OnInit {
  
  @Input('busGroup') busGroup: FormGroup;
  @Input('imageFile') imageFile: File;
  @Input('imageFile2') imageFile2: File;
  @Input('editMode') editMode: boolean;
  
  action = new Subject();
  defaultImage = null;
  defaultImage2 = null;
  excededImageSize: boolean = false;
  excededImageSize2: boolean = false;
  modalData: ModalData;
  stateButton:boolean = false;

  get busForm() {
    return this.busGroup?.controls;
  }

  get bus(): Bus {
    return this.modalData?.state.bus;
  }

  constructor() {}

  ngOnInit(): void {}
  
  ngAfterViewInit(): void {
    asapScheduler.schedule(() => this.patchImages());
  }

  async patchImages(): Promise<void> {
    if(this.bus && !this.editMode) {
      this.editMode = true;
      if(this.busForm.imageUrl) {
        try {
          this.defaultImage = JSON.parse(this.bus.imageUrl)[0];
          this.defaultImage2 = JSON.parse(this.bus.imageUrl)[1];
        } catch (error) {
          this.defaultImage = this.bus.imageUrl;
        }
      }
    }
    if(this.imageFile) {
      const reader = new FileReader();
      this.setImage({ target: { files: [this.imageFile] } });
        reader.readAsDataURL(this.imageFile);
        reader.onload = () => {
          this.defaultImage = reader.result as string;
        };
    }
    if(this.imageFile2) {
      const reader = new FileReader();
      this.setImage2({ target: { files: [this.imageFile2] } });
      reader.readAsDataURL(this.imageFile2);
      reader.onload = () => {
        this.defaultImage2 = reader.result as string;
      };
    }
  }

  onSubmit() {
    this.action.next([this.imageFile, this.imageFile2]);
  }

  public setImage(event) {
    this.excededImageSize = false;
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.defaultImage = null;
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.defaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.defaultImage = null;
    }
    if(this.editMode) {
      this.editMode = false;
    }
  }

  public setImage2(event) {
    this.excededImageSize2 = false;
    const reader = new FileReader();
    const file2: File = event.target.files[0];
    if(file2) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file2.size > limitSize) {
        this.imageFile2 = null;
        this.defaultImage2 = null;
        this.excededImageSize2 = true;
      } else {
        this.imageFile2 = file2;
        reader.readAsDataURL(file2);
        reader.onload = () => {
          this.defaultImage2 = reader.result as string;
        };
        this.excededImageSize2 = false;
      }
    } else {
      this.imageFile2 = null;
      this.defaultImage2 = null;
    }
    if(this.editMode) {
      this.editMode = false;
    }
  }

}
