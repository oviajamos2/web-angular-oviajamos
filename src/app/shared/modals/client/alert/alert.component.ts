import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  action = new Subject();

  modalData: ModalData;

  get heading(): string {
    return this.modalData.heading;
  }
  get content(): any {
    return this.modalData.content;
  }

  constructor() { }

  ngOnInit(): void {
  }

  cancel() {
    this.action.next(false)
  }

  acepted() {
    this.action.next(true)
  }
  
}
