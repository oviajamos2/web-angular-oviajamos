import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';

@Component({
  selector: 'app-destination-payment-modal',
  templateUrl: './destination-payment-modal.component.html',
  styleUrls: ['./destination-payment-modal.component.scss']
})
export class DestinationPaymentModalComponent implements OnInit {

  action = new Subject();
  floatInputValidator;
  destinationPaymentGroup: FormGroup;
  state;
  submitted: boolean;

  constructor( private formBuilder: FormBuilder ) {
    this.floatInputValidator = floatInputValidator;
  }

  get destinationPaymentForm() {
    return this.destinationPaymentGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.destinationPaymentGroup = this.formBuilder.group({
      advancePayment: ['', Validators.compose([Validators.required])]
    });
  }

  submitPayment() {
    this.submitted = true;
    if(this.destinationPaymentGroup.valid) {
      this.action.next(this.destinationPaymentGroup.value);
    }
  }

  refreshValidity() {
    this.destinationPaymentGroup.updateValueAndValidity();
  }
}