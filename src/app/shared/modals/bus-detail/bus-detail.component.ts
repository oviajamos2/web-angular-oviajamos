import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Bus } from 'src/app/core/http/bus';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-bus-detail',
  templateUrl: './bus-detail.component.html',
  styleUrls: ['./bus-detail.component.scss']
})
export class BusDetailComponent implements OnInit {

  action = new Subject();
  modalData: ModalData;

  get bus(): Bus {
    return this.modalData?.state.bus;
  }

  get busImage(): string {
    try {
      return JSON.parse(this.bus?.imageUrl)[0];
    } catch (e) {
      return this.bus?.imageUrl;
    }
  }

  get busImage2(): string {
    try {
      return JSON.parse(this.bus?.imageUrl)[1];
    } catch (e) {
      return null;
    }
  }

  constructor() {}

  ngOnInit(): void {}
}