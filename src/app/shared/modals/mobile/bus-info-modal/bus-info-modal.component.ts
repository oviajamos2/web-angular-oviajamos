import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Bus, BusStatusEnum } from 'src/app/core/http/bus';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-bus-info-modal',
  templateUrl: './bus-info-modal.component.html',
  styleUrls: ['./bus-info-modal.component.scss']
})
export class BusnfoModalComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  showTemplate$ = new Subject();
  showDetail$ = new Subject();
  busStatusEnum = BusStatusEnum;

  get bus(): Bus {
    return this.modalData.state.bus;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteBus(bus: Bus) {
    this.action.next([bus, 1]);
  }

  editBus(bus: Bus) {
    this.action.next([bus, 2]);
  }

  showTemplate() {
    this.showTemplate$.next(this.bus);
  }

  showDetail() {
    this.showDetail$.next(true);
  }
}