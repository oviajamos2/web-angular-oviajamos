import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-bus-template',
  templateUrl: './bus-template.component.html',
  styleUrls: ['./bus-template.component.scss']
})
export class BusTemplateComponent implements OnInit {

  @Input() modalData: SeatingScheme;
  action = new Subject();


  constructor(private router: Router) { }

  ngOnInit() {
  }
  get busDetaill():SeatingScheme {
    return this.modalData;
  }

  showBus():void {
    this.action.next('show');
  }

  deleteSeatingScheme():void {
    this.action.next('delete');
  }

  editSeatingScheme():void { 
    this.action.next('edit');
  }

  closseModal():void {
    this.action.next('closse');
  }

}
