import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { DepartureStatusEnum } from 'src/app/core/http/departure';

@Component({
  selector: 'app-departure-status',
  templateUrl: './departure-status.component.html',
  styleUrls: []
})
export class DepartureStatusComponent implements OnInit {

  action = new Subject();
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);

  constructor() {}

  ngOnInit(): void {}

  setStatus(statusKey) {
    this.action.next(statusKey);
  }
}