import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Bank } from 'src/app/core/http/bank';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.scss']
})
export class BankInfoComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get bank(): Bank {
    return this.modalData.state.bank;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteBank(bank: Bank) {
    this.action.next([bank, 1]);
  }

  editBank(bank: Bank) {
    this.action.next([bank, 2]);
  }
}