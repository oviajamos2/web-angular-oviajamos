import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Driver } from 'src/app/core/http/driver';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-driver-info-modal',
  templateUrl: './driver-info-modal.component.html',
  styleUrls: ['./driver-info-modal.component.scss']
})
export class DriverInfoModalComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get driver(): Driver {
    return this.modalData.state.driver;
  }

  constructor() {}

  ngOnInit(): void {}

  deleteDriver(driver: Driver) {
    this.action.next([driver, 1]);
  }

  editDriver(driver: Driver) {
    this.action.next([driver, 2]);
  }
}