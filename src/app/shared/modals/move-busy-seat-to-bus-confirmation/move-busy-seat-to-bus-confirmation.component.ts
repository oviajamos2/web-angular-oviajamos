import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Departure } from 'src/app/core/http/departure';
import { Ticket } from 'src/app/core/http/ticket';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-move-busy-seat-to-bus-confirmation',
  templateUrl: './move-busy-seat-to-bus-confirmation.component.html',
  styleUrls: ['./move-busy-seat-to-bus-confirmation.component.scss']
})
export class MoveBusySeatToBusConfirmationComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get actualDeparture(): Departure {
    return this.modalData?.state.actualDeparture;
  }

  get selectedDeparture(): Departure {
    return this.modalData?.state.selectedDeparture;
  }

  get selectedTicket(): Ticket {
    return this.modalData?.state.selectedTicket;
  }

  get newTicket(): Ticket {
    return this.modalData?.state.newTicket;
  }

  constructor() {}

  ngOnInit(): void {}
}