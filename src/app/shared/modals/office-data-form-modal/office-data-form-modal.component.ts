import { Component, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

import { Enterprise } from 'src/app/core/http/enterprise';
import { ModalData } from '../../../core/interfaces/modal-data.interface';
import { Office } from 'src/app/core/http/office';
import { OfficeDataFormComponent } from '../../forms/enterprise-steps/office-data-form/office-data-form.component';

@Component({
  selector: 'app-office-data-form-modal',
  templateUrl: './office-data-form-modal.component.html',
  styleUrls: ['./office-data-form-modal.component.scss']
})
export class OfficeDataFormModal implements OnInit, OnDestroy {

  @Input('office') office: Office;
  @Input('enterprise') enterprise: Enterprise;
  @ViewChild(OfficeDataFormComponent) officeDataForm: OfficeDataFormComponent;

  action = new Subject();
  submitted: boolean;
  submitting$ = new Subject();
  submittingSubs: Subscription;
  submitting: boolean;

  constructor() {}

  ngOnInit(): void {
    this.submittingSubs = this.submitting$.subscribe( (value: boolean) => {
      this.submitting = value;
    });
  }

  ngOnDestroy(): void {
    this.submittingSubs.unsubscribe();
  }

  async setValuesLikeModal(modalData: ModalData) {
    this.office = modalData.state.office;
    this.enterprise = modalData.state.enterprise;
  }

  async onSubmit() {
    let officeForm: Office;
    await this.officeDataForm.onSubmit().then((form: Office) => {
      officeForm = form;
    });
    if(officeForm) {
      this.submitting$.next(true);
      officeForm.dosage.dosageKey = encodeURIComponent(officeForm.dosage.dosageKey);
      if(!officeForm.officeId) {
        this.action.next([officeForm, 1]);
      } else {
        this.action.next([officeForm, 2]);
      }
    } else {
      this.officeDataForm.officeGroup.enable();
    }
  }
}