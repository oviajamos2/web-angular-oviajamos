import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Departure } from 'src/app/core/http/departure';
import { Ticket } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-move-busy-seat-confirmation',
  templateUrl: './move-busy-seat-confirmation.component.html',
  styleUrls: ['./move-busy-seat-confirmation.component.scss']
})
export class MoveBusySeatConfirmationComponent implements OnInit {

  @Input('action') action: Subject<boolean>;
  @Input('departure') departure: Departure;
  @Input('selectedTicket') selectedTicket: Ticket;
  @Input('newTicket') newTicket: Ticket;

  constructor() {}

  ngOnInit(): void {}
}