import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-sale-confirmation',
  templateUrl: './sale-confirmation.component.html',
  styleUrls: ['./sale-confirmation.component.scss']
})
export class SaleConfirmationComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();

  get content() {
    return this.modalData.content;
  }

  constructor() {}

  ngOnInit(): void {}

  submitSale() {
    this.action.next(true);
  }
}