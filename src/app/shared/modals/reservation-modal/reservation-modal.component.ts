import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { defaultTimeConf } from 'src/app/core/interfaces/datepicker-config.interface';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import * as moment from 'moment';
import { toModel } from 'src/app/core/helpers/ngbTimeStruct';

@Component({
  selector: 'app-reservation-modal',
  templateUrl: './reservation-modal.component.html',
  styleUrls: ['./reservation-modal.component.scss']
})
export class ReservationModalComponent implements OnInit {

  action = new Subject();
  datePickerTimeConf = defaultTimeConf;
  floatInputValidator;
  reserveGroup: FormGroup;
  state;
  submitted: boolean = false;
  minTime;

  constructor( private formBuilder: FormBuilder ) {
    this.floatInputValidator = floatInputValidator;
    this.minTime = moment().format('HH:mm');
  }

  get reserveForm() {
    return this.reserveGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.reserveGroup = this.formBuilder.group({
      advancePayment: ['', Validators.compose([Validators.required])],
      timeLimit: ['', [Validators.required]]
    });
  }

  submitReserve() {
    this.submitted = true;
    if(this.reserveGroup.valid) {
      let submitForm = this.reserveGroup.value;
      submitForm.timeLimit = toModel(submitForm.timeLimit);
      this.action.next(submitForm);
    }
  }

  refreshValidity() {
    this.reserveGroup.updateValueAndValidity();
  }
}