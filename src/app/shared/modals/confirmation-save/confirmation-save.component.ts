import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-confirmation-save',
  templateUrl: './confirmation-save.component.html',
  styleUrls: ['./confirmation-save.component.scss']
})
export class ConfirmationSaveComponent implements OnInit {

  title: string = "Advertencia";
  action = new Subject();

  constructor() { }

  ngOnInit(): void {
  }

  cancel() {
    this.action.next(false)
  }

  acepted() {
    this.action.next(true)
  }
}
