export const DATE_FORMAT = 'DD/MM/YYYY';
export const ISO_8601_FORMAT = 'YYYY-MM-DDTHH:mm:ss.sssZ';

export const CLIENT_SOCKET_RESERVING_EVENT = 'reserving';
export const SERVER_SOCKET_RESERVING_EVENT = 'reserving';
export const CLIENT_SOCKET_SELLING_EVENT = 'selling';
export const SERVER_REFRESH_EVENT = 'refresh';
export const CLIENT_SOCKET_CANCELLING_EVENT = 'cancel';
export const CLIENT_SOCKET_RESTART_TICKETS_EVENT = 'restart_tickets';

export const MAX_IMAGE_SIZE = 2 * 1024 * 1024;

export const ITEMS_PER_PAGE = 10;