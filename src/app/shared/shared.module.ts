import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { RecaptchaModule } from 'ng-recaptcha';
import { DpDatePickerModule } from 'ng2-date-picker';
import { NotifierModule } from 'angular-notifier';
import { TooltipModule } from 'ng2-tooltip-directive';
import { PipesModule } from '../core/pipes/pipes.module';

import { notifierOptions } from 'src/app/core/interfaces/notifier-options.interface';
import 'moment/locale/es';

// component
import {
  FooterComponent,
  NavbarComponent,
  ModalComponent,
  SidebarComponent,
  AdminFooterComponent,
  AssignedDriverComponent,
  DeparturesTopBarComponent,
  NotifierComponent,
  ErrorPageComponent,
  ClientNavbarComponent,
  SellerTravelCardComponent,
  ClientTravelCardComponent,
  TicketPurchaseTravelCardComponent,
  PaymentMethodCardComponent,
  Page404Component,
  QrScanComponent,
  CardPaymentComponent,
  PaymentErrorComponent,
  SuccessfulPaymentComponent,
  CashPaymentComponent,
  TicketDetailComponent,
  NotFoundComponent,
  SpinnerComponent,
  SpinnerSmallComponent,
  SalesReportTopBarComponent,
  DepartureSalesFilterComponent,
  SaleSeatingSchemeComponent,
  PassengerReservationInfoComponent,
  SalesTableComponent,
  ReservationTableComponent,
  SeatingSchemeComponent,
  ConfirmSalesTableComponent,
  PrintSaleComponent,
  FilteredSaleDeparturesTableComponent,
  MoveSeatComponent,
  MoveSeatToBusComponent,
  MoveToBusSeatSelectionComponent,
  SeatingSchemeSeatSelectionComponent,
  DosageOfficesTableComponent,
  ClientSeatingSchemeComponent,
  DeparturesTableComponent,
  QrScannerComponent,
  SteperProgressComponent,
  SteperActionsComponent,
  AssignmentDetailComponent,
  WarehouseOfficesFilterComponent,
  WarehousePackagesTableComponent,
  WorkingPageComponent,
  ShipmentAssignmentComponent,
  ShipmentAssignmentTableComponent
 } from './components';

// forms
import {
  TicketHolderFormComponent,
  TravelRouteFormComponent,
  AdminLoginFormComponent,
  AuxiliarContactFormComponent,
  BusStopFormComponent,
  DriverFormComponent,
  BusFormComponent,
  BusPropietaryFormComponent,
  EventualDepartureFormComponent,
  ExpressDepartureFormComponent,
  RecurrentDepartureFormComponent,
  DepartureRecurrencyComponent,
  DriverInfoFormComponent,
  DriverLicenseFormComponent,
  DocumentFormComponent,
  TravelFormComponent,
  TravelForm2Component,
  PassengerDataFormComponent,
  MultiPassengerDataFormComponent,
  PaymentDataFormComponent,
  CardPaymentFormComponent,
  CashPaymentFormComponent,
  OfficeAdminFormComponent,
  EnterpriseOldFormComponent,
  OfficeAdminInfoFormComponent,
  DepartureFormComponent,
  LegalOrganizerFormComponent,
  BusTemplateFormComponent,
  SeatingSchemeFormComponent,
  OfficeOldFormComponent,
  OfficeDataOldFormComponent,
  EnterpriseAdminOldFormComponent,
  EnterpriseSettingsFormComponent,
  PassengerNameFilterComponent,
  PassengerDocNumberFilterComponent,
  PhysicalCommissionFormComponent,
  OnlineCommissionFormComponent,
  BankAccountFormComponent,
  EnterpriseInfoFormComponent,
  EnterpriseAdminFormComponent,
  EnterpriseCommissionFormComponent,
  OfficeDataFormComponent,
  DosageFormComponent,
  FormInfoAdminComponent,
  FormsChangePasswordComponent,
  FormsInfoEnterpriceComponent,
  FormLegalRepresentativeComponent,
  SenderFormComponent,
  ReceiverFormComponent,
  AssignmentDataFormComponent,
  ChangeAdminPasswordFormComponent,
  MonthlyPaymentForm,
  ShipmentFormComponent,
  BankFormComponent
} from './forms';

import {
  DefaultInvoiceComponent,
  DefaultQuoteComponent,
  DepartureManifestComponent
} from './invoice-templates';

import {
  ConfirmationSaveComponent,
  ConfirmationComponent,
  AlertComponent,
  ReservationModalComponent,
  MoveSeatConfirmationComponent,
  MoveBusySeatConfirmationComponent,
  SaleConfirmationComponent,
  ReservedSeatWarningComponent,
  MoveSeatToBusConfirmationComponent,
  MoveBusySeatToBusConfirmationComponent,
  BusDetailComponent,
  DestinationPaymentModalComponent,
  EditOfficeDataModalComponent,
  OfficeDataFormModal,
  MonthlyPaymentModal,
  BoxcarShipemntAssifnmentModalComponent,
  BusUploadImageComponent
} from './modals';

import {
  DriverInfoModalComponent,
  BusnfoModalComponent,
  OfficeInfoModalComponent,
  OfficeAdminInfoModalComponent,
  EnterpriseInfoModalComponent,
  TicketDownloadDialogComponent,
  DepartureOptionsComponent,
  DepartureStatusComponent,
  DepartureTypeSelectionComponent,
  TravelRouteInfoComponent,
  EnterprisePaymentStatusModalComponent,
  BankInfoComponent
} from './modals/mobile';
@NgModule({
  declarations: [
    FooterComponent,
    NavbarComponent,
    TicketHolderFormComponent,
    DriverFormComponent,
    BusStopFormComponent,
    TravelRouteFormComponent,
    AdminLoginFormComponent,
    SidebarComponent,
    AdminFooterComponent,
    ModalComponent,
    ConfirmationSaveComponent,
    ConfirmationComponent,
    AuxiliarContactFormComponent,
    BusFormComponent,
    BusPropietaryFormComponent,
    AssignedDriverComponent,
    ErrorPageComponent,
    DeparturesTopBarComponent,
    EventualDepartureFormComponent,
    ExpressDepartureFormComponent,
    RecurrentDepartureFormComponent,
    DepartureRecurrencyComponent,
    NotifierComponent,
    DriverInfoFormComponent,
    DriverLicenseFormComponent,
    DocumentFormComponent,
    DocumentFormComponent,
    ClientNavbarComponent,
    TravelFormComponent,
    TravelForm2Component,
    SellerTravelCardComponent,
    ClientTravelCardComponent,
    ClientTravelCardComponent,
    PassengerDataFormComponent,
    MultiPassengerDataFormComponent,
    TicketPurchaseTravelCardComponent,
    AlertComponent,
    PaymentDataFormComponent,
    PaymentMethodCardComponent,
    Page404Component,
    CardPaymentFormComponent,
    QrScanComponent,
    CardPaymentComponent,
    PaymentErrorComponent,
    SuccessfulPaymentComponent,
    TicketDetailComponent,
    CashPaymentComponent,
    CashPaymentFormComponent,
    DepartureFormComponent,
    CashPaymentFormComponent,
    OfficeAdminFormComponent,
    EnterpriseOldFormComponent,
    OfficeAdminInfoFormComponent,
    LegalOrganizerFormComponent,
    NotFoundComponent,
    SpinnerComponent,
    SpinnerSmallComponent,
    SalesReportTopBarComponent,
    DepartureSalesFilterComponent,
    SaleSeatingSchemeComponent,
    PassengerReservationInfoComponent,
    SalesTableComponent,
    ReservationTableComponent,
    BusTemplateFormComponent,
    SeatingSchemeFormComponent,
    SeatingSchemeComponent,
    ConfirmSalesTableComponent,
    PrintSaleComponent,
    DefaultInvoiceComponent,
    DefaultQuoteComponent,
    FilteredSaleDeparturesTableComponent,
    ReservationModalComponent,
    MoveSeatComponent,
    MoveSeatConfirmationComponent,
    MoveBusySeatConfirmationComponent,
    SaleConfirmationComponent,
    MoveSeatToBusComponent,
    MoveToBusSeatSelectionComponent,
    SeatingSchemeSeatSelectionComponent,
    ReservedSeatWarningComponent,
    MoveSeatToBusConfirmationComponent,
    OfficeOldFormComponent,
    MoveBusySeatToBusConfirmationComponent,
    BusDetailComponent,
    DosageOfficesTableComponent,
    OfficeDataOldFormComponent,
    EnterpriseAdminOldFormComponent,
    EnterpriseSettingsFormComponent,
    PassengerNameFilterComponent,
    PassengerDocNumberFilterComponent,
    ClientSeatingSchemeComponent,
    PhysicalCommissionFormComponent,
    OnlineCommissionFormComponent,
    DeparturesTableComponent,
    BankAccountFormComponent,
    TicketDownloadDialogComponent,
    DepartureOptionsComponent,
    DepartureStatusComponent,
    DepartureTypeSelectionComponent,
    QrScannerComponent,
    DosageFormComponent,
    SteperProgressComponent,
    EnterpriseInfoFormComponent,
    SteperActionsComponent,
    EnterpriseAdminFormComponent,
    EnterpriseCommissionFormComponent,
    OfficeDataFormComponent,
    FormInfoAdminComponent,
    FormsChangePasswordComponent,
    FormsInfoEnterpriceComponent,
    FormLegalRepresentativeComponent,
    TravelRouteInfoComponent,
    SenderFormComponent,
    ReceiverFormComponent,
    AssignmentDataFormComponent,
    AssignmentDetailComponent,
    AssignmentDetailComponent,
    WarehouseOfficesFilterComponent,
    WarehousePackagesTableComponent,
    DestinationPaymentModalComponent,
    DriverInfoModalComponent,
    BusnfoModalComponent,
    OfficeInfoModalComponent,
    OfficeAdminInfoModalComponent,
    EnterpriseInfoModalComponent,
    ChangeAdminPasswordFormComponent,
    EditOfficeDataModalComponent,
    WorkingPageComponent,
    OfficeDataFormModal,
    MonthlyPaymentModal,
    MonthlyPaymentForm,
    EnterprisePaymentStatusModalComponent,
    ShipmentFormComponent,
    ShipmentAssignmentComponent,
    ShipmentAssignmentTableComponent,
    BoxcarShipemntAssifnmentModalComponent,
    BankInfoComponent,
    BankFormComponent,
    BusUploadImageComponent,
    DepartureManifestComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    DpDatePickerModule,
    NotifierModule.withConfig(notifierOptions),
    PipesModule,
    TooltipModule,
    RecaptchaModule
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    TicketHolderFormComponent,
    DriverFormComponent,
    BusStopFormComponent,
    TravelRouteFormComponent,
    AdminLoginFormComponent,
    SidebarComponent,
    AdminFooterComponent,
    ModalComponent,
    ConfirmationSaveComponent,
    ConfirmationComponent,
    BusFormComponent,
    DeparturesTopBarComponent,
    EventualDepartureFormComponent,
    ExpressDepartureFormComponent,
    RecurrentDepartureFormComponent,
    NotifierComponent,
    ClientNavbarComponent,
    ClientNavbarComponent,
    TravelFormComponent,
    TravelForm2Component,
    SellerTravelCardComponent,
    ClientTravelCardComponent,
    PassengerDataFormComponent,
    MultiPassengerDataFormComponent,
    TicketPurchaseTravelCardComponent,
    AlertComponent,
    PaymentDataFormComponent,
    PaymentMethodCardComponent,
    Page404Component,
    CardPaymentFormComponent,
    QrScanComponent,
    CardPaymentComponent,
    PaymentErrorComponent,
    SuccessfulPaymentComponent,
    TicketDetailComponent,
    CashPaymentComponent,
    CashPaymentFormComponent,
    OfficeAdminFormComponent,
    OfficeAdminInfoFormComponent,
    EnterpriseOldFormComponent,
    DepartureFormComponent,
    NotFoundComponent,
    SpinnerComponent,
    SpinnerSmallComponent,
    SalesReportTopBarComponent,
    DepartureSalesFilterComponent,
    SaleSeatingSchemeComponent,
    PassengerReservationInfoComponent,
    SalesTableComponent,
    ReservationTableComponent,
    BusTemplateFormComponent,
    SeatingSchemeFormComponent,
    SeatingSchemeComponent,
    ConfirmSalesTableComponent,
    PrintSaleComponent,
    DefaultInvoiceComponent,
    DefaultQuoteComponent,
    FilteredSaleDeparturesTableComponent,
    ReservationModalComponent,
    MoveSeatComponent,
    MoveSeatConfirmationComponent,
    MoveBusySeatConfirmationComponent,
    SaleConfirmationComponent,
    MoveSeatToBusComponent,
    MoveToBusSeatSelectionComponent,
    SeatingSchemeSeatSelectionComponent,
    OfficeOldFormComponent,
    ReservedSeatWarningComponent,
    MoveSeatToBusConfirmationComponent,
    MoveBusySeatToBusConfirmationComponent,
    BusDetailComponent,
    DosageOfficesTableComponent,
    OfficeDataOldFormComponent,
    EnterpriseAdminOldFormComponent,
    EnterpriseSettingsFormComponent,
    PassengerNameFilterComponent,
    PassengerDocNumberFilterComponent,
    ClientSeatingSchemeComponent,
    PhysicalCommissionFormComponent,
    OnlineCommissionFormComponent,
    DeparturesTableComponent,
    BankAccountFormComponent,
    TicketDownloadDialogComponent,
    DepartureOptionsComponent,
    DepartureStatusComponent,
    DepartureTypeSelectionComponent,
    QrScannerComponent,
    DosageFormComponent,
    SteperProgressComponent,
    EnterpriseInfoFormComponent,
    SteperActionsComponent,
    EnterpriseAdminFormComponent,
    EnterpriseCommissionFormComponent,
    OfficeDataFormComponent,
    FormInfoAdminComponent,
    FormsChangePasswordComponent,
    FormsInfoEnterpriceComponent,
    TravelRouteInfoComponent,
    SenderFormComponent,
    ReceiverFormComponent,
    AssignmentDataFormComponent,
    AssignmentDetailComponent,
    WarehouseOfficesFilterComponent,
    WarehousePackagesTableComponent,
    DestinationPaymentModalComponent,
    DriverInfoModalComponent,
    BusnfoModalComponent,
    OfficeInfoModalComponent,
    OfficeAdminInfoModalComponent,
    EnterpriseInfoModalComponent,
    ChangeAdminPasswordFormComponent,
    EditOfficeDataModalComponent,
    WorkingPageComponent,
    OfficeDataFormModal,
    MonthlyPaymentModal,
    MonthlyPaymentForm,
    EnterprisePaymentStatusModalComponent,
    ShipmentFormComponent,
    ShipmentAssignmentComponent,
    ShipmentAssignmentTableComponent,
    BoxcarShipemntAssifnmentModalComponent,
    BankInfoComponent,
    BankFormComponent,
    BusUploadImageComponent,
    DepartureManifestComponent
  ],
  providers: [
  ]
})
export class SharedModule { }
