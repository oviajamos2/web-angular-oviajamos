import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { CustomDatepickerI18n, I18n } from 'src/app/core/helpers/ngb-datepicker-i18n';
import { CustomNgbDateAdapter, CustomNgbDateFormatter } from 'src/app/core/adapters';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { BloodTypeEnum, DriverLicense, DriverLicenseCategoryEnum, GenderEnum } from 'src/app/core/http/driver';
import { BooleanEnum } from 'src/app/core/enums/boolean.enum';

@Component({
  selector: 'app-driver-license-form',
  templateUrl: './driver-license-form.component.html',
  styles: [],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateAdapter, useClass: CustomNgbDateAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomNgbDateFormatter }
  ]
})
export class DriverLicenseFormComponent implements OnInit {

  @Input('driverLicenseGroup') driverLicenseGroup: FormGroup;
  @Input('license') license: DriverLicense;
  @Input('submitted') submitted: boolean;
  
  booleanEnum = BooleanEnum;
  booleanEnumKeys = Object.keys(this.booleanEnum);
  bloodType = BloodTypeEnum;
  bloodTypeKeys = Object.keys(this.bloodType);
  category = DriverLicenseCategoryEnum;
  categoryKeys = Object.keys(this.category);
  gender = GenderEnum;
  genderKeys = Object.keys(this.gender);
  maxDate = this.dateAdapter.fromModel(moment().format(DATE_FORMAT));
  maxBirthDate = this.dateAdapter.fromModel(moment().subtract(18, "years").format(DATE_FORMAT));
  minDate = this.dateAdapter.fromModel(moment().format(DATE_FORMAT));
  numberInputValidator;

  constructor(
    private dateAdapter: NgbDateAdapter<string>
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  get licenseForm() { return this.driverLicenseGroup.controls; }

  ngOnInit(): void {
    if(this.license) {
      this.licenseForm.birthAt.setValue(moment(this.license.birthAt).format(DATE_FORMAT));
      this.licenseForm.emitAt.setValue(moment(this.license.emitAt).format(DATE_FORMAT));
      this.licenseForm.expireAt.setValue(moment(this.license.expireAt).format(DATE_FORMAT));
    }
  }
}
