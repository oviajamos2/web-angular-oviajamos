import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';
import { User } from 'src/app/core/http/user';
import { MAX_IMAGE_SIZE } from '../../constants';

@Component({
  selector: 'app-driver-info-form',
  templateUrl: './driver-info-form.component.html',
  styles: [
  ]
})
export class DriverInfoFormComponent implements OnInit {

  @Input('userGroup') userGroup: FormGroup;
  @Input('user') user: User;
  @Input('driverForm') driverForm: FormGroup;
  @Input('submitted') submitted: boolean;

  dafaultImage = "assets/icons/user-image-icon.svg";
  editMode: boolean;
  excededImageSize: boolean;
  imageFile: File;
  hasEmail: boolean;
  public numberInputValidator;

  constructor() {
    this.numberInputValidator = numberInputValidator;
  }

  get userForm() { return this.userGroup.controls; }

  ngOnInit(): void {
    this.hasEmail = false;
    if(this.user) {
      this.editMode = true;
      if(this.userForm.imageUrl) {
        this.dafaultImage = this.userForm.imageUrl.value;
        this.userForm.imageUrl.setValidators([]);
        this.userForm.imageUrl.setValue('');
      }
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file = event.target.files[0];
    if(file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.userForm.imageUrl.setValue('');
        this.dafaultImage = "assets/icons/user-image-icon.svg";
        this.excededImageSize = true;
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.dafaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.userForm.imageUrl.setValue('');
      this.dafaultImage = "assets/icons/user-image-icon.svg";
    }
    if(this.editMode) {
      this.editMode = false;
      this.userForm.imageUrl.setValidators([Validators.required])
    }
  }
}
