import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-enterprise-admin-old-form',
  templateUrl: './enterprise-admin-old-form.component.html',
  styleUrls: []
})
export class EnterpriseAdminOldFormComponent implements OnInit {

  @Input('userGroup') userGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  numberInputValidator = numberInputValidator;

  get userForm() {
    return this.userGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}
}