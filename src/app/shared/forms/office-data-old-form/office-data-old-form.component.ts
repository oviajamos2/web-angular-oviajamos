import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import * as moment from 'moment';

import { DATE_FORMAT } from '../../constants';
import { Office } from 'src/app/core/http/office';
import { Dosage } from 'src/app/core/http/dosage';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-office-data-old-form',
  templateUrl: './office-data-old-form.component.html',
  styleUrls: ['./office-data-old-form.component.scss']
})
export class OfficeDataOldFormComponent implements OnInit, OnDestroy {

  @Input() modalData: ModalData;

  action = new Subject();
  datePickerConf = defaultConf;
  floatInputValidator;
  officeDataGroup: FormGroup;
  submitted: boolean;
  $submitting = new Subject();
  submittingSubs: Subscription;
  submitting: boolean;
  minDate = moment().format(DATE_FORMAT);

  constructor( private formBuilder: FormBuilder ) {
    this.floatInputValidator = floatInputValidator;
    this.datePickerConf.min = this.minDate;
  }

  get officeDataForm() {
    return this.officeDataGroup.controls;
  }

  get office(): Office {
    return this.modalData?.state.office;
  }

  ngOnInit(): void {
    this.buildForm();
    this.submittingSubs = this.$submitting.subscribe( (value: boolean) => {
      this.submitting = value;
    });
  }

  ngOnDestroy(): void {
    this.submittingSubs.unsubscribe();
  }

  buildForm() {
    this.officeDataGroup = this.formBuilder.group({
      office: [this.office.officeId, Validators.compose([Validators.required])],
      gloss: ['', Validators.compose([Validators.required])],
      economicActivity: ['', Validators.compose([Validators.required])],
      invoiceNumber: ['', Validators.compose([Validators.required])],
      authorizationNumber: ['', Validators.compose([Validators.required])],
      emissionTimeLimit: ['', Validators.compose([Validators.required])],
      dosageKey: ['', Validators.compose([Validators.required])]
    });
    if(this.office.dosage) {
      this.officeDataGroup.patchValue(this.office.dosage);
      this.officeDataGroup.addControl('dosageId', this.formBuilder.control(this.office.dosage.dosageId));
      this.officeDataForm.emissionTimeLimit.setValue(moment(this.office.dosage.emissionTimeLimit).format('DD/MM/YYYY'));
    }
  }

  onSubmit() {
    this.submitted = true;
    if(this.officeDataGroup.valid) {
      this.$submitting.next(true);
      let officeData: Dosage = this.officeDataGroup.value;
      officeData.authorizationNumber = parseInt(officeData.authorizationNumber.toString());
      officeData.invoiceNumber = parseInt(officeData.invoiceNumber.toString());
      officeData.dosageKey = encodeURIComponent(officeData.dosageKey);
      officeData.emissionTimeLimit = moment(officeData.emissionTimeLimit, ['DD/MM/YYYY']).toISOString();
      if(!officeData.dosageId) {
        this.action.next([officeData, 1]);
      } else {
        this.action.next([officeData, 2]);
      }
    }
  }
}