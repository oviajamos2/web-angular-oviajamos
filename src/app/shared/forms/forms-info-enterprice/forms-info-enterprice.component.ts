import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormLegalRepresentativeComponent } from '../form-legal-representative/form-legal-representative.component';

@Component({
  selector: 'app-forms-info-enterprice',
  templateUrl: './forms-info-enterprice.component.html',
  styleUrls: ['./forms-info-enterprice.component.scss']
})
export class FormsInfoEnterpriceComponent implements OnInit {
  @ViewChild(FormLegalRepresentativeComponent) formLegalRepresentative: FormLegalRepresentativeComponent;
  busStopGroup: FormGroup;
  dafaultImage = 'assets/icons/default-image-ovj-admin.svg'; 
  hiddenInput:boolean;

  constructor() { 
    this.hiddenInput = false;
  }

  ngOnInit(): void {
  }

  editInfoEnterprice():void {
    this.hiddenInput = true;
    this.formLegalRepresentative.editLegalRepresentative();
    console.log("editar empresa y tener los label ");
  }

  public setImage(event:Event) {
    console.log('cambiar imagen');
  }

}
