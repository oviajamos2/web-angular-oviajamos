import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { CommissionOperationTypeEnum, CommissionTypeEnum } from 'src/app/core/http/enterprise';

@Component({
  selector: 'app-online-commission-form',
  templateUrl: './online-commission-form.component.html',
  styleUrls: []
})
export class OnlineCommissionFormComponent implements OnInit {

  @Input('onlineCommissionGroup') onlineCommissionGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  commissionTypeEnum = CommissionTypeEnum;
  commissionTypeEnumKeys = Object.keys(this.commissionTypeEnum);
  commissionOperationTypeEnum = CommissionOperationTypeEnum;
  commissionOperationTypeEnumKeys = Object.keys(this.commissionOperationTypeEnum);
  numberInputValidator = numberInputValidator;

  get onlineCommissionForm() {
    return this.onlineCommissionGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}

}