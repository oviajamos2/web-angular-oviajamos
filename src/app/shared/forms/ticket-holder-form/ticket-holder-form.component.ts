import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms'

// Validator
import { DocumentTypeEnum } from 'src/app/core/http/user';
import { MachPasswordValidator } from 'src/app/shared/validator/machPassword.validator';
import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';
import { TicketHolder } from 'src/app/core/http/ticket-holder/models/ticket-holder.model';

@Component({
  selector: 'app-ticket-holder-form',
  templateUrl: './ticket-holder-form.component.html',
  styleUrls: [],
  host: {
    '(window:click)': 'onWindowClick($event)'
  }
})
export class TicketHolderFormComponent implements OnInit {

  @Input('ticketHolder') ticketHolder: TicketHolder;

  ticketHolderGroup: FormGroup;
  dafaultImage = 'assets/icons/user-image-icon.svg';
  submitted = false;
  docType = DocumentTypeEnum;
  docTypeKeys = Object.keys(this.docType);
  showDocTypeSelect = false;

  public numberInputValidator;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  onWindowClick(){
    this.showDocTypeSelect = false;
  }

  get f() { return this.ticketHolderGroup.controls; }
  get contacts(): FormArray {
    return <FormArray>this.ticketHolderGroup.controls.contacts;
  }

  public buildForm() {
    this.ticketHolderGroup = this.formBuilder.group({
      photo: ['', Validators.compose([Validators.required])],
      names: ['', Validators.compose([Validators.required])],
      secondNames: ['', Validators.compose([Validators.required])],
      docType: ['', Validators.compose([Validators.required])],
      docNumber: ['', Validators.compose([Validators.required])],
      cellphone: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ])],
      address: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.required])],
      contacts: this.formBuilder.array([]),
    },{
      validator: [
        MachPasswordValidator('password','confirmPassword'),
      ]
    });
    if( this.ticketHolder ) {
      let { photo: tHolderPhoto, ...tHolderToForm } = this.ticketHolder;
      this.ticketHolderGroup.patchValue(tHolderToForm);
      this.initContacts();
    }
  }

  initContacts() {
    for(let i=0 ; i<this.ticketHolder.contacts.length ; i++) {
      this.addContact();
    }
  }

  public onSubmit() {
    this.submitted = true;
    if (this.ticketHolderGroup.valid) {
      return this.ticketHolderGroup.value;
    } else {
      return false;
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.dafaultImage = reader.result as string;
    };
  }

  public clickingInfo() {}

  addContact() {
    this.submitted = false;
    this.contacts.push(new FormGroup({}));
  }

  removeContact(indexForm) {
    this.contacts.removeAt(indexForm);
  }

  selectDocType(key) {
    this.f.docType.setValue(key);
    this.onWindowClick();
  }

}
