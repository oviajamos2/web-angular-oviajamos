import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// Validator
import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';

// Model
import { ReferenceContact } from 'src/app/core/http/driver';

@Component({
  selector: 'form-auxiliar-contact',
  templateUrl: './auxiliar-contact-form.component.html',
  styleUrls: [
    './auxiliar-contact-form.component.scss'
  ]
})
export class AuxiliarContactFormComponent implements OnInit {

  @Input('contact') auxiliarContact: ReferenceContact;
  @Input('contactForm') contactForm: FormGroup;
  @Input('submitted') submitted: boolean;
  @Input('indexForm') indexForm: number;

  @Output() removeContact = new EventEmitter<number>();

  public numberInputValidator;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  get auxiliarContactForm() {
    return this.contactForm.controls;
  }

  public buildForm() {
    this.contactForm.addControl(
      'description',
      this.formBuilder.control('', Validators.compose([Validators.required,
                                                      Validators.maxLength(24),
                                                    ])));
    this.contactForm.addControl(
      'fullName',
      this.formBuilder.control('', Validators.compose([Validators.required,
                                                       Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
                                                      ]))
    );
    this.contactForm.addControl(
      'phone',
      this.formBuilder.control('', Validators.compose([
        Validators.required,
        Validators.min(999999),
        Validators.max(999999999999)
      ]))
    );
    if( this.auxiliarContact ) {
      this.contactForm.patchValue( this.auxiliarContact );
    }
    if(this.auxiliarContact) {
      this.contactForm.addControl(
        'referenceContactId',
        this.formBuilder.control(this.auxiliarContact.referenceContactId, [])
      );
    }
  }

  removeForm() {
    this.removeContact.emit(this.indexForm);
  }
}