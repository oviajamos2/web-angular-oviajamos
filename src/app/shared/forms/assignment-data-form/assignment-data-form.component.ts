import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take, auditTime } from 'rxjs/operators';

import { ALERT_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Office, OfficeService } from 'src/app/core/http/office';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-assignment-data-form',
  templateUrl: './assignment-data-form.component.html',
  styleUrls: ['./assignment-data-form.component.scss']
})
export class AssignmentDataFormComponent implements OnInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;
  @Input('shipmentFormGroup') shipmentGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  departmentEnum = DepartmentEnum;
  departmentEnumKeys = Object.keys(this.departmentEnum);
  destinationDepartmentSubs: Subscription;
  destinationDepartment: DepartmentEnum;
  destinationOffices: Office[] = [];
  destinationOfficeSelectSubs: Subscription;
  loadingDestinationOffices: boolean = false;
  loadingSourceOffices: boolean = false;
  numberInputValidator = numberInputValidator;
  sourceDepartment: DepartmentEnum;
  sourceDepartmentSubs: Subscription;
  sourceOffices: Office[] = [];
  sourceOfficeSelectSubs: Subscription;

  get shipmentForm() {
    return this.shipmentGroup.controls;
  }

  constructor(
    private officeService: OfficeService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.initDeparmentSubs();
  }

  initDeparmentSubs() {
    this.sourceDepartmentSubs = this.shipmentForm.source.valueChanges.pipe(
      auditTime(100),
      distinctUntilChanged()
    ).subscribe((value: DepartmentEnum) => {
      if(value?.length > 0) {
        this.shipmentForm.originOfficeId.setValue('');
        this.sourceDepartment = value;
        this.loadSourceOffices();
      } else {
        this.sourceDepartment = null;
        this.sourceOffices = [];
      }
    });
    this.destinationDepartmentSubs = this.shipmentForm.destination.valueChanges.pipe(
      auditTime(100),
      distinctUntilChanged()
    ).subscribe((value: DepartmentEnum) => {
      if(value?.length > 0) {
        this.shipmentForm.destinationOfficeId.setValue('');
        this.destinationDepartment = value;
        this.loadDestinationOffices();
      } else {
        this.destinationDepartment = null;
        this.destinationOffices = [];
      }
    });
  }
  
  ngAfterViewInit(): void {
    this.initSourceOfficeSelectSubs();
  }

  ngOnDestroy(): void {
    this.sourceOfficeSelectSubs?.unsubscribe();
    this.destinationOfficeSelectSubs?.unsubscribe();
    this.sourceDepartmentSubs?.unsubscribe();
    this.destinationDepartmentSubs?.unsubscribe();
  }

  loadSourceOffices() {
    this.officeService.getAll(
      {
        department: this.sourceDepartment,
        companyId: this.enterprise.companyId
      },
      true
    ).pipe(
      take(1)
    ).subscribe((response: any) => {
      this.loadingSourceOffices = false;
      this.sourceOffices = (response.items as Office[]);
      if(this.sourceOffices.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: `Se requiere de oficinas en ${ this.sourceDepartment } para continuar`,
          link: ['Crear oficinas', '/dashboard/oficina/create']
        });
      }
    }, (error) => {
      console.warn(error);
      this.loadingSourceOffices = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  loadDestinationOffices() {
    this.officeService.getAll(
      {
        department: this.destinationDepartment,
        companyId: this.enterprise.companyId
      },
      true
    ).pipe(
      take(1)
    ).subscribe((response: any) => {
      this.loadingDestinationOffices = false;
      this.destinationOffices = (response.items as Office[]);
      if(this.destinationOffices.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: `Se requiere de oficinas en ${ this.destinationDepartment } para continuar`,
          link: ['Crear oficinas', '/dashboard/oficina/create']
        });
      }
    }, (error) => {
      console.warn(error);
      this.loadingDestinationOffices = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initSourceOfficeSelectSubs() {
    this.sourceOfficeSelectSubs = fromEvent(document.querySelectorAll('.source-office-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.officeService.getAll(
        {
          search: value,
          companyId: this.enterprise.companyId,
          department: this.sourceDepartment
        },
        true
      ).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.sourceOffices = response.items;
      })
    });
  }

  initDestinationOfficeSelectSubs() {
    this.destinationOfficeSelectSubs = fromEvent(document.querySelectorAll('.destination-office-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.officeService.getAll(
        {
          search: value,
          companyId: this.enterprise.companyId,
          department: this.destinationDepartment
        },
        true
      ).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.destinationOffices = response.items;
      })
    });
  }
}
