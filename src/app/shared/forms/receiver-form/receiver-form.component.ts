import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-receiver-form',
  templateUrl: './receiver-form.component.html',
  styleUrls: ['./receiver-form.component.scss']
})
export class ReceiverFormComponent implements OnInit {

  @Input('receiverFormGroup')  receiverGroup: FormGroup;
  @Input('submitted') submitted: FormGroup;

  numberInputValidator = numberInputValidator;

  get receiverForm() {
    return this.receiverGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}
}
