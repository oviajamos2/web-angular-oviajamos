import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Component, OnInit, Input } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import * as moment from 'moment';

import { DATE_FORMAT } from '../../constants';
import { Office } from 'src/app/core/http/office';

@Component({
    selector: 'app-dosage-form',
    templateUrl: './dosage-form.component.html',
    styleUrls: ['./dosage-form.component.scss'] 
  })

export class DosageFormComponent implements OnInit {

    @Input() officeForm: FormGroup ;
    @Input('submitted') submitted: boolean;

    action = new Subject();
    datePickerConf = defaultConf;
    floatInputValidator;
    officeDataGroup: FormGroup;
    state: { office: Office };
    $submitting = new Subject();
    submittingSubs: Subscription;
    submitting: boolean;
    minDate = moment().format(DATE_FORMAT);

    constructor(private formBuilder: FormBuilder){
        this.floatInputValidator = floatInputValidator;
        this.datePickerConf.min = this.minDate;
        
    }
    ngOnInit():void {
        this.buildForm();
    }

    get getOfficeForm(){
        return this.officeForm.controls;
    }

    public buildForm() {
        this.officeForm.addControl('gloss',
            this.formBuilder.control('',Validators.compose([Validators.required]))
        );
        this.officeForm.addControl('economicActivity',
        this.formBuilder.control('',Validators.compose([Validators.required]))
        )
        this.officeForm.addControl('invoiceNumber',
            this.formBuilder.control('',Validators.compose([Validators.required]))
        )
        this.officeForm.addControl('authorizationNumber',
            this.formBuilder.control('',Validators.compose([Validators.required]))
        )
        this.officeForm.addControl('emissionTimeLimit',
            this.formBuilder.control('',Validators.compose([Validators.required]))
        )
        this.officeForm.addControl('dosageKey',
            this.formBuilder.control('',Validators.compose([Validators.required]))
        )
    }
}