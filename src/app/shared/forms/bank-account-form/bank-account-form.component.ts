import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { take } from 'rxjs/operators';

import { Bank, BankService } from 'src/app/core/http/bank';
import { CurrenciesEnum } from 'src/app/core/http/enterprise-settings';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { PaginationResponse } from '../../../core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-bank-account-form',
  templateUrl: './bank-account-form.component.html',
  styleUrls: ['bank-account-form.component.scss']
})
export class BankAccountFormComponent implements OnInit {

  @Input('accountGroup') accountGroup: FormGroup;
  @Input('submitted') submitted: boolean;
  @Input('index') index: number;
  @Input('first') first: boolean;
  @Output('removeAccountEvent') removeAccountEvent = new EventEmitter<number>();

  banks: Bank[] = [];
  currenciesEnum = CurrenciesEnum;
  currenciesEnumKeys = Object.keys(this.currenciesEnum);
  numberInputValidator = numberInputValidator;

  get accountForm() {
    return this.accountGroup.controls;
  }

  constructor(
    private bankService: BankService
  ) {}

  ngOnInit(): void {
    this.loadBanks();
  }

  removeBankAccount() {
    this.removeAccountEvent.emit(this.index);
  }

  loadBanks() {
    this.bankService.getAll({}, true).pipe(take(1)).subscribe((response: PaginationResponse<Bank>) => {
      this.banks = response.items;
    });
  }
}