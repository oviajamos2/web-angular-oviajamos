import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import * as moment from 'moment';

import { Bus, BusService } from 'src/app/core/http/bus';
import { CustomDatepickerI18n, I18n } from 'src/app/core/helpers/ngb-datepicker-i18n';
import { CustomNgbDateAdapter, CustomNgbDateFormatter } from 'src/app/core/adapters';
import { Departure } from 'src/app/core/http/departure';
import { Driver, DriverService } from 'src/app/core/http/driver';
import { Enterprise } from 'src/app/core/http/enterprise';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { validateDateInput } from 'src/app/core/helpers/dateInputValidator';

@Component({
  selector: 'app-eventual-departure-form',
  templateUrl: './eventual-departure-form.component.html',
  styleUrls: ['./eventual-departure-form.component.scss'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateAdapter, useClass: CustomNgbDateAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomNgbDateFormatter }
  ]
})
export class EventualDepartureFormComponent implements OnInit, OnDestroy {
  
  @Input('departure') eventualDeparture: Departure;
  @Input('enterprise') enterprise: Enterprise;
  @Input('departureGroup') eventualGroup: FormGroup;
  @Input('submitted') submitted: boolean;
  @Input('buses') buses: Bus[] = [];
  @Input('travelRoutes') travelRoutes: TravelRoute[] = [];
  @Input('drivers') drivers: Driver[] = [];
  @Input('checkBusInArray') public checkBusInArray: Function;
  @Input('checkDriverInArray') public checkDriverInArray: Function;
  @Input('checkRouteInArray') public checkRouteInArray: Function;
  
  busSelectSubs: Subscription;
  driverSelectSubs: Subscription;
  minDate = this.dateAdapter.fromModel(moment().format(DATE_FORMAT));
  numberInputValidator;
  travelRouteSelectSubs: Subscription;
  validateDateInput = validateDateInput;
  
  get today() {
    return this.dateAdapter.toModel(this.ngbCalendar.getToday())!;
  }

  constructor(
    private busService: BusService,
    private travelRouteService: TravelRouteService,
    private driverService: DriverService,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.busSelectSubs.unsubscribe();
    this.travelRouteSelectSubs.unsubscribe();
    this.driverSelectSubs.unsubscribe();
  }

  ngAfterViewInit() {
    this.initbusSelectSubs();
    this.initTravelRouteSelectSubs();
    this.initDriverSelectSubs();
  }

  get eventualForm() {
    return this.eventualGroup.controls;
  }

  get bus(): FormGroup {
    return <FormGroup>this.eventualForm.bus;
  }

  get route(): FormGroup {
    return <FormGroup>this.eventualForm.route;
  }

  get driver(): FormGroup {
    return <FormGroup>this.eventualForm.driver;
  }

  get departureDate(): FormGroup {
    return <FormGroup>this.eventualForm.departureDate;
  }

  initbusSelectSubs() {
    this.busSelectSubs = fromEvent(document.querySelector('.eventual-license-plate'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<Bus>) => {
        this.buses = response.items;
        this.checkBusInArray();
      })
    });
  }

  initTravelRouteSelectSubs() {
    this.travelRouteSelectSubs = fromEvent(document.querySelector('.eventual-travel-route'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.travelRouteService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
        this.travelRoutes = response.items;
        this.checkRouteInArray();
      })
    });
  }
  
  initDriverSelectSubs() {
    this.driverSelectSubs = fromEvent(document.querySelector('.eventual-driver'), 'keyup').pipe(
      debounceTime( 500 ),
      pluck( 'target', 'value' ),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.driverService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<Driver>) => {
        this.drivers = response.items.map( (driver: Driver) => {
          driver.fullName = `${ driver.user.name } ${ driver.user.lastName }`;
          return driver;
        });
        this.checkRouteInArray();
      });
    });
  }
}