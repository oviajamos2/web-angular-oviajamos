import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/core/http/user';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';


@Component({
  selector: 'app-form-info-admin',
  templateUrl: './form-info-admin.component.html',
  styleUrls: ['./form-info-admin.component.scss']
})
export class FormInfoAdminComponent implements OnInit {
  
  @Input() formGroup:FormGroup;
  hiddenInput:boolean;
  
  constructor(private formBuilder: FormBuilder, 
              private userService: UserService,
              private localStorageService: LocalStorageService){ 

    this.hiddenInput = false;
  }

  ngOnInit(): void {
    console.log(this.formGroup);
    this.getUserForm();
  }

  private getUserForm(){
    this.userService.getUser(3).subscribe(data=>{console.log(data)});
  }
  
  editInfoAdmin():void {
    this.hiddenInput = true;
  }

}
