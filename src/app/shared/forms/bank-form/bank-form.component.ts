import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import focusInvalidInput from '../../validator/focusInvalidInput';

import { Bank } from 'src/app/core/http/bank';

@Component({
  selector: 'app-bank-form',
  templateUrl: './bank-form.component.html',
  styleUrls: []
})
export class BankFormComponent implements OnInit {

  @Input('bank') bank: Bank;

  bankGroup: FormGroup;
  editMode: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  get bankForm() {
    return this.bankGroup.controls;
  }

  public buildForm() {
    this.bankGroup = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])]
    });
    if (this.bank) {
      this.bankGroup.addControl(
        'bankId',
        this.formBuilder.control(this.bank.bankId, Validators.compose([Validators.required]))
      );
      this.bankGroup.patchValue(this.bank);
    }
  }

  public async onSubmit(): Promise<Bank> {
    this.submitted = true;
    this.bankGroup.enable();
    if (this.bankGroup.valid) {
      this.bankGroup.disable();
      const submitForm: Bank = this.bankGroup.value;
      return submitForm;
    } else {
      this.bankGroup.disable();
      focusInvalidInput();
      return null;
    }
  }
}