import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { LegalOrganizerModel } from 'src/app/core/http/enterprise/models/legal-organizer.model';
import { DocumentTypeEnum } from 'src/app/core/http/user';

import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-legal-organizer-form',
  templateUrl: './legal-organizer-form.component.html',
  styleUrls: []
})
export class LegalOrganizerFormComponent implements OnInit {

  @Input('legalOrganizerForm') legalOrganizerGroup: FormGroup;
  @Input('legalOrganizer') legalOrganizer: LegalOrganizerModel;
  @Input('submitted') submitted: boolean;

  documentTypeEnum = DocumentTypeEnum;
  documentTypeEnumKeys = Object.keys(this.documentTypeEnum);
  numberInputValidator;

  get legalOrganizerForm() { return this.legalOrganizerGroup.controls; }

  constructor() {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {}
}