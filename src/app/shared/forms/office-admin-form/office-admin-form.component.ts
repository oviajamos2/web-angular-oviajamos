import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MachPasswordValidator } from 'src/app/shared/validator/machPassword.validator';
import focusInvalidInput from '../../validator/focusInvalidInput';

import { DocumentTypeEnum, UserRoleEnum } from 'src/app/core/http/user';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { OfficeAdmin } from 'src/app/core/http/office-admin';
import { OfficeAdminInfoFormComponent } from '../office-admin-info-form/office-admin-info-form.component';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-office-admin-form',
  templateUrl: './office-admin-form.component.html',
  styleUrls: []
})
export class OfficeAdminFormComponent implements OnInit {

  @Input('secretary') secretary: OfficeAdmin;
  @Input('company') company: Enterprise;
  @ViewChild(OfficeAdminInfoFormComponent) secretaryInfo: OfficeAdminInfoFormComponent;

  dafaultImage = 'assets/icons/user-image-icon.svg';
  secretaryGroup: FormGroup;
  submitted = false;

  get secretaryForm() {
    return this.secretaryGroup.controls;
  }

  get user(): FormGroup {
    return <FormGroup>this.secretaryForm.user;
  }

  get contacts(): FormArray {
    return <FormArray>this.secretaryGroup.controls.referenceContact;
  }

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm(); 
  }

  public async buildForm() {
    this.secretaryGroup = this.formBuilder.group({
      documentType: [DocumentTypeEnum.IDENTITY_CARD, [Validators.required]],
      documentNumber: ['', [Validators.required, 
        Validators.minLength(5),
        Validators.maxLength(12)
        ]],
      user: this.formBuilder.group({
        imageUrl: [''],
        name: ['',  [Validators.required,
          Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
        ]],
        lastName: ['', [Validators.required,
          Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
        ]],
        phone: ['', [
          Validators.required,
          Validators.min(999999),
          Validators.max(999999999999)
        ]],
        address: ['', [Validators.required]],
        password: ['', [Validators.required,
          Validators.minLength(8)
        ]],
        confirmPassword: ['', [Validators.required]],
        email: ['', [Validators.required, 
          Validators.email]
        ],
        role: [UserRoleEnum.OFFICE_ADMIN],
        office: [null, [Validators.required]]
      }, {
        validator: [
          MachPasswordValidator('password','confirmPassword')
        ]
      }),
      referenceContact: this.formBuilder.array([]),
    });
    this.secretaryForm.documentType.disable();
    if( this.secretary ) {
      this.user.removeControl('password');
      this.user.removeControl('confirmPassword');
      this.user.controls.email.disable();
      this.secretaryGroup.addControl('secretaryId', this.formBuilder.control(this.secretary.secretaryId));
      this.initContacts();
      let officeId = this.secretary.user.office.officeId;
      this.secretary.user.office = null;
      await this.secretaryGroup.patchValue(this.secretary);
      this.user.controls.office.setValue(officeId);
    }
  }

  initContacts() {
    for(let secretary of this.secretary.referenceContact) {
      this.addContact();
    }
  }

  public async onSubmit(): Promise<OfficeAdmin> {
    this.submitted = true;
    this.enableForm();
    if (this.secretaryGroup.valid) {
      this.secretaryGroup.disable();
      const submitForm: OfficeAdmin = this.secretaryGroup.value;
      let error: boolean;
      if(this.secretaryInfo.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.secretaryInfo.imageFile).then( (uniqueFileName: string) => {
          if(!uniqueFileName) {
            error = true;
          }
          submitForm.user.imageUrl = uniqueFileName;
        });
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      return submitForm;
    } else {
      this.secretaryGroup.disable();
      focusInvalidInput();
      return null;
    }
  }

  enableForm() {
    this.secretaryGroup.enable();
    this.secretaryForm.documentType.disable();
    if(this.secretary)
      this.user.controls.email.disable();
  }

  public clickingInfo() {}

  addContact() {
    this.submitted = false;
    this.contacts.push(new FormGroup({}));
  }

  removeContact(indexForm) {
    this.contacts.removeAt(indexForm);
  }
}