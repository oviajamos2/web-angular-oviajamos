import { Component, Input, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription, fromEvent, asapScheduler } from 'rxjs';
import { debounceTime, pluck, distinctUntilChanged, take } from 'rxjs/operators';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Person, PersonService } from 'src/app/core/http/person';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-passenger-name-filter',
  templateUrl: './passenger-name-filter.component.html',
  styleUrls: []
})
export class PassengerNameFilterComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('ticketGroup') ticketGroup: FormGroup;
  @Input('persons') persons: Person[];

  passengerNameSubs: Subscription;
  numberInputValidator = numberInputValidator;

  get personGroup(): FormGroup {
    return (<FormGroup>this.ticketGroup.controls.person);
  }
  
  constructor(
    private personService: PersonService
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    asapScheduler.schedule(() => this.initPassengerNameSubs());
  }

  ngOnDestroy(): void {
    this.passengerNameSubs?.unsubscribe()
  }

  passengerNameOnChanges(event) {
    this.personGroup.controls.name.setValue(event.term);
  }

  initPassengerNameSubs() {
    this.passengerNameSubs = fromEvent(document.querySelector('.passengerNameSelect'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.personService.getAll(value).pipe(take(1)).subscribe( (response: PaginationResponse<Person>) => {
        this.persons = response.items;
      })
    });
  }

  seatNumber(seat) {
    return (seat < 10) ? '0' + seat : '' + seat;
  }
}
