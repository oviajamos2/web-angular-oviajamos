import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

@Component({
  selector: 'app-sender-form',
  templateUrl: './sender-form.component.html',
  styleUrls: ['./sender-form.component.scss']
})
export class SenderFormComponent implements OnInit {

  @Input('senderFormGroup') senderGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  numberInputValidator = numberInputValidator;

  get senderForm() {
    return this.senderGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}
}
