import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { asapScheduler, fromEvent, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import { DATE_FORMAT } from 'src/app/shared/constants';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';
import focusInvalidInput from 'src/app/shared/validator/focusInvalidInput';
import * as moment from 'moment';

import { ALERT_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { CustomDatepickerI18n, I18n } from 'src/app/core/helpers/ngb-datepicker-i18n';
import { CustomNgbDateAdapter, CustomNgbDateFormatter } from 'src/app/core/adapters';
import { DATE_PATTERN } from 'src/app/core/helpers/dateInputValidator';
import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { CompanyInvoiceTypeEnum, Enterprise, PhysicalSalesCommission } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Office, OfficeTypeEnum } from 'src/app/core/http/office';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-office-data-form',
  templateUrl: './office-data-form.component.html',
  styleUrls: [],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateAdapter, useClass: CustomNgbDateAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomNgbDateFormatter }
  ]
})
export class OfficeDataFormComponent implements OnInit, OnDestroy {

  @Input('office') office: Office;
  @Input('enterprise') enterprise: Enterprise;
  @Input('firstOffice') firstOffice: boolean;

  action = new Subject();
  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  departmentEnum = DepartmentEnum;
  departmentEnumKeys = Object.keys(this.departmentEnum);
  floatInputValidator;
  companyInvoiceTypeEnum = CompanyInvoiceTypeEnum;
  minDate = this.dateAdapter.fromModel(moment().format(DATE_FORMAT));
  numberInputValidator = numberInputValidator;
  officeGroup: FormGroup;
  officeTypeEnum = OfficeTypeEnum;
  officeTypeEnumKeys = Object.keys(this.officeTypeEnum);
  submitted: boolean = false;

  get officeForm() {
    return this.officeGroup.controls;
  }

  get dosageGroup(): FormGroup {
    return (<FormGroup>this.officeForm.dosage);
  }

  get dosageForm() {
    return this.dosageGroup.controls;
  }

  get physicalComission(): PhysicalSalesCommission {
    return this.enterprise.physicalSalesCommission;
  }

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService,
    private notificationService: NotificationService,
    private dateAdapter: NgbDateAdapter<string>
  ) {
    this.floatInputValidator = floatInputValidator;
  }

  ngOnInit(): void {
    this.builForm();
    this.loadBusStops();
    asapScheduler.schedule(() => this.patchValue());
  }

  ngOnDestroy() {
    this.busStopSelectSubs?.unsubscribe();
  }

  builForm() {
    this.officeGroup = this.formBuilder.group({
      company: [(this.enterprise?.companyId || null)],
      busStop: [null, [Validators.required]],
      name: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])],
      address: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])],
      department: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      dosage: this.formBuilder.group({
        gloss: ['', Validators.compose([Validators.required])],
        economicActivity: ['', Validators.compose([Validators.required])],
        invoiceNumber: [1, Validators.compose([Validators.required])],
        authorizationNumber: ['', Validators.compose([Validators.required])],
        emissionTimeLimit: ['', Validators.compose([Validators.required, Validators.pattern(DATE_PATTERN)])],
        dosageKey: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3)
        ])]
      })
    });
  }

  patchValue() {
    if(this.office) {
      let office = { ...this.office };
      this.officeGroup.addControl('officeId', this.formBuilder.control(office.officeId));
      this.dosageGroup.addControl('dosageId', this.formBuilder.control(office.dosage.dosageId));
      if(isNaN(office.busStop)) office.busStop = (<any>office.busStop).busStopId; 
      office.dosage.emissionTimeLimit = moment(office.dosage.emissionTimeLimit).format(DATE_FORMAT);
      this.officeGroup.patchValue(office);
    }
  }

  public async onSubmit(): Promise<Office> {
    this.submitted = true;
    this.enableForm()
    if (this.officeGroup.valid) {
      this.officeGroup.disable();
      const submitForm: Office = this.officeGroup.value;
      submitForm.companyId = submitForm.company;
      submitForm.busStopId = submitForm.busStop;
      submitForm.dosage.emissionTimeLimit = moment(submitForm.dosage.emissionTimeLimit, [DATE_FORMAT]).toISOString();
      submitForm.dosage.invoiceNumber = parseInt(submitForm.dosage.invoiceNumber.toString());
      submitForm.dosage.authorizationNumber = parseInt(submitForm.dosage.authorizationNumber.toString());
      return submitForm;
    } else {
      this.officeGroup.disable();
      focusInvalidInput();
      return null;
    }
  }

  enableForm() {
    this.officeGroup.enable();
  }

  loadBusStops() {
    this.busStopService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = response.items;
      if(this.busStops.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: 'Se requiere de paradas para continuar',
          link: ['Crear paradas', '/dashboard/parada/create']
        })
      }
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initbusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.busStopFilter'), 'keyup').pipe(
      debounceTime( 500 ),
      pluck( 'target', 'value' ),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll({ search: value }, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }
}