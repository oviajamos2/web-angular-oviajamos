import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import focusInvalidInput from 'src/app/shared/validator/focusInvalidInput';
import * as moment from 'moment';

import { BankAccount } from 'src/app/core/http/enterprise-settings';
import { CompanyStatusEnum, Enterprise, EnterpriseTypeEnum } from 'src/app/core/http/enterprise';
import { DocumentTypeEnum } from 'src/app/core/http/user';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { MAX_IMAGE_SIZE } from 'src/app/shared/constants';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprise-info-form',
  templateUrl: './enterprise-info-form.component.html',
  styleUrls: ['enterprise-info-form.component.scss']
})
export class EnterpriseInfoFormComponent implements OnInit {

  @Input('enterprise') enterprise: Enterprise;

  dafaultImage = 'assets/icons/default-image-ovj-admin.svg';
  editMode: boolean;
  enterpriseGroup: FormGroup;
  enterpriseTypeEnum = EnterpriseTypeEnum;
  enterpriseTypeEnumKeys = Object.keys(this.enterpriseTypeEnum);
  excededImageSize: boolean;
  imageFile: File;
  numberInputValidator = numberInputValidator;
  submitted: boolean = false;
  removedAccounts: BankAccount[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.checkDefaultImage();
  }

  get enterpriseForm() {
    return this.enterpriseGroup.controls;
  }

  get companySettingsGroup(): FormGroup {
    return <FormGroup>this.enterpriseForm.companySettings;
  }

  get companySettingsForm() {
    return this.companySettingsGroup.controls;
  }

  get accountsArray(): FormArray {
    return (<FormArray>this.companySettingsForm.accounts);
  }

  get legalOrganizerForm() {
    return (<FormGroup>this.enterpriseForm.legalOrganizer).controls;
  }

  buildForm() {
    this.enterpriseGroup = this.formBuilder.group({
      imageUrl: ['', [Validators.required]],
      name: ['', [Validators.required, Validators.maxLength(100)]],
      type: ['', [Validators.required]],
      businessName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
      ])],
      nit: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
      ])],
      status: [CompanyStatusEnum.INCOMPLETE_RECORD],
      companySettings: this.formBuilder.group({
        accounts: this.formBuilder.array([
          this.formBuilder.group({
            bankName: ['', [
              Validators.required
            ]],
            accountNumber: ['', [
              Validators.required,
              Validators.min(1000000),
              Validators.max(9999999999)
            ]],
            currency: ['', [
              Validators.required
            ]],
            accountOwnerName: ['', [
              Validators.required,
              Validators.minLength(4)
            ]]
          }),
        ])
      }),
      legalOrganizer: this.formBuilder.group({
        firstName: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100)
        ])],
        lastName: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100)
        ])],
        documentType: [DocumentTypeEnum.IDENTITY_CARD],
        documentNumber: ['', Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(12),
        ])],
        phoneNumber: ['', [
          Validators.required,
          Validators.min(1000000),
          Validators.max(99999999),
          Validators.pattern('^[0-9]*$')
        ]],
        address: ['...']
      }),
    });
    this.patchValue();
  }

  patchValue() {
    if(this.enterprise) {
      if(!this.enterprise.legalOrganizer) delete this.enterprise.legalOrganizer;
      if(this.enterprise.companySettings) {
        let settings = this.enterprise.companySettings;
        settings.timeToDisableSales = settings.timeToDisableSales?.toString().substr(0, 5) || null;
        settings.reserveTimeLimit = settings.reserveTimeLimit?.toString().substr(0, 5) || null;
        settings.accounts = settings.accounts.reverse();
        this.companySettingsGroup.addControl('companySettingsId', this.formBuilder.control(settings.companySettingsId));
      } else {
        delete this.enterprise.companySettings;
      }
      this.enterpriseGroup.addControl('companyId', this.formBuilder.control(this.enterprise.companyId));
      this.enterpriseGroup.patchValue(this.enterprise);
      this.initAccounts()
    }
  }

  initAccounts() {
    if(this.enterprise.companySettings?.accounts.length > 0) {
      this.accountsArray.clear();
    }
    this.enterprise.companySettings?.accounts.reverse().forEach( (account: BankAccount) => {
      let accountForm = this.formBuilder.group({
        bankAccountId: [account.bankAccountId],
        bankName: [account.bankName, [
          Validators.required
        ]],
        accountNumber: [account.accountNumber, [
          Validators.required,
          Validators.min(1000)
        ]],
        currency: [account.currency, [
          Validators.required
        ]],
        accountOwnerName: [account.accountOwnerName, [
          Validators.required,
          Validators.minLength(4)
        ]]
      });
      this.accountsArray.push(accountForm);
    });
  }

  public async onSubmit(): Promise<Enterprise> {
    this.submitted = true;
    this.enterpriseGroup.enable();
    if (this.enterpriseGroup.valid) {
      this.enterpriseGroup.disable();
      const submitForm: Enterprise = this.enterpriseGroup.value;
      submitForm.nit = submitForm.nit.toString();
      let accounts = submitForm.companySettings.accounts;
      submitForm.companySettings.accounts = accounts.map( (bankAccount: BankAccount) => {
        return {
          ...bankAccount,
          accountNumber: bankAccount.accountNumber.toString()
        }
      });
      let error: boolean;
      if(this.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.imageFile).then( (uniqueFileName: string)  => {
          if(!uniqueFileName) error = true;
          submitForm.imageUrl = uniqueFileName;
        });
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      if(this.editMode) delete submitForm.imageUrl;
      return submitForm;
    } else {
      this.enterpriseGroup.disable();
      focusInvalidInput();
      return null;
    }
  }

  checkDefaultImage() {
    if(this.enterprise) {
      this.editMode = true;
      if(this.enterpriseForm.imageUrl) {
        this.dafaultImage = this.enterpriseForm.imageUrl.value;
        this.enterpriseForm.imageUrl.setValidators([]);
        this.enterpriseForm.imageUrl.setValue('');
      }
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if (file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.enterpriseForm.imageUrl.setValue('');
        this.dafaultImage = 'assets/icons/default-image-ovj-admin.svg';
        this.excededImageSize = true
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.dafaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.enterpriseForm.imageUrl.setValue('');
      this.dafaultImage = 'assets/icons/default-image-ovj-admin.svg';
    }
    if (this.editMode) {
      this.editMode = false;
      this.enterpriseForm.imageUrl.setValidators([Validators.required]);
    }
  }

  addBankAccount() {
    let newBankAccount = this.formBuilder.group({
      bankName: [''],
      accountNumber: [''],
      currency: [''],
      accountOwnerName: ['']
    });
    this.accountsArray.push(newBankAccount);
  }

  removeAccount(event: number) {
    let removedAccount: BankAccount = this.accountsArray.at(event).value;
    if(removedAccount.bankAccountId) {
      removedAccount.deletedAt = moment().toISOString();
      this.removedAccounts.push(removedAccount);
    }
    this.accountsArray.removeAt(event);
  }
}