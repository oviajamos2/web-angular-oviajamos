import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MachPasswordValidator } from 'src/app/shared/validator/machPassword.validator';

@Component({
  selector: 'app-change-admin-password-form',
  templateUrl: './change-admin-password-form.component.html',
  styleUrls: ['./change-admin-password-form.component.scss']
})
export class ChangeAdminPasswordFormComponent implements OnInit {

  changePasswordGroup: FormGroup;
  submitted: boolean = false;

  get changePasswordForm() {
    return this.changePasswordGroup.controls;
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.changePasswordGroup = this.formBuilder.group({
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]],
      confirmPassword: ['', [Validators.required]]
    }, {
      validator: [
        MachPasswordValidator('password', 'confirmPassword')
      ]
    });
  }

  public async onSubmit(): Promise<string> {
    this.submitted = true;
    this.changePasswordGroup.enable();
    if(this.changePasswordGroup.valid) {
      this.changePasswordGroup.disable();
      return this.changePasswordGroup.value.password;
    } else {
      this.changePasswordGroup.disable();
      return null;
    }
  }
}
