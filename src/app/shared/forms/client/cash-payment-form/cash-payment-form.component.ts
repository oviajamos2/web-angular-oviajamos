import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

import { AlertComponent } from 'src/app/shared/modals';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cash-payment-form',
  templateUrl: './cash-payment-form.component.html',
  styleUrls: []
})
export class CashPaymentFormComponent implements OnInit {

  @Input('amountPaid') amountPaid: number;
  @Input('toPay') toPay: number;

  cashPayemntGroup: FormGroup;
  modalRef: NgbModalRef;
  public numberInputValidator;

  constructor(
    private formBuilder: FormBuilder,
    private modalService: ModalService
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  get cashPaymentForm() {
    return this.cashPayemntGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.cashPayemntGroup = this.formBuilder.group({
      amountPaid: ['', [Validators.required, Validators.pattern('^[1-9]+[0-9]*$')]],
      amountReturn: [{value: 0, disabled: true}]
    });
    this.amountChanges();
  }

  onSubmit() {
    if(this.cashPaymentForm.amountPaid.errors && this.cashPaymentForm.amountPaid.errors.required) {
      let modalData: ModalData = {
        content: {
          description: `El necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      let submitModalError: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
        submitModalError.unsubscribe();
        this.modalRef.close();
      });
      return false;
    } else if(this.cashPaymentForm.amountPaid.errors && this.cashPaymentForm.amountPaid.errors.pattern) {
      let modalData: ModalData = {
        content: {
          description: `El monto ingresado no es válido`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      let submitModalError2: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
        submitModalError2.unsubscribe();
        this.modalRef.close();
      });
      return false;
    } else {
      return this.cashPayemntGroup.value;
    }
  }

  amountChanges() {
    this.cashPaymentForm.amountPaid.valueChanges.subscribe( (value: number) => {
      let returnAmount = value - this.toPay;
      if(returnAmount < 0) {
        this.cashPaymentForm.amountReturn.setValue(0);
      } else {
        this.cashPaymentForm.amountReturn.setValue(returnAmount);
      }
    });
  }
}