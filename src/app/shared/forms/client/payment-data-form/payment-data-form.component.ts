import { Component, Input, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';

import { AlertComponent } from 'src/app/shared/modals';
import { FormGroup } from '@angular/forms';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-payment-data-form',
  templateUrl: './payment-data-form.component.html',
  styleUrls: ['./payment-data-form.component.scss']
})
export class PaymentDataFormComponent implements OnInit {

  @Input('ticketBuyerGroup') ticketBuyerGroup;

  modalRef: NgbModalRef;
  numberInputValidator;

  get ticketBuyerForm() {
    return this.ticketBuyerGroup.controls;
  }
  
  get personGroup(): FormGroup {
    return (<FormGroup>this.ticketBuyerGroup.controls.person);
  }

  constructor(
    private modalService: ModalService
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {}

  onSubmit() {
    if(!this.personGroup.controls.email.valid) {
      let modalData: ModalData = {
        content: {
          description: `El correo ingresado debe ser válido`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData  = modalData;
      let submitModalError: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
        submitModalError.unsubscribe();
        this.modalRef.close();
      });
      return false;
    }
    if(this.personGroup.valid) {
      return this.personGroup.value;
    } else {
      let modalData: ModalData = {
        content: {
          description: `Es necesario llenar los campos requeridos`,
          type: 'alert'
        }
      }
      this.modalRef = this.modalService.open(AlertComponent, 'modal-md' );
      this.modalRef.componentInstance.modalData = modalData;
      let submitModalError2: Subscription = this.modalRef.componentInstance.action.subscribe( (result: any) => {
        submitModalError2.unsubscribe();
        this.modalRef.close();
      });
      return false;
    }
  }
}