import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-legal-representative',
  templateUrl: './form-legal-representative.component.html',
  styleUrls: ['./form-legal-representative.component.scss']
})
export class FormLegalRepresentativeComponent implements OnInit {
  hiddenInput:boolean;
  
  constructor() { 
    this.hiddenInput = false;
  }

  ngOnInit(): void {
  }

  editLegalRepresentative():void{
    this.hiddenInput = true;
  }

}
