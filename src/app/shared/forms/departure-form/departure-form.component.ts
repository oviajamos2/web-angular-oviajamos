import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { take } from 'rxjs/operators';
import focusInvalidInput from '../../validator/focusInvalidInput';
import * as moment from 'moment';

import { ALERT_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { Bus, BusService } from 'src/app/core/http/bus';
import { Departure, DepartureStatusEnum, DepartureTypeEnum } from 'src/app/core/http/departure';
import { Driver, DriverService } from 'src/app/core/http/driver';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { toModel, toNgbTime } from 'src/app/core/helpers/ngbTimeStruct';
import { DATE_PATTERN } from 'src/app/core/helpers/dateInputValidator';

@Component({
  selector: 'app-departure-form',
  templateUrl: './departure-form.component.html',
  styleUrls: ['./departure-form.component.scss']
})
export class DepartureFormComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Input('type') type: string;
  @Input('enterprise') enterprise: Enterprise;

  buses: Bus[] = [];
  departureTypeEnum = DepartureTypeEnum;
  departureTypeEnumKeys = Object.keys(this.departureTypeEnum);
  departureGroup: FormGroup;
  departureTypeSelected: DepartureTypeEnum = DepartureTypeEnum.EVENTUAL;
  drivers: Driver[] = [];
  minDate = moment().format(DATE_FORMAT);
  submitted: boolean;
  travelRoutes: TravelRoute[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private busService: BusService,
    private travelRouteService: TravelRouteService,
    private driverService: DriverService,
    private notificationService: NotificationService
  ) {}

  get departureForm() {
    return this.departureGroup.controls;
  }

  get bus(): FormGroup {
    return <FormGroup>this.departureForm.bus;
  }

  get route(): FormGroup {
    return <FormGroup>this.departureForm.route;
  }

  get driver(): FormGroup {
    return <FormGroup>this.departureForm.driver;
  }

  get departureDate(): FormGroup {
    return <FormGroup>this.departureForm.departureDate;
  }

  get departureContractor(): FormGroup {
    return <FormGroup>this.departureForm.departureContractor;
  }

  ngOnInit(): void {
    if(this.type) {
      switch (this.type) {
        case 'express':
          this.departureTypeSelected = this.departureTypeEnum.EXPRESS
          break;
        case 'eventual':
          this.departureTypeSelected = this.departureTypeEnum.EVENTUAL
          break;
        case 'recurrent':
          this.departureTypeSelected = this.departureTypeEnum.RECURRENT
          break;
      }
    }
    if(this.departure) {
      this.departureTypeSelected = this.departure.departureType;
    }
    this.buildForm();
    this.loadBuses();
    this.loadTravelRoutes();
    this.loadDrivers();
  }

  public buildForm() {
    this.departureGroup = this.formBuilder.group({
      bus: this.formBuilder.group({
        busId: [null, Validators.compose([Validators.required])]
      }),
      route: this.formBuilder.group({
        routeId: [null, Validators.compose([Validators.required])]
      }),
      driver: this.formBuilder.group({
        driverId: [null, Validators.compose([Validators.required])]
      }),
      lane: ['', Validators.compose([Validators.required])],
      departureType: [this.departureTypeSelected],
      price: ['', Validators.compose([Validators.required])], 
      status: [DepartureStatusEnum.WAITING],
      onSale: [true],
      departureContractor: this.formBuilder.group({
        phoneNumber: ['', Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.required])],
        documentType: ['', Validators.compose([Validators.required])],
        documentNumber: ['', Validators.compose([Validators.required])],
        description: ['']
      }),
      departureDate: this.formBuilder.group({
        departureTime: ['', Validators.compose([Validators.required])],
        startDate: ['', Validators.compose([Validators.required, Validators.pattern(DATE_PATTERN)])],
        endDate: []
      }),
    });
    this.patchForm();
  }

  patchForm() {
    if(this.departure) {
      let dep = { ...this.departure };
      if( dep.departureContractor ) {
        this.departureContractor.addControl('departureContractorId',
          this.formBuilder.control(dep.departureContractor.departureContractorId));
      } else {
        delete dep.departureContractor;
      }
      this.departureDate.addControl('departureDateId', this.formBuilder.control(dep.departureDate.departureDateId));
      this.departureGroup.addControl('departureId', this.formBuilder.control(dep.departureId));
      this.departureGroup.patchValue(dep);
      this.departureDate.controls.departureTime.setValue(toNgbTime(dep.departureDate.departureTime.toString()));
      this.departureDate.controls.startDate.setValue(moment(dep.departureDate.startDate).format(DATE_FORMAT));
    }
    this.requiredContractor();
    this.initBusValueChanges();
  }

  public onSubmit() {
    this.submitted = true;
    if(this.departureGroup.valid) {
      const submitForm = this.departureGroup.value;
      if(submitForm.departureType !== this.departureTypeEnum.EXPRESS) {
        delete submitForm.departureContractor
      }
      submitForm.departureDate.startDate = moment(submitForm.departureDate.startDate, [DATE_FORMAT]).toDate();
      submitForm.departureDate.departureTime = toModel(submitForm.departureDate.departureTime);
      return submitForm;
    } else {
      focusInvalidInput();
      return false;
    }
  }

  setDepartureType(departureKey: string) {
    if(this.departureTypeSelected !== this.departureTypeEnum[departureKey]) {
      this.submitted = false;
      this.departureTypeSelected = this.departureTypeEnum[departureKey];
      this.departureForm.departureType.setValue(this.departureTypeSelected);
      this.requiredContractor();
    }
  }

  loadBuses() {
    this.busService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Bus>) => {
      this.buses = response.items;
      if(this.buses.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: 'Se requiere de buses para continuar',
          link: ['Crear buses', '/dashboard/bus/create']
        });
      }
      this.checkBusInArray();
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  loadTravelRoutes() {
    this.travelRouteService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
      this.travelRoutes = response.items;
      if(this.travelRoutes.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: 'Se requiere de rutas para continuar',
          link: ['Crear rutas', '/dashboard/ruta/create']
        });
      }
      this.checkRouteInArray();
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  loadDrivers() {
    this.driverService.getAll({companyId: this.enterprise.companyId}).pipe(take(1)).subscribe( (response: PaginationResponse<Driver>) => {
      this.drivers = response.items.map( (driver: Driver) => {
        driver.fullName = `${ driver.user.name } ${ driver.user.lastName }`;
        return driver;
      });
      if(this.drivers.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: 'Se requiere de choferes para continuar',
          link: ['Crear choferes', '/dashboard/chofer/create']
        });
      }
      this.checkDriverInArray();
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initBusValueChanges() {
    this.bus.controls.busId.valueChanges.subscribe( (value: number) => {
      this.buses.find( (bus: Bus) => {
        if(bus.busId === value) {
          if(bus.driver) {
            this.driverService.get(bus.driver.driverId).pipe(take(1)).subscribe( (response: Driver) => {
              this.drivers.push(response);
              this.driver.controls.driverId.setValue(response.driverId);
            }, (error) => {})
          }
        }
      });
    });
  }

  requiredContractor() {
    if(this.departureTypeSelected === this.departureTypeEnum.EXPRESS) {
      this.departureContractor.controls.phoneNumber.setValidators([
          Validators.required, 
          Validators.min(999999),
          Validators.max(999999999999)
        ]);
      this.departureContractor.controls.name.setValidators([
          Validators.required,
          Validators.pattern('[0-9a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
        ]);
      this.departureContractor.controls.documentType.setValidators([Validators.required]);
      this.departureContractor.controls.documentNumber.setValidators([
        Validators.required, 
        Validators.maxLength(12),
        Validators.minLength(7)
      ]);
    } else {
      this.departureContractor.controls.phoneNumber.clearValidators();
      this.departureContractor.controls.name.clearValidators();
      this.departureContractor.controls.documentType.clearValidators();
      this.departureContractor.controls.documentNumber.clearValidators();
    }
    this.departureGroup.updateValueAndValidity();
  }

  checkDriverInArray() {
    if(!this.route.controls.routeId.value) return;
    let exist: boolean;
    this.drivers.find( (driver: Driver) => {
      if(driver.driverId === this.route.controls.routeId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.driverService.get(this.driver.controls.driverId.value).pipe(take(1)).subscribe( (response: Driver) => {
      if(response?.user) {
        response.fullName = `${ response.user.name } ${ response.user.lastName }`;
        this.drivers.push(response);
      } else {
        this.driver.controls.driverId.setValue('');
      }
    }, error => {
      this.driver.controls.driverId.setValue('');
    });
  }

  checkBusInArray() {
    if(!this.bus.controls.busId.value) return;
    let exist: boolean;
    this.buses.find( (bus: Bus) => {
      if(bus.busId === this.bus.controls.busId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.busService.get(this.bus.controls.busId.value).pipe(take(1)).subscribe( (response: Bus) => {
      if(response) {
        this.buses.push(response);
      } else {
        this.bus.controls.busId.setValue('');
      }
    }, error => {
      this.bus.controls.busId.setValue('');
    });
  }

  checkRouteInArray() {
    if(!this.route.controls.routeId.value) return;
    let exist: boolean;
    this.travelRoutes.find( (route: TravelRoute) => {
      if(route.routeId === this.route.controls.routeId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.travelRouteService.get(this.route.controls.routeId.value).pipe(take(1)).subscribe( (response: TravelRoute) => {
      if(response) {
        this.travelRoutes.push(response);
      } else {
        this.route.controls.routeId.setValue('');
      }
    }, error => {
      this.route.controls.routeId.setValue('');
    });
  }
}