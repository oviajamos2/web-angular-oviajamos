import { Component, Input, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription, fromEvent, asapScheduler } from 'rxjs';
import { debounceTime, pluck, distinctUntilChanged, take } from 'rxjs/operators';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Person, PersonService } from 'src/app/core/http/person';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-passenger-doc-number-filter',
  templateUrl: './passenger-doc-number-filter.component.html',
  styleUrls: ['passenger-doc-number-filter.component.scss']
})
export class PassengerDocNumberFilterComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('personGroup') personGroup: FormGroup;
  @Input('persons') persons: Person[];
  @Input('withLabel') withLabel: boolean;
  @Input('index') index: number;
  @ViewChild('selectForm') selectForm: ElementRef<HTMLElement>;

  documentNumberSubs: Subscription;
  loading: boolean;
  numberInputValidator = numberInputValidator;

  constructor(
    private personService: PersonService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    asapScheduler.schedule(() => {
      let selectInput = this.selectForm.nativeElement.querySelector('.ng-input').children[0];
      selectInput.setAttribute('type', 'number');
      selectInput.classList.add('ovj-input');
      selectInput.addEventListener('keydown', numberInputValidator)
      this.initDocumentNumberSubs();
    });
  }

  ngOnDestroy(): void {
    this.documentNumberSubs?.unsubscribe()
  }

  documentNumberOnChanges(event) {
    this.loading = true;
    if(event.term.length === 0) {
      this.loading = false;
      this.persons = [];
    }
    if(this.personGroup.controls.personId) {
      this.personGroup.removeControl('personId');
    }
    this.personGroup.controls.documentNumber.setValue(event.term);
  }

  initDocumentNumberSubs() {
    this.documentNumberSubs = fromEvent(document.querySelector(`#documentNumberSelect${this.index}`), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      if(value.length === 0) {
        this.persons = [];
        return;
      }
      this.personService.getAll(value).pipe(take(1)).subscribe( (response: PaginationResponse<Person>) => {
        this.loading = false;
        this.persons = response.items;
        let docNumber = this.personGroup.controls.documentNumber.value;
        let existPerson: Person = this.persons.find((person: Person) => person.documentNumber === docNumber)
        if(existPerson) {
          this.setPassengerData(existPerson);
        } else {
          if(this.personGroup.controls.personId) {
            this.personGroup.removeControl('personId');
          }
          this.personGroup.controls.name.setValue('');
          this.personGroup.controls.phone.setValue('');
        }
      }, (error) => {
        console.warn(error);
        this.loading = false;
      });
    });
  }

  seatNumber(seat) {
    return (seat < 10) ? '0' + seat : '' + seat;
  }

  setPassengerData(event: Person) {
    if(!event) return;
    this.personGroup.addControl('personId', this.formBuilder.control(event.personId));
    this.personGroup.controls.documentNumber.setValue(event.documentNumber);
    this.personGroup.controls.name.setValue(event.name);
    this.personGroup.controls.phone.setValue(event.phone);
  }

  customSearchFn(term: string, item) {
    let name = item.name.replace(',', '');
    let documentNumber = item.documentNumber.toString().replace(',', '');
    term = term.toLocaleLowerCase();
    let existForName = name.toLocaleLowerCase().indexOf(term) > -1;
    let existForDocNumber = documentNumber.toLocaleLowerCase().indexOf(term) > -1;
    if(existForName) {
      return existForName;
    } else {
      return existForDocNumber;
    }
  }

  setValueOnInput() {
    (<HTMLInputElement>document.getElementById(`passengerDoc${this.index}`)).value = this.personGroup.controls.documentNumber.value;
  }

  clearInput() {
    (<HTMLInputElement>document.getElementById(`passengerDoc${this.index}`)).value = '';
  }
}