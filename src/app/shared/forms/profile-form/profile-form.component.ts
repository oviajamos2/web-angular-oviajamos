import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router';

import { Enterprise } from 'src/app/core/http/enterprise';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Profile } from 'src/app/core/http/profile';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @Input('profile') profile: Profile;
  @Input('enterprise') enterprise: Enterprise;
  @Output('editOfficeDataEvent') editProfileDataEvent = new EventEmitter<Profile>();

  profileGroup: FormGroup;
  dafaultImage = 'assets/icons/home-outline.svg';
  editMode: boolean;
  imageFile: File;
  public numberInputValidator;
  submitted = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {
    this.numberInputValidator = numberInputValidator;
  }

  ngOnInit(): void {
    this.buildForm();
    this.checkDefaultImage();
  }

  get profileForm() {
    return this.profileGroup.controls;
  }

  public buildForm() {
    this.profileGroup = this.formBuilder.group({
      imageUrl: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      bankName: ['', Validators.compose([Validators.required])],
      bankAccountNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(100)
      ])],
      phone: ['', [
        Validators.required,
        Validators.pattern('^[0-9]*$')
      ]],
      email: ['', [Validators.email]],
      docNumber: ['', Validators.compose([Validators.required])]
    });
    if (this.enterprise) {
      this.profileGroup.addControl(
        'companyId',
        this.formBuilder.control(this.enterprise.companyId, Validators.compose([Validators.required]))
      );
      this.profileGroup.patchValue(this.enterprise);
    }
    console.log(this.enterprise);
    console.log(this.profileGroup.value);
  }

  public async onSubmit(): Promise<Profile> {
    this.submitted = true;
    this.profileGroup.enable();
    if (this.profileGroup.valid) {
      this.profileGroup.disable();
      const submitForm: Profile = this.profileGroup.value;
      let error: boolean;
      if(this.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.imageFile).then( (uniqueFileName: string)  => {
          if(!uniqueFileName) error = true;
          submitForm.imageUrl = uniqueFileName;
        });
      } else {
        submitForm.imageUrl = this.profile.imageUrl;
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      return submitForm;
    } else {
      this.profileGroup.disable();
      return null;
    }
  }

  checkDefaultImage() {
    if(this.profile) {
      this.editMode = true;
      if(this.profileForm.imageUrl) {
        this.dafaultImage = this.profileForm.imageUrl.value;
        this.profileForm.imageUrl.setValidators([]);
        this.profileForm.imageUrl.setValue('');
      }
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const [file] = event.target.files;
    if(file) {
      this.imageFile = file;
      reader.readAsDataURL(file);
      reader.onload = () => {
      this.dafaultImage = reader.result as string;
    };
    } else {
      this.imageFile = null;
      this.profileForm.imageUrl.setValue('');
      this.dafaultImage = "assets/images/bus-stop-imagen.jpg";
    }
    if(this.editMode) {
      this.editMode = false;
      this.profileForm.imageUrl.setValidators([Validators.required])
    }
  }

  public changePassword(prof: Profile) {
    this.router.navigateByUrl(`/dashboard/perfil/edit/${prof.profileId}`, {state: {prof}});
  }
}
