import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import { asapScheduler, fromEvent, Subscription } from 'rxjs';
import focusInvalidInput from '../../validator/focusInvalidInput';

import { ALERT_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { defaultTimeConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, IntermediateRoute } from 'src/app/core/http/travel-route';
import { toModel, toNgbTime } from 'src/app/core/helpers/ngbTimeStruct';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-travel-route-form',
  templateUrl: './travel-route-form.component.html',
  styleUrls: ['./travel-route-form.component.scss']
})
export class TravelRouteFormComponent implements OnInit, OnDestroy {

  @Input('travelRoute') travelRoute: TravelRoute;
  @Input('enterprise') enterprise: Enterprise;

  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  datePickerTimeConf = defaultTimeConf;
  deletedIntermediateRoutes: IntermediateRoute[] = [];
  submitted = false;
  travelRouteGroup: FormGroup;
  goAndBack: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.loadBusStops();
  }

  ngAfterViewInit() {
    this.initbusStopSelectSubs();
  }

  ngOnDestroy() {
    this.busStopSelectSubs?.unsubscribe();
  }  

  get travelRouteForm() {
    return this.travelRouteGroup.controls;
  }

  get source(): FormGroup {
    return <FormGroup>this.travelRouteForm.source;
  }

  get destination(): FormGroup {
    return <FormGroup>this.travelRouteForm.destination;
  }
  
  get intermediateRoutes(): FormArray {
    return <FormArray>this.travelRouteForm.intermediateRoutes;
  }

  public buildForm() {
    this.travelRouteGroup = this.formBuilder.group({
      company: [this.enterprise.companyId],
      source: this.formBuilder.group({
        busStopId: [null, Validators.compose([Validators.required])]
      }),
      destination: this.formBuilder.group({
        busStopId: [null, Validators.compose([Validators.required])]
      }),
      intermediateRoutes: this.formBuilder.array([]),
      description: [''],
    });
    this.initSourceValueChanges();
    if(this.travelRoute) {
      this.travelRouteGroup.addControl('routeId', this.formBuilder.control(this.travelRoute.routeId));
      this.travelRouteGroup.patchValue(this.travelRoute);
      this.initIntermediateRoutes();
    }
  }

  public onSubmit() {
    this.submitted = true;
    if (this.travelRouteGroup.valid) {
      const submitForm: TravelRoute = this.travelRouteGroup.value;
      submitForm.intermediateRoutes = submitForm.intermediateRoutes.concat(this.deletedIntermediateRoutes);
      submitForm.intermediateRoutes = submitForm.intermediateRoutes.map((intermediateRoute: IntermediateRoute) => {
        intermediateRoute.timeEstimation = toModel(<NgbTimeStruct>intermediateRoute.timeEstimation);
        return intermediateRoute;
      });
      return this.travelRouteGroup.value;
    } else {
      focusInvalidInput();
      return false;
    }
  }

  loadBusStops() {
    this.busStopService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = response.items;
      if(this.busStops.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: 'Se requiere de paradas para continuar',
          link: ['Crear paradas', '/dashboard/parada/create']
        })
      }
      this.checkBusStopsInArray();
    }, (error) => {
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  initbusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.busStopFilter'), 'keyup').pipe(
      debounceTime( 500 ),
      pluck( 'target', 'value' ),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll({ search: value }, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
        this.checkBusStopsInArray();
      });
    });
  }

  initIntermediateRoutes() {
    this.travelRoute.intermediateRoutes.forEach( (intermediateRoute: IntermediateRoute) => {
      let newIntermediateRouteForm = this.formBuilder.group({
        intermediateRouteId: [intermediateRoute.intermediateRouteId],
        source: this.formBuilder.group({
          busStopId: [intermediateRoute.source.busStopId, []]
        }),
        destination: this.formBuilder.group({
          busStopId: [intermediateRoute.destination.busStopId, [Validators.required]]
        }),
        timeEstimation: [toNgbTime(intermediateRoute.timeEstimation.toString()), []]  
      });
      this.intermediateRoutes.push(newIntermediateRouteForm);
    });
  }

  addIntermediateRoute(index: number = this.intermediateRoutes.length) {
    this.submitted = false;
    this.intermediateRoutes.insert(index,
      this.formBuilder.group({
        source: this.formBuilder.group({
          busStopId: [this.source.controls.busStopId.value]
        }),
        destination: this.formBuilder.group({
          busStopId: [null, [Validators.required]]
        }),
        timeEstimation: ['01:00']
      })
    );
    asapScheduler.schedule(() => {
      this.busStopSelectSubs.unsubscribe();
      this.initbusStopSelectSubs();
    });
  }

  initSourceValueChanges() {
    this.source.controls.busStopId.valueChanges.subscribe( value => {
      this.intermediateRoutes.controls.forEach( (intermediateRoute: FormGroup) => {
        (<FormGroup>intermediateRoute.controls.source).controls.busStopId.setValue(value);
      })
    });
  }

  removeIntermediateRoute(index: number) {
    let deletedRoute: IntermediateRoute = this.travelRouteGroup.value.intermediateRoutes[index];
    if(deletedRoute.intermediateRouteId) {
      deletedRoute.deletedAt = new Date();
      if(!this.deletedIntermediateRoutes.find(route => { return route.intermediateRouteId === deletedRoute.intermediateRouteId } )) {
        this.deletedIntermediateRoutes.push(deletedRoute);
      }
    }
    if(this.travelRoute) {
      this.travelRoute.intermediateRoutes.splice(index, 1);
    }
    this.intermediateRoutes.removeAt(index);
  }

  checkBusStopsInArray() {
    this.checkSourceInArray();
    this.checkDestinationInArray();
    this.checkIntermediatesInArray();
  }

  checkSourceInArray() {
    if(!this.source.controls.busStopId.value) return;
    let exist: boolean;
    this.busStops.find( (busStop: BusStop) => {
      if(busStop.busStopId === this.source.controls.busStopId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.busStopService.get(this.source.controls.busStopId.value).pipe(take(1)).subscribe( (response: BusStop) => {
      if(response) {
        this.busStops.push(response);
      } else {
        this.source.controls.busStopId.setValue('');
      }
    }, error => {
      this.source.controls.busStopId.setValue('');
    });
  }

  checkDestinationInArray() {
    if(!this.destination.controls.busStopId.value) return;
    let exist: boolean;
    this.busStops.find( (busStop: BusStop) => {
      if(busStop.busStopId === this.destination.controls.busStopId.value) {
        exist = true;
      }
    });
    if(exist) return;
    this.busStopService.get(this.destination.controls.busStopId.value).pipe(take(1)).subscribe( (response: BusStop) => {
      if(response) {
        this.busStops.push(response);
      } else {
        this.destination.controls.busStopId.setValue('');
      }
    }, error => {
      this.destination.controls.busStopId.setValue('');
    });
  }

  checkIntermediatesInArray() {
    this.intermediateRoutes.controls.forEach( (intermediateRoute: FormGroup) => {
      if((<FormGroup>intermediateRoute.controls.destination).controls.busStopId.value === '') return;
      let exist: boolean;
      this.busStops.find( (busStop: BusStop) => {
        if(busStop.busStopId === (<FormGroup>intermediateRoute.controls.destination).controls.busStopId.value) {
          exist = true;
        }
      });
      if(exist) return;
      this.busStopService.get((<FormGroup>intermediateRoute.controls.destination).controls.busStopId.value)
        .pipe(take(1)).subscribe( (response: BusStop) => {
          if(response) {
            this.busStops.push(response);
          } else {
            (<FormGroup>intermediateRoute.controls.destination).controls.busStopId.setValue('');
          }
        }, error => {
          (<FormGroup>intermediateRoute.controls.destination).controls.busStopId.setValue('');
        });
    });
  }

  clickingInfo() {}
}