import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MachPasswordValidator } from 'src/app/shared/validator/machPassword.validator';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';

import { Enterprise, EnterpriseTypeEnum } from 'src/app/core/http/enterprise';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { MAX_IMAGE_SIZE } from '../../constants';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { UserRoleEnum } from 'src/app/core/http/user';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-enterprise-old-form',
  templateUrl: './enterprise-old-form.component.html',
  styleUrls: [],
})
export class EnterpriseOldFormComponent implements OnInit {

  @Input('enterprise') enterprise: Enterprise;

  dafaultImage = 'assets/icons/default-image-ovj-admin.svg';
  editMode: boolean;
  enterpriseGroup: FormGroup;
  enterpriseTypeEnum = EnterpriseTypeEnum;
  enterpriseTypeEnumKeys = Object.keys(this.enterpriseTypeEnum);
  excededImageSize: boolean;
  floatInputValidator = floatInputValidator;
  imageFile: File;
  numberInputValidator = numberInputValidator;
  submitted = false;

  
  get enterpriseForm() {
    return this.enterpriseGroup.controls;
  }
  
  get legalOrganizer(): FormGroup {
    return <FormGroup>this.enterpriseForm.legalOrganizer;
  }

  get userGroup(): FormGroup {
    return <FormGroup >(<FormArray>this.enterpriseForm.users).at(0);
  }

  get physicalSalesCommissionGroup(): FormGroup {
    return (<FormGroup>this.enterpriseForm.physicalSalesCommission);
  }
  
  get onlineSalesCommissionGroup(): FormGroup {
    return (<FormGroup>this.enterpriseForm.onlineSalesCommission);
  }

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.checkDefaultImage();
  }

  public async buildForm() {
    this.enterpriseGroup = this.formBuilder.group({
      imageUrl: [''],
      name: ['', [Validators.required, Validators.maxLength(100)]],
      referencePhoneNumber: ['', Validators.compose([
        Validators.required,
        Validators.min(1000000),
        Validators.pattern('^[0-9]*$')
      ])],
      type: ['', [Validators.required]],
      invoiceName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
      ])],
      nit: ['', Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(100)
      ])],
      legalOrganizer: this.formBuilder.group({
        name: ['', Validators.compose([
          Validators.required,
          Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'),
          Validators.minLength(3),
          Validators.maxLength(100)
        ])],
        documentType: ['', [Validators.required]],
        documentNumber: ['', Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(12),
        ])],
        phoneNumber: ['', [
          Validators.required,
          Validators.min(100000),
          Validators.max(100000000000),
          Validators.pattern('^[0-9]*$')
        ]],
        address: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(255)
        ])]
      }),
      users: this.formBuilder.array([
        this.formBuilder.group({
          imageUrl: [''],
          name: ['', [Validators.required, 
                      Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'), 
                      Validators.maxLength(100)
                    ]],
          lastName: ['', [Validators.required, 
                          Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*'),
                          Validators.maxLength(100)
                        ]],
          address: [''],
          role: [UserRoleEnum.COMPANY_ADMIN],
          phone: ['', Validators.compose([
            Validators.required,
            Validators.max(100000000000),
            Validators.min(1000000)
          ])],
          email: ['', Validators.compose([
            Validators.required,
            Validators.email,
          ])],
          password: ['', Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(24)
          ])],
          confirmPassword: [''],
        }, {
          validator: [
            MachPasswordValidator('password', 'confirmPassword')
          ]
        })

      ]),
      templateName: ['ovjTemplate'],
      onlineSalesCommission: this.formBuilder.group({
        targetAmount: [''],
        value: ['', [Validators.required]],
        type: ['', [Validators.required]],
        operationType: ['', [Validators.required]],
      }),
      physicalSalesCommission: this.formBuilder.group({
        value: ['', [Validators.required]],
        type: ['', [Validators.required]],
        operationType: ['', [Validators.required]]
      })
    });
    if (this.enterprise) {
      this.enterpriseGroup.addControl('companyId', this.formBuilder.control(this.enterprise.companyId));
      await this.enterpriseGroup.patchValue(this.enterprise);
    }
  }

  public async onSubmit(): Promise<Enterprise> {
    this.submitted = true;
    this.enterpriseGroup.enable();
    if (this.enterpriseGroup.valid) {
      this.enterpriseGroup.disable();
      const submitForm: Enterprise = this.enterpriseGroup.value;
      let error: boolean;
      submitForm.referencePhoneNumber = submitForm.referencePhoneNumber.toString();
      submitForm.nit = submitForm.nit.toString();
      if (this.imageFile) {
        await this.imageRepositoryService.uploadDigital(this.imageFile).then((uniqueFileName: string) => {
          if(!uniqueFileName) error = true;
          submitForm.imageUrl = uniqueFileName;
          submitForm.users[0].imageUrl = uniqueFileName;
        });
      } else {
        submitForm.imageUrl = this.enterprise.imageUrl;
        submitForm.users[0].imageUrl = this.enterprise.imageUrl;
      }
      if(error) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      return submitForm;
    } else {
      this.enterpriseGroup.disable();
      return null;
    }
  }

  public clickingInfo() {}

  checkDefaultImage() {
    if(this.enterprise) {
      this.editMode = true;
      if(this.enterpriseForm.imageUrl) {
        this.dafaultImage = this.enterpriseForm.imageUrl.value;
        this.enterpriseForm.imageUrl.setValidators([]);
        this.enterpriseForm.imageUrl.setValue('');
      }
    }
  }

  public setImage(event) {
    const reader = new FileReader();
    const file: File = event.target.files[0];
    if (file) {
      const limitSize: number = MAX_IMAGE_SIZE;
      if(file.size > limitSize) {
        this.imageFile = null;
        this.enterpriseForm.imageUrl.setValue('');
        this.dafaultImage = 'assets/icons/default-image-ovj-admin.svg';
        this.excededImageSize = true
      } else {
        this.imageFile = file;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.dafaultImage = reader.result as string;
        };
        this.excededImageSize = false;
      }
    } else {
      this.imageFile = null;
      this.enterpriseForm.imageUrl.setValue('');
      this.dafaultImage = 'assets/icons/default-image-ovj-admin.svg';
    }
    if (this.editMode) {
      this.editMode = false;
      this.enterpriseForm.imageUrl.setValidators([Validators.required]);
    }
  }
}