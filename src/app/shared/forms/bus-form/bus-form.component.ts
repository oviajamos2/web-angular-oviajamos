import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import focusInvalidInput from '../../validator/focusInvalidInput';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { Bus, BusTypeEnum, SeatingSchemeEnum } from 'src/app/core/http/bus';
import { BusStatusEnum } from 'src/app/core/http/bus';
import { BusUploadImageComponent } from '../../modals';
import { DocumentTypeEnum } from 'src/app/core/http/user';
import { Enterprise } from 'src/app/core/http/enterprise';
import { ImageRepositoryService } from 'src/app/core/http/image-repository.service';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService} from 'src/app/core/services';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-bus-form',
  templateUrl: './bus-form.component.html',
  styleUrls: ['./bus-form.component.scss']
})
export class BusFormComponent implements OnInit {

  @Input('bus') bus: Bus;
  @Input('enterprise') enterprise: Enterprise;
  @Output('busTemplatesEvent') busTemplatesEvent = new EventEmitter<boolean>();

  busGroup: FormGroup;
  busStatusEnum = BusStatusEnum;
  busStatusEnumKeys = Object.keys(this.busStatusEnum);
  busTypeEnum = BusTypeEnum;
  busTypeEnumKeys = Object.keys(this.busTypeEnum);
  defaultImage = "assets/images/bus-default.png";
  editMode: boolean = false;
  excededImageSize: boolean = false;
  imageFile: File;
  imageFile2: File;
  public selectedSeatingScheme: SeatingScheme;
  seatingSchemeEnum = SeatingSchemeEnum;
  seatingSchemeEnumKeys = Object.keys(this.seatingSchemeEnum);
  submitted = false;

  modalRef: NgbModalRef;
  formImages: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private imageRepositoryService: ImageRepositoryService,
    private notificationService: NotificationService,
    private modalService: ModalService,
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.checkImageOnEdit();
  }

  checkImageOnEdit() {
    if(this.bus) {
      if(this.busForm.imageUrl) {
        try {
          this.defaultImage = JSON.parse(this.bus.imageUrl)[0];
        } catch (error) {
          this.defaultImage = this.bus.imageUrl;
        }
      }
    }
  }

  get busForm() {
    return this.busGroup.controls;
  }

  get busPropietary(): FormGroup {
    return <FormGroup>this.busForm.busPropietary;
  }

  public async buildFormImages() {
    this.formImages = this.formBuilder.group({
      imageBus:['',Validators.compose([Validators.required])],
      imageInsideBus:['',Validators.compose([Validators.required])]
    });
  }

  public async buildForm() {
    this.busGroup = this.formBuilder.group({
      company: [this.enterprise.companyId],
      imageUrl: [''],
      type: ['', Validators.compose([Validators.required])],
      seatingScheme: this.formBuilder.group({
        seatingSchemeId: ['']
      }), 
      licensePlate: ['', Validators.compose([Validators.required,
        Validators.maxLength(8)
      ])],
      status: ['', Validators.compose([Validators.required])],
      busPropietary: this.formBuilder.group({
        documentType: [DocumentTypeEnum.IDENTITY_CARD],
        name: ['', Validators.compose([Validators.required,
          Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
        ])],
        lastName: ['', Validators.compose([Validators.required,
          Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*')
        ])],
        phone: ['', Validators.compose([Validators.required,
          Validators.min(999999),
          Validators.max(999999999999)
        ])]
      })
    });
    if( this.bus ) {
      let busToPatch: Bus = { ...this.bus };
      this.busGroup.addControl('busId', this.formBuilder.control(busToPatch.busId));
      this.busPropietary.addControl('busPropietaryId', this.formBuilder.control(busToPatch.busPropietary.busPropietaryId));
      this.selectedSeatingScheme = this.bus.seatingScheme;
      delete busToPatch.imageUrl;
      await this.busGroup.patchValue(busToPatch);
    }
  }

  public async onSubmit(): Promise<Bus> {
    this.submitted = true;
    this.busGroup.enable();
    if (this.busGroup.valid && this.selectedSeatingScheme) {
      this.busGroup.disable();
      const submitForm: Bus = { ...this.busGroup.value };
      let uploadedFiles: string[] = null;
      uploadedFiles = await this.uploadImages();
      if(!uploadedFiles) {
        this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
        return null;
      }
      submitForm.imageUrl = JSON.stringify(uploadedFiles);
      submitForm.seatingScheme = this.selectedSeatingScheme;
      delete submitForm.seatingScheme.seatingSchemeId;
      return submitForm;
    } else {
      this.busGroup.disable();
      focusInvalidInput();
      return null;
    }
  }

  async uploadImages(): Promise<string[]> {
    return new Promise( async (resolve) => {
      let uploadedFiles = [];
      if(this.imageFile)
        await this.imageRepositoryService.uploadDigital(this.imageFile).then( (uniqueFileName: string) => {
          uploadedFiles[0] = uniqueFileName;
        }).catch((error) => resolve (null));
      if(this.imageFile2)
        await this.imageRepositoryService.uploadDigital(this.imageFile2).then( (uniqueFileName: string) => {
          uploadedFiles[1] = uniqueFileName;
        }).catch((error) => resolve (null));
      if(!uploadedFiles[0]) {
        try {
          uploadedFiles[0] = JSON.parse(this.bus?.imageUrl)[0] || null;
        } catch (error) {
          uploadedFiles[0] = this.bus?.imageUrl || null;
        }
      }
      if(!uploadedFiles[1]) {
        try {
          uploadedFiles[1] = JSON.parse(this.bus?.imageUrl)[1] || null;
        } catch (error) {
          uploadedFiles[1] = null;
        }
      }
      resolve(uploadedFiles);
    });
  }

  busTemplates() {
    this.busTemplatesEvent.emit( true );
  }
  
  openImagesModal():void {
    let modalData: ModalData = {
      state: {
        bus: this.bus
      }
    };
    this.modalRef = this.modalService.open(BusUploadImageComponent, 'modal-md-2x');
    this.modalRef.componentInstance.busGroup = this.busGroup;
    this.modalRef.componentInstance.imageFile = this.imageFile;
    this.modalRef.componentInstance.imageFile2 = this.imageFile2;
    this.modalRef.componentInstance.editMode = this.editMode;
    this.modalRef.componentInstance.modalData = modalData;
    let templateModal: Subscription = this.modalRef.componentInstance.action.subscribe((result: File[]) => {
      templateModal.unsubscribe();
      this.modalRef.close();
      if(result) {
        if(result[0]) this.imageFile = result[0];
        if(result[1]) this.imageFile2 = result[1];
        if(this.imageFile) {
          const reader = new FileReader();
          reader.readAsDataURL(this.imageFile);
          reader.onload = () => {
            this.defaultImage = reader.result as string;
          };
        }
      }
    });
  }
}