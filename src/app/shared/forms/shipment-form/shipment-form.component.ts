import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import focusInvalidInput from 'src/app/shared/validator/focusInvalidInput';

import { Enterprise } from 'src/app/core/http/enterprise';
import { Shipping } from 'src/app/core/http/shipment';

@Component({
  selector: 'app-shipment-form',
  templateUrl: './shipment-form.component.html',
  styleUrls: ['./shipment-form.component.scss']
})
export class ShipmentFormComponent implements OnInit {

  @Input('enterprise') enterprise: Enterprise;

  shipmentGroup: FormGroup;
  submitted: boolean = false;

  get shipmentForm() {
    return this.shipmentGroup.controls;
  }

  get senderGroup(): FormGroup {
    return (<FormGroup>this.shipmentForm.sender);
  }

  get receiverGroup(): FormGroup {
    return (<FormGroup>this.shipmentForm.receiver);
  }

  constructor( private formBuilder: FormBuilder ) {}

  ngOnInit(): void {
    this.buidForm();
  }

  buidForm() {
    this.shipmentGroup = this.formBuilder.group({
      sender: this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(4)]],
        documentNumber: ['', [Validators.required, Validators.pattern('^[0-9]{4,12}')]],
        phone: ['', [Validators.required, Validators.pattern('^[0-9]{4,12}')]]
      }),
      receiver: this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(4)]],
        documentNumber: ['', [Validators.required, Validators.pattern('^[0-9]{4,12}')]],
        phone: ['', [Validators.required, Validators.pattern('^[0-9]{4,12}')]]
      }),
      source: ['', [Validators.required]],
      destination: ['', [Validators.required]],
      originOfficeId: ['', [Validators.required]],
      destinationOfficeId: ['', [Validators.required]],
      totalPrice: [],
      comment: [],
      status: [],
      packages: this.formBuilder.array([
        this.formBuilder.group({
            quantity: ['', [Validators.required]],
            description: ['', [Validators.required]],
            isFragile: [false],
            weight: ['', [Validators.required]],
            price: ['', [Validators.required]]
        })
      ])
    });
  }

  public async onSubmit(): Promise<Shipping> {
    this.submitted = true;
    this.shipmentGroup.enable();
    if (this.shipmentGroup.valid) {
      this.shipmentGroup.disable();
      const submitForm: Shipping = this.shipmentGroup.value;
      return {
        ...submitForm,
        sender: {
          ...submitForm.sender,
          documentNumber: submitForm.sender.documentNumber.toString(),
          phone: submitForm.sender.phone.toString()
        },
        receiver: {
          ...submitForm.receiver,
          documentNumber: submitForm.receiver.documentNumber.toString(),
          phone: submitForm.receiver.phone.toString()
        }
      };
    } else {
      this.shipmentGroup.disable();
      focusInvalidInput();
      return null;
    }
  }

  reloadForm() {
    this.buidForm();
    this.submitted = false;
  }
}
