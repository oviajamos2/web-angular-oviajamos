import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Shipping } from 'src/app/core/http/shipment';

@Component({
  selector: 'app-warehouse-packages-table',
  templateUrl: './warehouse-packages-table.component.html',
  styleUrls: ['./warehouse-packages-table.component.scss']
})
export class WarehousePackagesTableComponent implements OnInit {

  @Input('shipments') shipments: Shipping[];
  @Output('selectShipmentEvent') selectShipmentEvent = new EventEmitter<any>();
  @Output('checkShipmentEvent') checkShipmentEvent = new EventEmitter<any>();
  @Output('uncheckShipmentEvent') uncheckShipmentEvent = new EventEmitter<any>();
  @Output('checkAllEvent') checkAllEvent = new EventEmitter<boolean>();
  @Output('uncheckAllEvent') uncheckAllEvent = new EventEmitter<boolean>();

  selectedShipment: Shipping = null;

  constructor() {}

  ngOnInit(): void {}

  getTotalPackages(shipment: Shipping): number {
    let amount = 0;
    shipment.packages.forEach(pack => {
      amount = amount + parseInt(pack.quantity.toString());
    });
    return amount;
  }

  selectShipment(shipment) {
    this.selectedShipment = shipment;
    this.selectShipmentEvent.emit(this.selectedShipment);
  }

  checkShipment(event, shipment) {
    (<any>document.querySelector('.header-package-checkbox')).checked = false;
    if(event.target.checked) {
      this.checkShipmentEvent.emit(shipment);
    } else {
      this.uncheckShipmentEvent.emit(shipment);
    }
  }

  checkAll(event) {
    if(event.target.checked) {
      this.checkAllEvent.emit(true);
      document.querySelectorAll('.package-checkbox').forEach((element: any) => {
        element.checked = true;
      });
    } else {
      this.uncheckAllEvent.emit(true);
      document.querySelectorAll('.package-checkbox').forEach((element: any) => {
        element.checked = false;
      });
    }
  }
}
