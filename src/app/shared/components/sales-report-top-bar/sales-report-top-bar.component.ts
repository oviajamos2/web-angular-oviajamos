import { Component, OnInit, Output, ViewChild, EventEmitter, OnDestroy, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { fromEvent, Subscription, Subject } from 'rxjs';
import { auditTime, debounceTime, distinctUntilChanged, pluck, take } from 'rxjs/operators';
import * as moment from 'moment';

import { DatePickerComponent } from 'ng2-date-picker';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { TravelRoute, TravelRouteService } from 'src/app/core/http/travel-route';
import { Enterprise } from 'src/app/core/http/enterprise';
import { Office, OfficeService } from 'src/app/core/http/office';

type FilterSaleParams = {
  travelRoute?: TravelRoute,
  office?: number,
  departureDate?: string
}

@Component({
  selector: 'app-sales-report-top-bar',
  templateUrl: './sales-report-top-bar.component.html',
  styleUrls: ['./sales-report-top-bar.component.scss']
})
export class SalesReportTopBarComponent implements OnInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;
  @Output('addDeparture') addDepartureEvent = new EventEmitter<boolean>();
  @Output('filterSaleEvent') filterDepartureEvent = new EventEmitter<FilterSaleParams>();
  @ViewChild('datepicker') datepicker: DatePickerComponent;


  dpConf = {...defaultConf};
  offices: Office[] = [];
  officeSelectSubs: Subscription;
  submitSubs: Subscription;
  submit$ = new Subject<boolean>();
  topBarGroup: FormGroup;
  travelRoutes: TravelRoute[] = [];
  travelRouteSelectSubs: Subscription;

  ticketOffices = [
    { id: 1, name: 'Boleteria 1' },
    { id: 2, name: 'Boleteria 2' },
    { id: 3, name: 'Boleteria 3' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private travelRouteService: TravelRouteService,
    private officeService: OfficeService
  ) {
    moment().locale('es');
    this.dpConf.unSelectOnClick = false;
  }

  get filterDepartureForm() { return this.topBarGroup.controls; }

  get day() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('dddd')}
  get numberDay() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('D')}
  get month() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('MMMM')}
  get year() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('YYYY')}

  ngOnInit(): void {
    this.buildForm();
    this.loadTravelRoutes();
    this.loadOffices();
    this.initSubmitSubs();
  }

  ngAfterViewInit(): void {
    this.initTravelRouteSelectSubs();
    this.initOfficeSelectSubs();
  }

  ngOnDestroy(): void {
    this.travelRouteSelectSubs?.unsubscribe();
    this.officeSelectSubs?.unsubscribe();
    this.submitSubs?.unsubscribe();
  }

  public buildForm() {
    this.topBarGroup = this.formBuilder.group({
      travelRoute: ['', Validators.compose([Validators.required])],
      office: ['', Validators.compose([Validators.required])],
      departureDate: [moment().format(DATE_FORMAT)],
      ticketOffice: ['', Validators.compose([Validators.required])]
    });
  }

  public onSubmit() {
    this.filterDepartureEvent.emit({
      travelRoute: this.filterDepartureForm.travelRoute.value,
      office: this.filterDepartureForm.office.value,
      departureDate: moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).toISOString()
    });
  }

  initSubmitSubs() {
    this.submitSubs = this.submit$.pipe(
      auditTime(100)
    ).subscribe(() => {
      this.onSubmit();
    });
  }

  loadTravelRoutes() {
    this.travelRouteService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
      this.travelRoutes = response.items;
    })
  }

  initTravelRouteSelectSubs() {
    this.travelRouteSelectSubs = fromEvent(document.querySelector('.travel-route-select'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe((value: string) => {
      this.travelRouteService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<TravelRoute>) => {
        this.travelRoutes = response.items;
      });
    });
  }

  loadOffices() {
    this.officeService.getAll({companyId: this.enterprise.companyId})
      .pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
      this.offices = response.items;
    })
  }

  initOfficeSelectSubs() {
    this.officeSelectSubs = fromEvent(document.querySelector('.office-select'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe((value: string) => {
      this.officeService.getAll({search: value, companyId: this.enterprise.companyId})
        .pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.offices = response.items;
      });
    });
  }

  prevMonth() {
    this.filterDepartureForm.departureDate.setValue(
      moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).subtract(1, 'month').format(DATE_FORMAT)
    );
  }

  nextMonth() {
    this.filterDepartureForm.departureDate.setValue(
      moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).add(1, 'month').format(DATE_FORMAT)
    );
  }

  capitalize( val: string ) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  }
}