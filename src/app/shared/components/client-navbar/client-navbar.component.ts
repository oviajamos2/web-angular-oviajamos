import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';

@Component({
  selector: 'app-client-navbar',
  templateUrl: './client-navbar.component.html',
  styleUrls: ['./client-navbar.component.scss']
})
export class ClientNavbarComponent implements OnInit, OnDestroy {

  @Output('navLinkEvent') navLinkEvent = new EventEmitter<string>();

  showMobileMenu: boolean;

  sections = [
    { name: 'Inicio', url: '/client/home' },
    { name: 'Destinos', url: '/client/#' },
    { name: 'Horarios', url: 'client/#' },
    { name: 'Tarifas', url: '/client/#' },
    { name: 'Contactos', url: '/client/#' },
  ];

  hideMobileMenu: Subscription;

  constructor( private router: Router ) { }

  ngOnInit(): void {
    this.hidingMobileMenu();
  }

  ngOnDestroy(): void {
    this.hideMobileMenu.unsubscribe();
  }

  goToSection( section ) {
    this.showMobileMenu = false;
    this.navLinkEvent.emit(section.url);
  }

  toggleMenu() {
    this.showMobileMenu = !this.showMobileMenu;
  }

  hidingMobileMenu() {
    this.hideMobileMenu = fromEvent(document.querySelector('.client-content'), 'click').subscribe( response => {
      this.showMobileMenu = false;
    })
  }

  checkActiveRoute(navLinkUrl: string): boolean {
    let res = false;
    if (this.router.url.includes(navLinkUrl) &&
      navLinkUrl !== '' &&
      navLinkUrl !== '/') {
      res = true;
    }
    return res;
  }
}