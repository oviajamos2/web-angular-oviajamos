import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Input } from '@angular/core';

import { Driver } from 'src/app/core/http/driver';

@Component({
  selector: 'app-assigned-driver',
  templateUrl: './assigned-driver.component.html',
  styleUrls: ['./assigned-driver.component.scss']
})
export class AssignedDriverComponent implements OnInit {

  @Input('driver') driver: Driver
  @Output('removeDriverEvent') removeDriverEvent = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {}

  remove() {
    this.removeDriverEvent.emit(true);
  }
}
