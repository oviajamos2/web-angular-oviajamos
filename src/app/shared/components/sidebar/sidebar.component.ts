import { Component, OnInit, EventEmitter, Output, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { asapScheduler, Observable, Subscription, timer } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import * as moment from 'moment';

import { AuthService } from 'src/app/core/services/auth/auth.service';
import { User, UserRoleEnum } from 'src/app/core/http/user';
import {
  COMPANY_ADMIN_MENU,
  OVJ_ADMIN_MENU,
  OFFICE_ADMIN_MENU,
  DRIVER_MENU
} from 'src/app/shared/admin-sidebar-routes';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: [],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class SidebarComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input('loggedUser') loggedUser: User;
  @Output('sidebarItemEvent') itemEvent = new EventEmitter<string>();
  @Output('burgerEvent') burgrEvent = new EventEmitter<boolean>();

  dateTime = {
    day: '',
    hour: '',
    month: '',
    numberDay: '',
  };
  loggedUserSubs: Subscription;
  menu: any[] = [];
  selectedMenuItem: string = '';
  userRoleEnum = UserRoleEnum;
  public adminMainContainer;

  dateTime$: Observable<any>;
  dateTimeSubs: Subscription;
      
  constructor(
    private router: Router,
    public authService: AuthService
  ) {
    this.initLoggedUserSubs();
    this.initDateTime();
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.loggedUserSubs?.unsubscribe()
    this.dateTimeSubs.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.adminMainContainer = document.querySelector('.admin-main-container');
    asapScheduler.schedule(() => this.initMenu());
  }

  initMenu() {
    switch(this.loggedUser.role) {
      case UserRoleEnum.OVJ_ADMIN:
        this.menu = OVJ_ADMIN_MENU
        break;
      case UserRoleEnum.COMPANY_ADMIN:
        this.menu = COMPANY_ADMIN_MENU;
        break;
      case UserRoleEnum.OFFICE_ADMIN:
        this.menu = OFFICE_ADMIN_MENU;
        break;
      case UserRoleEnum.DRIVER:
        this.menu = DRIVER_MENU;
        break;
      default:
        this.menu = [];
    }
    this.checkActiveMenuItem();
  }

  initLoggedUserSubs() {
    this.authService.loggedUser$.pipe(take(1)).subscribe( (user: User) => {
      if(user) {
        this.initMenu();
      }
    });
  }

  onResize(event) {
    if (window.innerWidth > 768) {
      if (this.adminMainContainer.classList.contains('toggled')) {
        this.sidebarBackDropClicking();
      }
    }
  }

  toggleMenuItem(menuItem: any) {
    menuItem.opened = !menuItem.opened;
  }

  goToUrl(item: any) {
    this.itemEvent.emit(item.url);
    if (!item.submenu) {
      this.sidebarBackDropClicking();
    }
  }

  checkActiveRoute(subItemUrl: string): boolean {
    let res = false;
    if (this.router.url.includes(subItemUrl) &&
      subItemUrl !== '' &&
      subItemUrl !== '/') {
      res = true;
    }
    return res;
  }

  checkActiveMenuItem() {
    let activeItem = -1;
    this.menu.forEach((item: any) => {
      if(this.checkActiveRoute(item.url)) {
        item.opened = true;
        activeItem = 1;
      };
      (<any[]>item.submenu).forEach((subItem: any) => {
        if(this.checkActiveRoute(subItem.url)) {
          item.opened = true;
          activeItem = 1;
        }
      });
    });
    if(activeItem === -1) this.menu[0].opened = true;
  }

  sidebarBackDropClicking() {
    this.burgrEvent.emit(true);
  }

  initDateTime() {
    this.dateTime$ = timer( 0, 1000 ).pipe(
      tap( t => {
        let screenshotMoment = moment();
        this.dateTime.day = screenshotMoment.format( 'dddd' );
        this.dateTime.hour = screenshotMoment.format( 'HH:mm' );
        this.dateTime.month = screenshotMoment.format( 'MMMM' );
        this.dateTime.numberDay = screenshotMoment.format( 'D' );
      })
    );
    this.dateTimeSubs = this.dateTime$.subscribe();
  }
  redirectToHome():void{
    this.router.navigateByUrl('dashboard');
  }
}