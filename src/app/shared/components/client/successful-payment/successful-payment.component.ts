import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-successful-payment',
  templateUrl: './successful-payment.component.html',
  styleUrls: ['./successful-payment.component.scss']
})
export class SuccessfulPaymentComponent implements OnInit, AfterViewInit {

  @Output('seeDetailEvent') seeDetailEvent = new EventEmitter<boolean>();

  constructor(
    private ticketPurchaseService: TicketPurchaseService
  ) {
    this.ticketPurchaseService.loadingSteperContent = true;
  }

  get paymentMethod() {
    return this.ticketPurchaseService.paymentMethod;
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout( () => {
      this.ticketPurchaseService.reloadSelectedDeparture();
    }, 1)
  }

  buttonText(): string {
    return '< Volver Atrás';
  }

  public backToHome() {
    this.ticketPurchaseService.backToHome();
  }

  public detail(): void{
    this.seeDetailEvent.emit(true);
  }

  public downloadTicket(): void{}
}