import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-ticket-purchase-travel-card',
  templateUrl: './ticket-purchase-travel-card.component.html',
  styleUrls: ['./ticket-purchase-travel-card.component.scss']
})
export class TicketPurchaseTravelCardComponent implements OnInit {

  @Input('selectedDeparture') selectedDeparture: Departure;
  @Input('selectedPositions') selectedPositions: string[];
  @Output('selectTravel') selectTravelEvent = new EventEmitter<boolean>();

  constructor() {}

  get availableSeats(): number {
    let availableSeats = 0;
    this.selectedDeparture?.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === TicketStatusEnum.AVAILABLE) {
        availableSeats++;
      }
    });
    return availableSeats;
  }

  get toPay(): number {
    return this.selectedDeparture?.price * this.selectedPositions?.length;
  }

  ngOnInit(): void {}

  selectTravel() {
    this.selectTravelEvent.emit(true);
  }
}