import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-seller-travel-card',
  templateUrl: './seller-travel-card.component.html',
  styleUrls: ['./seller-travel-card.component.scss']
})
export class SellerTravelCardComponent implements OnInit {

  @Output('selectTravel') selectTravelEvent = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  selectTravel() {
    this.selectTravelEvent.emit(true);
  }
}