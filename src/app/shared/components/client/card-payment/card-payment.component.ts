import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { CardPaymentFormComponent } from 'src/app/shared/forms';

@Component({
  selector: 'app-card-payment',
  templateUrl: './card-payment.component.html',
  styleUrls: ['./card-payment.component.scss']
})
export class CardPaymentComponent implements OnInit {

  @Output('payWithCardEvent') payWithCardEvent = new EventEmitter<any>();
  @ViewChild(CardPaymentFormComponent) cardPaymentForm: CardPaymentFormComponent;

  constructor() { }

  ngOnInit(): void {}

  onSubmit() {
    const cardPaymentForm = this.cardPaymentForm.onSubmit();
    this.payWithCardEvent.emit(cardPaymentForm);
  }
}