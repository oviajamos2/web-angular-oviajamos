import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-qr-scan',
  templateUrl: './qr-scan.component.html',
  styleUrls: ['./qr-scan.component.scss']
})
export class QrScanComponent implements OnInit {

  @Output('verifyPaymentEvent') verifyPaymentEvent = new EventEmitter<boolean>();
  @Output('downloadQrEvent') downloadQrEvent = new EventEmitter<boolean>();
  
  constructor() {}

  ngOnInit(): void {}

  verifyPayment() {
    this.verifyPaymentEvent.emit(true);
  }

  downloadQr() {
    this.downloadQrEvent.emit(true);
  }
}