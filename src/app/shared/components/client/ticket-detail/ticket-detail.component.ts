import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Departure } from 'src/app/core/http/departure';
import { TicketPurchaseService } from 'src/app/core/services/client/ticket-purchase.service';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent implements OnInit, OnDestroy {

  constructor(
    public ticketPurchaseService: TicketPurchaseService,
    private router: Router
  ) {
    this.ticketPurchaseService.step = 7;
  }

  get paymentMethod() {
    return this.ticketPurchaseService.paymentMethod;
  }

  get selectedDeparture(): Departure {
    return this.ticketPurchaseService.selectedDeparture;
  }

  ngOnInit(): void {
    if(!this.paymentMethod) this.router.navigateByUrl('/client/home/compra-boleto');
  }
  
  ngOnDestroy(): void {  
    this.ticketPurchaseService.step = 1;
    this.ticketPurchaseService.selectedDeparture = null;
    this.ticketPurchaseService.paymentMethod = null;
  }

  downloadTicket() {}

  backToHome() {
    this.ticketPurchaseService.backToHome();
  }
}