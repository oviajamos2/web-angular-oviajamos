import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import * as moment from 'moment';

@Component({
  selector: 'app-client-seating-scheme',
  templateUrl: './client-seating-scheme.component.html',
  styleUrls: ['./client-seating-scheme.component.scss']
})
export class ClientSeatingSchemeComponent implements OnInit {

  @Input('departure') departure: Departure;
  @Input('selectedTickets') selectedTickets: number[];
  @Output('selectSeatEvent') selectSeatEvent = new EventEmitter<Ticket>();
  @Output('unselectSeatEvent') unselectSeatEvent = new EventEmitter<Ticket>();

  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);
  today = moment();

  get seatingScheme(): SeatingScheme {
    return this.departure.bus.seatingScheme;
  }

  get tickets(): Ticket[] {
    return this.departure.tickets;
  }

  constructor() {}
  
  ngOnInit(): void {}

  selectSeat(seatElement: string) {
    if(!this.validDepartureDate()) return;
    if (isNaN(parseInt(seatElement))) return;
    let seatPosition = (parseInt(seatElement));
    const ticket: Ticket = this.tickets.find((ticket: Ticket) => seatPosition === ticket.position);
    if(ticket.status === TicketStatusEnum.AVAILABLE ) {
      if(!this.selectedTickets.includes(ticket.ticketId)) {
        this.selectSeatEvent.emit(ticket);
      } else {
        this.unselectSeatEvent.emit(ticket);
      }
    }
  }

  validDepartureDate(): boolean {
    if(this.departure?.departureDate.endDate) {
      return this.today.isBefore(
        moment(this.departure?.departureDate.endDate).add(1, 'day')
      );
    } else {
      return this.today.isBefore(
        moment(this.departure?.departureDate.startDate).add(1, 'day')
      );
    }
  }

  checkSeatStatus(seatElement): TicketStatusEnum {
    if (isNaN(parseInt(seatElement))) return;
    let seatPosition = (parseInt(seatElement));
    const ticket: Ticket = this.tickets.find((ticket: Ticket) => seatPosition === ticket.position);
    if (this.selectedTickets.includes(ticket.ticketId)) return TicketStatusEnum.SELECTED;
    return ticket.status;
  }
}