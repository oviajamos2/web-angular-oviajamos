import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import QrScanner from 'qr-scanner/qr-scanner.min.js';
// QrScanner.WORKER_PATH = '@qr-scanner/qr-scanner-worker.min.js';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.component.html',
  styleUrls: ['./qr-scanner.component.scss']
})
export class QrScannerComponent implements OnInit, AfterViewInit {
  
  @ViewChild('scannerScreen') videoElem: any;
  qrScanner;
  result: any;
  camHasCamera: any;
  camList: any[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }
  
  ngAfterViewInit(): void {
    QrScanner.listCameras(true).then(cameras => cameras.forEach(camera => {
      this.camList.push(camera);
      QrScanner.hasCamera().then(hasCamera => this.camHasCamera = hasCamera);
      QrScanner.WORKER_PATH = 'assets/qr-scanner-worker.min.js';
      this.qrScanner = new QrScanner(this.videoElem.nativeElement, result => {
        this.result = result;
      });
      this.qrScanner.start().then(() => {
      });
    }));
  }
}