import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take, auditTime } from 'rxjs/operators';

import { ALERT_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';
import { DepartmentEnum } from 'src/app/core/enums/department.enum';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from '../../../core/services/notification/notification.service';
import { Office, OfficeService } from 'src/app/core/http/office';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

type ShipmentSearchParams = {
  destinationOfficeId?: number
}

@Component({
  selector: 'app-warehouse-offices-filter',
  templateUrl: './warehouse-offices-filter.component.html',
  styleUrls: ['./warehouse-offices-filter.component.scss']
})
export class WarehouseOfficesFilterComponent implements OnInit, OnDestroy {

  @Input('enterprise') enterprise: Enterprise;
  @Output('filterPackagesEvent') filterPackagesEvent = new EventEmitter<ShipmentSearchParams>();

  departamentEnum = DepartmentEnum;
  departamentEnumKeys = Object.keys(this.departamentEnum);
  destinationDepartmentSubs: Subscription;
  destinationOffices: Office[] = [];
  destinationOfficeSelectSubs: Subscription;
  selectedDepartment: DepartmentEnum = null;
  submitFilterParams$ = new Subject<ShipmentSearchParams>();
  submitFilterParamsSubs: Subscription;
  submitted = false;
  warehouseFilterGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private officeService: OfficeService,
    private notificationService: NotificationService,
    private router: Router
  ) {}

  get warehouseFilterForm() {
    return this.warehouseFilterGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
    this.submitFilterParams();
  }

  ngAfterViewInit(): void {
    this.initDestinationOfficeSelectSubs();
  }

  ngOnDestroy(): void {
    this.destinationDepartmentSubs?.unsubscribe();
    this.destinationOfficeSelectSubs?.unsubscribe();
    this.submitFilterParamsSubs?.unsubscribe();
  }

  public buildForm() {
    this.warehouseFilterGroup = this.formBuilder.group({
      destinationDepartment: ['', Validators.compose([Validators.required])],
      destinationOfficeId: ['', Validators.compose([Validators.required])],
    });
    this.initDestinationDepartmentSubs();
  }

  submitFilterParams() {
    this.submitFilterParamsSubs = this.submitFilterParams$.pipe(
      auditTime(100),
      distinctUntilChanged()
    ).subscribe((value: ShipmentSearchParams) => {
      this.filterPackagesEvent.emit(value);
    });
  }

  initDestinationDepartmentSubs() {
    this.destinationDepartmentSubs = this.warehouseFilterForm.destinationDepartment.valueChanges.pipe(
      auditTime(100),
      distinctUntilChanged()
    ).subscribe((value: DepartmentEnum) => {
      this.warehouseFilterForm.destinationOfficeId.setValue('');
      if(value) {
        this.selectedDepartment = value;
        this.loadDestinationOffices(this.selectedDepartment);
      } else {
        this.selectedDepartment = null;
        this.destinationOffices = [];
      }
    });
  }

  loadDestinationOffices(department: DepartmentEnum) {
    this.officeService.getAll({ companyId: this.enterprise.companyId, department }).pipe(
      take(1)
    ).subscribe( (response: PaginationResponse<Office>) => {
      this.destinationOffices = (response.items as Office[]);
      if(this.destinationOffices.length === 0) {
        this.notificationService.notify({
          ...ALERT_NOTIFICATION,
          message: `Se requiere de oficinas en ${ this.selectedDepartment } para continuar`,
          link: ['Crear oficinas', '/dashboard/oficina/create']
        });
      }
    });
  }

  initDestinationOfficeSelectSubs() {
    this.destinationOfficeSelectSubs = fromEvent(document.querySelectorAll('.destination-department-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.officeService.getAll({
        companyId: this.enterprise.companyId,
        search: value,
        department: this.selectedDepartment
      }).pipe(take(1)).subscribe( (response: PaginationResponse<Office>) => {
        this.destinationOffices = response.items;
        if(this.destinationOffices.length === 0) {
          this.notificationService.notify({
            ...ALERT_NOTIFICATION,
            message: `Se requiere de oficinas en ${ this.selectedDepartment } para continuar`,
            link: ['Crear oficinas', '/dashboard/oficina/create']
          });
        }
      });
    });
  }

  createShipment() {
    this.router.navigateByUrl('/dashboard/encomienda/registrar');
  }
}