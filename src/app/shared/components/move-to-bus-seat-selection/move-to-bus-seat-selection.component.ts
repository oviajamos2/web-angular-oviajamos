import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { Departure } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { ModalService } from 'src/app/core/services';
import { MoveSeatToBusConfirmationComponent } from '../../modals';
import { MoveBusySeatToBusConfirmationComponent } from '../../modals/move-busy-seat-to-bus-confirmation/move-busy-seat-to-bus-confirmation.component';

@Component({
  selector: 'app-move-to-bus-seat-selection',
  templateUrl: './move-to-bus-seat-selection.component.html',
  styleUrls: ['./move-to-bus-seat-selection.component.scss']
})
export class MoveToBusSeatSelectionComponent implements OnInit {

  @Input('actualDeparture') actualDeparture: Departure;
  @Input('selectedDeparture') selectedDeparture: Departure
  @Input('departures') departures: Departure[];
  @Input('seatSold') seatSold: boolean;
  @Input('selectedSeatIndex') selectedSeatIndex: number;
  @Input('selectedTicket') selectedTicket: Ticket;
  @Input('selectedTickets') selectedTickets: number[];
  @Output('backToDeparturesEvent') backToDeparturesEvent = new EventEmitter<boolean>();
  @Output('changeSeatEvent') changeSeatEvent = new EventEmitter<boolean>();

  modalRef: NgbModalRef;

  constructor(
    private modalService: ModalService
  ) {}

  ngOnInit(): void {}

  selectNewSeat(seatElement: string) {
    let selectedSeatIndex = parseInt(seatElement) - 1
    let newSeat = this.selectedDeparture.tickets[selectedSeatIndex];
    if(newSeat.ticketId === this.selectedTicket.ticketId) return;
    if(newSeat.status === TicketStatusEnum.AVAILABLE) {
      this.selectedSeatIndex = parseInt(seatElement) - 1;
      this.moveSeatToBus(newSeat);
    } else if(newSeat.status === TicketStatusEnum.SOLD) {
      this.selectedSeatIndex = parseInt(seatElement) - 1;
      this.seatSold = true;
      this.moveBusySeatToBus(newSeat);
    }
  }

  moveSeatToBus(newSeat: Ticket) {
    let modalData: ModalData = {
      state: {
        actualDeparture: this.actualDeparture,
        selectedDeparture: this.selectedDeparture,
        newTicket: newSeat,
        selectedTicket: this.selectedTicket,
      }
    };
    this.modalRef = this.modalService.open(MoveSeatToBusConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let reserveModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
      if(result) {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
        this.changeSeatEvent.emit(true);
      } else {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
      }
    });
  }
  
  moveBusySeatToBus(newSeat: Ticket) {
    let modalData: ModalData = {
      state: {
        actualDeparture: this.actualDeparture,
        selectedDeparture: this.selectedDeparture,
        newTicket: newSeat,
        selectedTicket: this.selectedTicket,
      }
    };
    this.modalRef = this.modalService.open(MoveBusySeatToBusConfirmationComponent);
    this.modalRef.componentInstance.modalData = modalData;
    let reserveModalSubs: Subscription =  this.modalRef.componentInstance.action.subscribe( (result: any) => {
      if(result) {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
        this.changeSeatEvent.emit(true);
      } else {
        this.modalRef.close();
        reserveModalSubs.unsubscribe();
      }
    });
  }

  backToDepartures() {
    this.backToDeparturesEvent.emit(true);
  }
}