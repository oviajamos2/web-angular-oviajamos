import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SteperForm } from 'src/app/core/services/steper-form/steper-form';

@Component({
  selector: 'app-steper-actions',
  templateUrl: './steper-actions.component.html',
  styleUrls: []
})
export class SteperActionsComponent implements OnInit {

  @Input('steper') steper: SteperForm;
  @Input('submitting') submitting: boolean;
  @Output('onToHomeEvent') onToHomeEvent = new EventEmitter<boolean>();
  @Output('onPrevEvent') onPrevEvent = new EventEmitter<boolean>();
  @Output('onSubmitEvent') onSubmitEvent = new EventEmitter<boolean>();
  @Output('onNextEvent') onNextEvent = new EventEmitter<boolean>();

  get currentStep(): number {
    return this.steper.currentStep;
  }

  get steps(): number {
    return this.steper.steps;
  }

  constructor() {}

  ngOnInit(): void {}

  back() {
    this.onPrevEvent.emit(true);
  }

  onSubmit() {
    this.onSubmitEvent.emit(true);
  }

  onNext() {
    this.onNextEvent.emit(true);
  }

  backToHome() {
    this.onToHomeEvent.emit(true);
  }
}
