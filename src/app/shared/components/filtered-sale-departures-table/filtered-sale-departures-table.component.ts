import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Departure, DepartureStatusEnum } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ModalService } from 'src/app/core/services';
import { DepartureStatusComponent } from '../../modals/mobile/departure-status/departure-status.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-filtered-sale-departures-table',
  templateUrl: './filtered-sale-departures-table.component.html',
  styleUrls: ['./filtered-sale-departures-table.component.scss']
})
export class FilteredSaleDeparturesTableComponent implements OnInit {

  @Input('departures') departures: Departure[];
  @Input('modifying') modifying: number[];
  @Input('loadingDepartures') loadingDepartures: boolean;
  @Input('initialized') initialized: boolean;
  @Input('popupMode') popupMode: boolean;
  @Input('selectedDeparture') selectedDeparture: Departure;
  @Output('selectDepartureEvent') selectDepartureEvent = new EventEmitter<[Departure, MouseEvent]>();
  @Output('setStatusEvent') setStatusEvent = new EventEmitter<[Departure,DepartureStatusEnum]>();


  colsTable = [
    { name: 'Horario', orderBy: true },
    { name: 'Orden', orderBy: false },
    { name: 'Ruta', orderBy: false },
    { name: 'Bus', orderBy: false },
    { name: 'Precio', orderBy: false },
    { name: 'Asientos', orderBy: false },
    { name: 'Estado salida', orderBy: false }
  ];
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  modalRef: NgbModalRef;

  constructor(
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    if(this.popupMode) this.colsTable.splice(6, 1);
  }

  getDepartureAvailableSeats(departure: Departure): number {
    let total = 0;
    departure.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === TicketStatusEnum.AVAILABLE) total++;
    });
    return total;
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  setStatus(departure: Departure, statusKey: DepartureStatusEnum) {
    this.setStatusEvent.emit([departure, statusKey]);
  }

  selectDeparture( departure: Departure, event: MouseEvent ) {
    if(!event.defaultPrevented) {
      this.selectDepartureEvent.emit([departure, event]);
    }
  }

  departureStatus(departure: Departure) {
    this.modalRef = this.modalService.open(
      DepartureStatusComponent,
      'mobile-modal bordered'
    );
    let statusModal: Subscription = this.modalRef.componentInstance.action
      .subscribe((result: DepartureStatusEnum) => {
      statusModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.setStatus(departure, result);
      }
    });
  }
}