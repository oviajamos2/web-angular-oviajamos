import { Component, OnInit, ViewChild } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { BankService } from 'src/app/core/http/bank';
import { BusStopService } from 'src/app/core/http/bus-stop';
import { BusService } from 'src/app/core/http/bus';
import { DepartureService } from 'src/app/core/http/departure';
import { DriverService } from 'src/app/core/http/driver';
import { EnterpriseService } from 'src/app/core/http/enterprise';
import { SeatingSchemeService } from 'src/app/core/http/seating-scheme';
import { OfficeAdminService } from 'src/app/core/http/office-admin';
import { TravelRouteService } from 'src/app/core/http/travel-route';
import { NotificationData } from 'src/app/core/services/notification/notificationData.interface';

@Component({
  selector: 'app-notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.scss']
})
export class NotifierComponent implements OnInit {

  @ViewChild('customNotification', { static: true }) customNotificationTmpl;

  public notificationData;

  constructor(
    private notifierService: NotifierService,
    private driverService: DriverService,
    private busStopService: BusStopService,
    private travelRouteService: TravelRouteService,
    private busService: BusService,
    private departureService: DepartureService,
    private enterpriseService: EnterpriseService,
    private officeAdminService: OfficeAdminService,
    private seatingSchemeService: SeatingSchemeService,
    private bankService: BankService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public notify(notificationData: NotificationData) {
    let { type, color, objectId, objectType, link } = notificationData;
    this.notificationData = {
      icon: type,
      type: color,
      message: notificationData.message || this.getMessageByType(type),
      template: this.customNotificationTmpl,
      id: objectId,
      objectType,
      objectId,
      link
    };
    this.notifierService.show(this.notificationData);
  }

  public hideNotification(id: string) {
    this.notifierService.hide(id);
  }

  public redirect(notificationData: NotificationData) {
    this.router.navigateByUrl(notificationData.link[1]);
    this.hideNotification(notificationData.id);
  }

  public undoDeleting(notificationData: NotificationData) {
    if(notificationData.recovered) return;
    notificationData.recovered = true;
    this.hideNotification(notificationData.id);
    switch( notificationData.objectType ) {
      case 'bus':
        this.busService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.busService.recoveredBus$.next(true);
        }, error => {
          this.busService.recoveredBus$.next(false);
        });
        break;
      case 'busStop':
        this.busStopService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.busStopService.recoveredBusStop$.next(true);
        }, error => {
          this.busStopService.recoveredBusStop$.next(false);
        });
        break;
      case 'departure':
        this.departureService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.departureService.recoveredDeparture$.next(true);
        }, error => {
          this.departureService.recoveredDeparture$.next(false);
        });
        break;
      case 'driver':
        this.driverService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.driverService.recoveredDriver$.next(true);
        }, error => {
          this.driverService.recoveredDriver$.next(false);
        });
        break;
      case 'enterprise':
        this.enterpriseService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.enterpriseService.recoveredEnterprise$.next(true);
        }, error => {
          this.enterpriseService.recoveredEnterprise$.next(false);
        });
        break;
      case 'officeAdmin':
        this.officeAdminService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.officeAdminService.recoveredOfficeAdmin$.next(true);
        }, error => {
          this.officeAdminService.recoveredOfficeAdmin$.next(false);
        });
        break;
      case 'travelRoute':
        this.travelRouteService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.travelRouteService.recoveredTravelRoute$.next(true);
        }, error => {
          this.travelRouteService.recoveredTravelRoute$.next(false);
        });
        break;
      case 'seatingScheme':
        this.seatingSchemeService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.seatingSchemeService.recoveredSeatingScheme$.next(true);
        }, error => {
          this.seatingSchemeService.recoveredSeatingScheme$.next(false);
        });
        break;
      case 'bank':
        this.bankService.recover(notificationData.objectId).pipe(take(1)).subscribe( response => {
          this.bankService.recoveredBank$.next(true);
        }, error => {
          this.bankService.recoveredBank$.next(false);
        });
    }
  }

  getMessageByType(type: string): string {
    let message = '';
    switch(type) {
      case 'save':
        message = 'Los cambios han sido guardados exitosamente';
        break;
      case 'delete':
        message = 'Se ha eliminado exitosamente';
        break;
      case 'serverError':
        message = 'Error del servidor';
        break;
      case 'conError':
        message = 'Error de conexión';
        break;
      case 'recovered':
        message = 'La acción se ha deshecho';
        break;
      default:
        message = '...';
      }
      return message;
  }
}
