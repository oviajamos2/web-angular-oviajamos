import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { Departure } from 'src/app/core/http/departure';
import { Ticket,TicketStatusEnum } from '../../../core/http/ticket';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { TicketSaleService } from '../../../core/services/ticket-sale.service';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';

@Component({
  selector: 'app-move-seat',
  templateUrl: './move-seat.component.html',
  styleUrls: ['./move-seat.component.scss']
})
export class MoveSeatComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  selectedSeatIndex: number = null;
  seatSold: boolean;

  get departure(): Departure {
    return this.ticketSaleService.selectedDeparture;
  }

  get selectedTicket(): Ticket {
    return this.ticketSaleService.selectedDeparture.tickets.find( (ticket: Ticket) => ticket.ticketId === this.modalData.state.selectedTicket.ticketId );
  }

  get selectedTickets(): number[] {
    return this.ticketSaleService.selectedTickets;
  }

  get seatingScheme(): SeatingScheme {
    return this.ticketSaleService.selectedDeparture.bus.seatingScheme;
  }

  constructor(
    private ticketSaleService: TicketSaleService
  ) {}

  ngOnInit(): void {}

  selectNewSeat(seatElement: string) {
    let selectedSeatIndex = parseInt(seatElement) - 1;
    let newSeat = this.departure.tickets[selectedSeatIndex];
    if(newSeat.ticketId === this.selectedTicket.ticketId) return;
    if(newSeat.status === TicketStatusEnum.AVAILABLE) {
      this.selectedSeatIndex = parseInt(seatElement) - 1;
    } else if(newSeat.status === TicketStatusEnum.SOLD) {
      this.selectedSeatIndex = parseInt(seatElement) - 1;
      this.seatSold = true;
    }
  }

  changeSeat() {
    if(isNaN(this.selectedSeatIndex)) return;
    this.action.next([this.selectedTicket.ticketId, this.departure.tickets[this.selectedSeatIndex].ticketId]);
  }
}