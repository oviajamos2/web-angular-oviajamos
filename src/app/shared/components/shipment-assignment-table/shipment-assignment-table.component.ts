import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { Departure, DepartureStatusEnum } from 'src/app/core/http/departure';
import { DepartureStatusComponent } from 'src/app/shared/modals/mobile';
import { ModalService } from 'src/app/core/services';
import { TicketStatusEnum, Ticket } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-shipment-assignment-table',
  templateUrl: './shipment-assignment-table.component.html',
  styleUrls: ['./shipment-assignment-table.component.scss']
})
export class ShipmentAssignmentTableComponent implements OnInit {

  @Input('departures') departures: Departure[];
  @Input('modifying') modifying: number[];
  @Input('loadingDepartures') loadingDepartures: boolean;
  @Input('initialized') initialized: boolean;
  @Input('whitoutStatusOptions') whitoutStatusOptions: boolean;
  @Output('selectDepartureEvent') selectDepartureEvent = new EventEmitter<[Departure, MouseEvent]>();
  @Output('setStatusEvent') setStatusEvent = new EventEmitter<[Departure,DepartureStatusEnum]>();

  colsTable = [
    { name: 'Horario', orderBy: true },
    { name: 'Carril', orderBy: false },
    { name: 'Ruta', orderBy: false },
    { name: 'Matrícula', orderBy: false },
    { name: 'Tipo', orderBy: false },
    { name: 'Estado salida', orderBy: false }
  ];
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  modalRef: NgbModalRef;

  constructor(
    private modalService: ModalService
  ) {}

  ngOnInit(): void {
    if(this.whitoutStatusOptions) this.colsTable.splice(6, 1);
  }

  getDepartureAvailableSeats(departure: Departure): number {
    let total = 0;
    departure.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status === TicketStatusEnum.AVAILABLE) total++;
    });
    return total;
  }

  orderTableBy(index: number) {
    this.colsTable.forEach( col => {
      col.orderBy = false;
    });
    this.colsTable[index].orderBy = true;
  }

  setStatus(departure: Departure, statusKey: DepartureStatusEnum) {
    this.setStatusEvent.emit([departure, statusKey]);
  }

  selectDeparture( departure: Departure, event: MouseEvent ) {
    if(!event.defaultPrevented) {
      this.selectDepartureEvent.emit([departure, event]);
    }
  }

  departureStatus(departure: Departure) {
    this.modalRef = this.modalService.open(
      DepartureStatusComponent,
      'mobile-modal bordered'
    );
    let statusModal: Subscription = this.modalRef.componentInstance.action
      .subscribe((result: DepartureStatusEnum) => {
      statusModal.unsubscribe();
      this.modalRef.close();
      if( result ) {
        this.setStatus(departure, result);
      }
    });
  }
}
