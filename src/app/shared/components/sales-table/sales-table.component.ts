import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';

import { Ticket } from 'src/app/core/http/ticket';
import { Person } from 'src/app/core/http/person';
import { SelectionModeEnum } from 'src/app/modules/admin/pages/admin-dashboard/sales/enums/selection-mode.enum';

@Component({
  selector: 'app-sales-table',
  templateUrl: './sales-table.component.html',
  styleUrls: ['./sales-table.component.scss']
})
export class SalesTableComponent implements OnInit {

  @Input('ticketsPaidArray') ticketsPaidArray: FormArray;
  @Input('selectedTickets') selectedTickets: number[];
  @Input('selectionMode') selectionMode: SelectionModeEnum;
  @Output('moveSeatEvent') moveSeatEvent = new EventEmitter<number>();
  @Output('moveSeatToBusEvent') moveSeatToBusEvent = new EventEmitter<number>();

  floatInputValidator = floatInputValidator;
  numberInputValidator = numberInputValidator;
  persons: Person[] = [];
  selectionModeEnum = SelectionModeEnum;

  constructor() {}

  ngOnInit(): void {}

  getPersonGroup(ticketGroup: FormGroup): FormGroup {
    return (<FormGroup>ticketGroup.controls.person);
  }

  getFormGroup(seatIndex: number): FormGroup {
    if(!this.ticketsPaidArray) return;
    let ticketIndex = (<Ticket[]>this.ticketsPaidArray.value)
      .map( (ticket: Ticket) => ticket.position )
      .indexOf( seatIndex + 1 );
    return this.ticketsPaidArray[ticketIndex];
  }

  moveSeat(ticketId: number) {
    this.moveSeatEvent.emit(ticketId);
  }

  moveSeatToBus(ticketId: number) {
    this.moveSeatToBusEvent.emit(ticketId);
  }
}