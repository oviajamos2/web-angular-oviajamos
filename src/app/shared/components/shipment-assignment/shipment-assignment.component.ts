import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

import { Departure, DepartureSearchParams, DepartureService, DepartureStatusEnum } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { Enterprise } from 'src/app/core/http/enterprise';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { SERVER_ERROR_NOTIFICATION, SAVED_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-shipment-assignment',
  templateUrl: './shipment-assignment.component.html',
  styleUrls: ['./shipment-assignment.component.scss']
})
export class ShipmentAssignmentComponent implements OnInit {

  action = new Subject;
  departures: Departure[] = [];
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  initialized = false;
  loadingDepartures: boolean = false;
  modalData: ModalData;
  modifying: number[] = [];

  get enterprise(): Enterprise {
    return this.modalData?.state?.enterprise;
  }

  constructor(
    private departureService: DepartureService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
  }

  filterDeparturesEvent(event: DepartureSearchParams) {
    this.loadingDepartures = true;
    this.departureService.getAll({
      companyId: this.enterprise.companyId,
      ...event
    }).pipe(take(1)).subscribe((response: PaginationResponse<Departure>) => {
      this.departures = response.items;
      if(!this.initialized) this.initialized = true;
      this.loadingDepartures = false;
    }, (error) => {
      console.warn(error);
      this.loadingDepartures = false;
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION);
    });
  }

  selectDeparture([departure, event]: [Departure, MouseEvent]) {
    console.log( departure );
  }

  setStatus([departure, statusKey]:[Departure, DepartureStatusEnum]) {
    this.modifying.push(departure.departureId);
    let departureToPatch: Departure = JSON.parse(JSON.stringify(departure));
    departureToPatch.status = this.departureStatusEnum[statusKey];
    if(departureToPatch.status === this.departureStatusEnum.CANCELED) {
      departureToPatch.onSale = false;
    }
    this.departureService.patch(departureToPatch.departureId, departureToPatch).pipe(take(1)).subscribe( (response: Departure) => {
      let updatedDeparture = this.departures.find((departureInArray: Departure) => response.departureId === departureInArray.departureId);
      updatedDeparture.status = response.status;
      updatedDeparture.onSale = response.onSale;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
  }
}
