import { Component, Input, OnInit } from '@angular/core';
import { asapScheduler, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import * as moment from 'moment';

import { Departure, DepartureStatusEnum, DepartureService } from 'src/app/core/http/departure';
import { ModalData } from 'src/app/core/interfaces/modal-data.interface';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { SeatingScheme } from 'src/app/core/http/seating-scheme';
import { Ticket, TicketStatusEnum } from '../../../core/http/ticket';
import { TicketSaleService } from '../../../core/services/ticket-sale.service';
import { SAVED_NOTIFICATION, SERVER_ERROR_NOTIFICATION } from 'src/app/core/services/notification/default-notifications';

@Component({
  selector: 'app-move-seat-to-bus',
  templateUrl: './move-seat-to-bus.component.html',
  styleUrls: ['./move-seat-to-bus.component.scss']
})
export class MoveSeatToBusComponent implements OnInit {

  @Input() modalData: ModalData;

  action = new Subject();
  departureStatusEnum = DepartureStatusEnum;
  departureStatusEnumKeys = Object.keys(this.departureStatusEnum);
  modifying: number[] = [];
  selectedSeatingScheme: SeatingScheme;
  ticketStatusEnum = TicketStatusEnum;
  ticketStatusEnumKeys = Object.keys(this.ticketStatusEnum);
  selectedSeatIndex: number;
  selectedDeparture: Departure = null;
  soldSeat: boolean;

  get departure(): Departure {
    return this.ticketSaleService.selectedDeparture;
  }

  get selectedTicket(): Ticket {
    return this.ticketSaleService.selectedDeparture.tickets.find( (ticket: Ticket) => ticket.ticketId === this.modalData?.state.selectedTicket.ticketId );
  }

  get departures(): Departure[] {
    return this.ticketSaleService.filteredDepartures2;
  }

  get selectedTickets(): number[] {
    return this.ticketSaleService.selectedTickets;
  }

  get seatingScheme(): SeatingScheme {
    return this.ticketSaleService.selectedDeparture.bus.seatingScheme;
  }

  get tickets(): Ticket[] {
    return this.ticketSaleService.selectedDeparture.tickets;
  }

  get defaultParams(): {source: number, destination: number, departureDate: string} {
    return {
      source: this.departure.route.source.busStopId,
      destination: this.departure.route.destination.busStopId,
      departureDate: moment(this.departure.departureDate.startDate).format('DD/MMMM/YYYY')
    }
  }

  get loadingDepartures(): boolean {
    return this.ticketSaleService.loadingDepartures2;
  }

  get initialized(): boolean {
    return this.ticketSaleService.initialized2;
  }

  constructor(
    private departureService: DepartureService,
    private notificationService: NotificationService,
    private ticketSaleService: TicketSaleService
  ) {}

  ngOnInit(): void {}
  
  setValues(modalData: ModalData) {
    asapScheduler.schedule(() => {
      this.modalData = modalData;
    });
  }

  filterDeparturesEvent(event) {
    asapScheduler.schedule(() => {
      this.selectedDeparture = null;
      this.ticketSaleService.loadDepartures2(event);
    });
  }

  setStatus([departure, statusKey]:[Departure, DepartureStatusEnum]) {
    this.modifying.push(departure.departureId);
    let departureToPatch: Departure = JSON.parse(JSON.stringify(departure));
    departureToPatch.status = this.departureStatusEnum[statusKey];
    if(departureToPatch.status === this.departureStatusEnum.CANCELED) {
      departureToPatch.onSale = false;
    }
    this.departureService.patch(departureToPatch.departureId, departureToPatch).pipe(take(1)).subscribe( (response: Departure) => {
      let updatedDeparture = this.departures.find((departureInArray: Departure) => response.departureId === departureInArray.departureId);
      updatedDeparture.status = response.status;
      updatedDeparture.onSale = response.onSale;
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SAVED_NOTIFICATION);
    }, (error) => {
      console.warn(error);
      this.modifying.splice(
        this.modifying.findIndex((id: number) => departure.departureId === id), 1
      );
      this.notificationService.notify(SERVER_ERROR_NOTIFICATION)
    });
  }

  selectDeparture([departure, event]: [Departure, MouseEvent]) {
    this.selectedDeparture = departure;
  }

  checkSeatStatus(seatElement): TicketStatusEnum {
    if (isNaN(parseInt(seatElement))) {
      return;
    }
    let seatIndex = (parseInt(seatElement)) - 1;
    const ticket = this.tickets[seatIndex];
    const {ticketId} = ticket;
    if (this.selectedTickets.includes(ticketId)) {
      return TicketStatusEnum.SELECTED;
    }
    return ticket.status;
  }

  getSeatNumber(position: string): string {
    let seatNumber = parseInt(position);
    return (seatNumber < 10) ? '0' + seatNumber : '' + seatNumber;
  }

  changeSeat() {
    if(isNaN(this.selectedSeatIndex)) return;
    this.action.next([this.selectedTicket.ticketId, this.departure.tickets[this.selectedSeatIndex].ticketId]);
  }

  backToDepartures() {
    this.selectedDeparture = null;
  }
}
