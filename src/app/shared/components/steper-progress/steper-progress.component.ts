import { Component, Input, OnInit } from '@angular/core';
import { SteperForm } from 'src/app/core/services/steper-form/steper-form';

@Component({
  selector: 'app-steper-progress',
  templateUrl: './steper-progress.component.html',
  styleUrls: []
})
export class SteperProgressComponent implements OnInit {

  @Input('steper') steper: SteperForm;
  @Input('ovjAdminMode') ovjAdminMode: boolean;
  @Input('lastCreatedStep') lastCreatedStep: number;

  get totalSteps(): number {
    return this.steper.steps;
  }

  get progressSteps() {
    return this.steper.progressSteps;
  }

  get currentStep(): number {
    return this.steper.currentStep;
  }

  constructor() {}

  ngOnInit(): void {}
}