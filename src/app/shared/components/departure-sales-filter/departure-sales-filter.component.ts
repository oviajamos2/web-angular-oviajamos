import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbDatepickerI18n, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { numberInputValidator }  from 'src/app/core/helpers/numberInputValidator.helper';
import { IDatePickerConfig } from 'ng2-date-picker';
import { defaultConf } from 'src/app/core/interfaces/datepicker-config.interface';
import { fromEvent, Subscription, Subject, asapScheduler } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take, auditTime } from 'rxjs/operators';
import { DATE_FORMAT } from 'src/app/shared/constants';
import * as moment from 'moment';

import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { CustomDatepickerI18n, I18n } from 'src/app/core/helpers/ngb-datepicker-i18n';
import { CustomNgbDateAdapter, CustomNgbDateFormatter } from 'src/app/core/adapters';
import { Departure, DepartureSearchParams } from 'src/app/core/http/departure';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-departure-sales-filter',
  templateUrl: './departure-sales-filter.component.html',
  styleUrls: ['./departure-sales-filter.component.scss'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateAdapter, useClass: CustomNgbDateAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomNgbDateFormatter }
  ]
})
export class DepartureSalesFilterComponent implements OnInit, OnDestroy {

  @Input('departureSearchParams') departureSearchParams: DepartureSearchParams;
  @Input('departure') departure: Departure;
  @Input('departures') departures: Departure[];
  @Input('withoutTime') withoutTime: boolean;
  @Input('initialized') initialized: boolean;
  @Input('loadingDepartures') loadingDepartures: boolean;
  @Input('defaultParams') defaultParams: { source: number, destination: number, departureDate: string | NgbDateStruct };
  @Output('selectDepartureEvent') selectDepartureEvent = new EventEmitter<Departure>();
  @Output('filterDeparturesEvent') filterDeparturesEvent = new EventEmitter<DepartureSearchParams>();

  busStops: BusStop[] = [];
  busStopSelectSubs: Subscription;
  datePickerConf: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  datePickerConf2: IDatePickerConfig = JSON.parse(JSON.stringify(defaultConf));
  filter$ = new Subject<DepartureSearchParams>();
  filterSubs: Subscription;
  minDate = this.dateAdapter.fromModel(moment().format(DATE_FORMAT));
  modifying: number[] = [];
  numberInputValidator;
  submitted = false;
  travelGroup: FormGroup;

  get today() {
    return this.dateAdapter.toModel(this.ngbCalendar.getToday())!;
  }

  get dateValue(): string {
    return moment(this.travelForm.departureDate.value, [DATE_FORMAT]).format('DD/MMMM/YYYY').toUpperCase();
  }

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService,
    private router: Router,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>
  ) {
    this.numberInputValidator = numberInputValidator;
    this.datePickerConf.format = 'DD/MMMM/YYYY';
    this.datePickerConf.unSelectOnClick = false;
  }

  get travelForm() {
    return this.travelGroup.controls;
  }

  ngOnInit(): void {
    this.buildForm();
    this.initFilterSubs();
  }
  
  ngAfterViewInit(): void {
    this.loadBusStops();
    this.initBusStopSelectSubs();
    this.patchForm();
  }

  ngOnDestroy(): void {
    this.busStopSelectSubs.unsubscribe();
    this.filterSubs?.unsubscribe();
  }

  buildForm() {
    this.travelGroup = this.formBuilder.group({
      source: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
      availableSeats: [1],
      departureDate: [moment().format(DATE_FORMAT), Validators.compose([Validators.required])],
      onSale: [true],
      departureTime: ['']
    });
  }

  patchForm() {
    asapScheduler.schedule(() => {
      if(this.defaultParams) {
        let params = { ...this.defaultParams };
        params.departureDate = moment(params.departureDate).format(DATE_FORMAT);
        this.travelGroup.patchValue(params);
        this.filterDepartures();
      }
    });
  }

  filterDepartures() {
    this.filter$.next(this.travelGroup.value);
  }

  initFilterSubs() {
    this.filterSubs = this.filter$.pipe(
      auditTime(100),
      distinctUntilChanged()
    ).subscribe((travelGroupValue: DepartureSearchParams) => {
      let searchParams = { ...travelGroupValue };
      if(!searchParams.source) delete searchParams.source;
      if(!searchParams.destination) delete searchParams.destination;
      if(searchParams.departureDate?.length === 0 || !searchParams.departureDate) delete searchParams.departureDate;
      searchParams.departureDate = moment(searchParams.departureDate, 'DD/MM/YYYY').toISOString();
      this.filterDeparturesEvent.emit( searchParams );
      if(!this.withoutTime) this.router.navigateByUrl('/dashboard/venta/salidas');
    });
  }

  loadBusStops() {
    this.busStopService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = (response.items as BusStop[]);
    });
  }

  initBusStopSelectSubs() {
    this.busStopSelectSubs = fromEvent(document.querySelectorAll('.bus-stop-filter'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe( (value: string) => {
      this.busStopService.getAll({ search: value }, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }

  selectDeparture([departure, event]: [Departure, MouseEvent]) {
    if(!event.defaultPrevented) {
      this.selectDepartureEvent.emit(departure);
    }
  }
}