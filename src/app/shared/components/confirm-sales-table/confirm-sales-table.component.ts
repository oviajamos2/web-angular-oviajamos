import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Person } from 'src/app/core/http/person';

@Component({
  selector: 'app-confirm-sales-table',
  templateUrl: './confirm-sales-table.component.html',
  styleUrls: ['./confirm-sales-table.component.scss']
})
export class ConfirmSalesTableComponent implements OnInit {

  @Input('ticketsPaidArray') ticketsPaidArray: FormArray;
  @Input('personGroup') personGroup: FormGroup;

  numberInputValidator = numberInputValidator;
  persons: Person[] = [];

  get personForm() {
    return this.personGroup.controls;
  }

  constructor() {}

  ngOnInit(): void {}
}