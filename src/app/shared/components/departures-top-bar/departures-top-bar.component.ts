import { Component, OnInit, Input, Output, ViewChild, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbDatepickerI18n, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DATE_FORMAT } from 'src/app/shared/constants';
import { fromEvent, Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, pluck, take, auditTime } from 'rxjs/operators';
import * as moment from 'moment';

import { BusStop, BusStopService } from 'src/app/core/http/bus-stop';
import { CustomDatepickerI18n, I18n } from 'src/app/core/helpers/ngb-datepicker-i18n';
import { CustomNgbDateAdapter, CustomNgbDateFormatter } from 'src/app/core/adapters';
import { DatePickerComponent } from 'ng2-date-picker';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';

@Component({
  selector: 'app-departures-top-bar',
  templateUrl: './departures-top-bar.component.html',
  styleUrls: ['./departures-top-bar.component.scss'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateAdapter, useClass: CustomNgbDateAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomNgbDateFormatter }
  ]
})
export class DeparturesTopBarComponent implements OnInit, OnDestroy {

  @Input('ShowAddDepartureButton') ShowAddDepartureButton : boolean;
  @Output('addDeparture') addDepartureEvent = new EventEmitter<boolean>();
  @Output('filterDepartureEvent') filterDepartureEvent = new EventEmitter<{source?: number, departureDate: string}>()
  @ViewChild('datepicker') datepicker: DatePickerComponent;

  busStops: BusStop[] = [];
  defaultBusStopImageUrl = 'assets/icons/location-bg-blue.svg';
  busStopImageUrl: string;
  sourceSelectSubs: Subscription;
  submitSubs: Subscription;
  submit$ = new Subject();
  topBarGroup: FormGroup;
  condition: boolean = false;

  get today() {
    return this.dateAdapter.toModel(this.ngbCalendar.getToday())!;
  }

  constructor(
    private formBuilder: FormBuilder,
    private busStopService: BusStopService,
    private ngbCalendar: NgbCalendar,
    private dateAdapter: NgbDateAdapter<string>
  ) {
    moment().locale('es');
  }

  get filterDepartureForm() { return this.topBarGroup.controls; }

  get day() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('dddd')}
  get numberDay() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('D')}
  get month() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('MMMM')}
  get year() { return moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).format('YYYY')}

  get departureId(): number {
    return this.topBarGroup.controls.source.value;
  }

  get date(): string {
    return this.topBarGroup.controls.departureDate.value;
  }

  ngOnInit(): void {
    this.buildForm();
    this.loadBusStops();
    this.initSubmitSubs();
  }

  ngAfterViewInit(): void {
    this.initSourceSelectSubs();
  }

  ngOnDestroy(): void {
    this.sourceSelectSubs.unsubscribe();
    this.submitSubs?.unsubscribe();
  }

  public buildForm() {
    this.topBarGroup = this.formBuilder.group({
      source: [null, Validators.compose([Validators.required])],
      departureDate: [moment().format(DATE_FORMAT)]
    });
    this.initSourceValueChanges();
    this.onSubmit();
  }

  onSubmit(event?: NgbDateStruct) {
    this.updateInputValues(event);
    this.filterDepartureEvent.emit({
      source: <number>this.filterDepartureForm.source.value,
      departureDate: moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).toISOString()
    });
  }

  initSubmitSubs() {
    this.submitSubs = this.submit$.pipe(
      auditTime(100)
    ).subscribe((event: NgbDateStruct) => {
      this.onSubmit(event);
    });
  }

  updateInputValues(event: NgbDateStruct | any) {
    if(event) {
      if(event.busStopId) {
        this.topBarGroup.controls.source.setValue(event.busStopId);
      } else {
        this.topBarGroup.controls.departureDate.setValue(
          this.dateAdapter.toModel(event)
        );
      }
    }
  }

  loadBusStops() {
    this.busStopService.getAll({}, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
      this.busStops = response.items;
    })
  }

  initSourceSelectSubs() {
    this.sourceSelectSubs = fromEvent(document.querySelectorAll('.sourceSelect'), 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged()
    ).subscribe((value: string) => {
      this.busStopService.getAll({ search: value }, true).pipe(take(1)).subscribe( (response: PaginationResponse<BusStop>) => {
        this.busStops = response.items;
      });
    });
  }

  prevDay() {
    this.filterDepartureForm.departureDate.setValue(
      moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).subtract(1, 'days').format(DATE_FORMAT)
    );
    this.onSubmit()
  }

  nextDay() {
    this.filterDepartureForm.departureDate.setValue(
      moment(this.filterDepartureForm.departureDate.value, [DATE_FORMAT]).add(1, 'days').format(DATE_FORMAT)
    );
    this.onSubmit();
  }

  addDeparture() {
    this.addDepartureEvent.emit(true);
  }

  initSourceValueChanges() {
    this.filterDepartureForm.source.valueChanges.subscribe( (value: number) => {
      if(value) {
        this.busStops.find( (busStop: BusStop) => {
          if(busStop.busStopId === value) {
            this.busStopImageUrl = busStop.imageUrl;
          }
        })
      }
    })
  }

  capitalize( val: string ) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  }
}