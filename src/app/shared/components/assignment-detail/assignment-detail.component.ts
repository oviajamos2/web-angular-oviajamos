import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { numberInputValidator } from 'src/app/core/helpers/numberInputValidator.helper';
import { Subscription } from 'rxjs';
import floatInputValidator from 'src/app/core/helpers/floatInputValidator.helper';

import { Package } from 'src/app/core/http/shipment';

@Component({
  selector: 'app-assignment-detail',
  templateUrl: './assignment-detail.component.html',
  styleUrls: ['./assignment-detail.component.scss']
})
export class AssignmentDetailComponent implements OnInit, OnDestroy {

  @Input('shipmentFormGroup') shipmentGroup: FormGroup;
  @Input('submitted') submitted: boolean;

  floatInputValidator = floatInputValidator;
  numberInputValidator = numberInputValidator;
  packagesChangeValuesSubs: Subscription;

  get shipmentForm() {
    return this.shipmentGroup.controls;
  }

  get packagesFormArray(): FormArray {
    return (<FormArray>this.shipmentForm.packages);
  }

  get totalPrice(): number {
    let total: number = 0;
    this.packagesFormArray.value.forEach((pack: Package) => {
      if(!isNaN(pack.price)) {
        total += pack.price;
      }
    });
    return total;
  }

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.packagesChangeValuesSubs = this.shipmentForm.packages.valueChanges.subscribe(() => {
      this.shipmentForm.totalPrice.setValue(this.totalPrice);
    });
  }

  ngOnDestroy(): void {
    this.packagesChangeValuesSubs?.unsubscribe();
  }

  addPackage() {
    this.packagesFormArray.push(this.formBuilder.group({
      quantity: ['', [Validators.required]],
      description: ['', [Validators.required]],
      isFragile: [false],
      weight: ['', [Validators.required]],
      price: ['', [Validators.required]]
    }));
  }

  removePackage(index: number) {
    if(this.packagesFormArray.length === 1) return;
    this.packagesFormArray.removeAt(index);
  }
}
