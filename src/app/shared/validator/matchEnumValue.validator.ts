import { FormGroup } from '@angular/forms';

export function MatchEnumValueValidator(controlName: string,
                                        en: Record<string, string | number>) {
  return (formGroup: FormGroup) => {
    const value = formGroup.controls[controlName];
    let enumKeys = Object.keys(en);
    let validVal = false;
    for(let i=0; i<enumKeys.length ; i++) {
      if(value.value === enumKeys[i]) {
        validVal = true;
        break;
      }
    }
    if( !validVal ) {
      value.setErrors({ matchEnumValue: true });
    } else {
      value.setErrors(null);
    }
  }
}