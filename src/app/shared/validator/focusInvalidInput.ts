import { asyncScheduler } from "rxjs";

const focusInvalidInput = () => {
  asyncScheduler.schedule(() => {
    document.querySelectorAll('.is-invalid')[0]?.scrollIntoView({ behavior: 'smooth' });
  });
};

export default focusInvalidInput;