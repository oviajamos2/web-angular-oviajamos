import { Component, Input, OnInit } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { Ticket, TicketStatusEnum } from 'src/app/core/http/ticket';

@Component({
  selector: 'app-departure-manifest',
  templateUrl: './departure-manifest.component.html',
  styleUrls: ['./departure-manifest.component.css']
})
export class DepartureManifestComponent implements OnInit {

  @Input('departure') departure: Departure;

  ticketStatusEnum = TicketStatusEnum;

  get totalAmount(): string {
    let response = 0;
    this.departure.tickets.forEach( (ticket: Ticket) => {
      if(ticket.status !== this.ticketStatusEnum.SOLD) return;
      response += ticket.amount
    });
    return response.toFixed(2);
  }

  constructor() {}

  ngOnInit(): void {}
}