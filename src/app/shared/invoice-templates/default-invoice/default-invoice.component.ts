import { Component, Input, OnInit } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { PaymentMethod, Invoice } from 'src/app/core/http/payment-method';
import { User } from 'src/app/core/http/user';

@Component({
  selector: 'app-default-invoice',
  templateUrl: './default-invoice.component.html',
  styleUrls: ['./default-invoice.component.css']
})
export class DefaultInvoiceComponent implements OnInit {

  @Input('paymentMethod') paymentMethod: PaymentMethod;

  get invoice(): Invoice {
    return this.paymentMethod.invoice;
  }

  get seller(): User {
    return this.paymentMethod.seller || this.paymentMethod.__seller__;
  }

  get departure(): Departure {
    return this.paymentMethod.departure || this.paymentMethod.__departure__;
  }

  constructor() {}

  ngOnInit(): void {}
}