import { Component, Input, OnInit } from '@angular/core';
import { Departure } from 'src/app/core/http/departure';
import { PaymentMethod } from 'src/app/core/http/payment-method';
import { Invoice } from 'src/app/core/http/payment-method/models/invoice.model';
import { User } from 'src/app/core/http/user';
import { Person } from '../../../core/http/person/models/person.model';

@Component({
  selector: 'app-default-quote',
  templateUrl: './default-quote.component.html',
  styleUrls: ['./default-quote.component.css']
})
export class DefaultQuoteComponent implements OnInit {

  @Input('paymentMethod') paymentMethod: PaymentMethod;
  
  get invoice(): Invoice {
    return this.paymentMethod.invoice;
  }

  get ticketBuyer(): Person {
    return this.paymentMethod?.ticketBuyer?.ticketsPaid[0].person;
  }

  get seller(): User {
    return this.paymentMethod.__seller__;
  }

  get departure(): Departure {
    return this.paymentMethod.__departure__;
  }

  constructor() {}

  ngOnInit(): void {}
}