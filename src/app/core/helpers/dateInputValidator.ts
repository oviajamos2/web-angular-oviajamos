export const DATE_PATTERN = "^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$";

export function validateDateInput(event) {
  if(!isNaN(event.key) || !(event.key === '/'))
    event.preventDefault();
};