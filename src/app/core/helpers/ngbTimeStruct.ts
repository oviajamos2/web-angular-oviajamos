import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";

const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

export function toModel(time: NgbTimeStruct | null): string | null {
  return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
}

export function toNgbTime(time: string | null): NgbTimeStruct | null {
  if(!time) return null;
  const timeArray = time.split(':');
  return { hour: parseInt(timeArray[0]), minute: parseInt(timeArray[1]), second: parseInt(timeArray[2]) };
}