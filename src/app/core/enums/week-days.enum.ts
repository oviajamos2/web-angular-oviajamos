export enum WeekDays {
  l = 'Lunes',
  m = 'Martes',
  x = 'Miércoles',
  j = 'Jueves',
  v = 'Viernes',
  s = 'Sábado',
  d = 'Domingo'
}
