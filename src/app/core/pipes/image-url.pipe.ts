import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'imageUrl'
})
export class ImageUrlPipe implements PipeTransform {

  transform(imageRepositoryName: string): string {
    if(!imageRepositoryName) return;
    return `${ environment.apiURL }/image/${ imageRepositoryName }`;
  }
}