import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate2'
})
export class FormatDate2Pipe implements PipeTransform {

  transform(date: Date): string {
    let response = moment(date);
    return `${ response.format('DD') } de ${ response.format('MMMM') } ${ response.get('year') }`;
  }

}
