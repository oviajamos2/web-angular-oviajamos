import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'seatNumber'
})
export class SeatNumberPipe implements PipeTransform {

  transform(position: number|string): string|number {
    if(isNaN(parseInt(position as string))) return position;
    return (position < 10) ? `0${ position }` : `${ position }`;
  }

}
