import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimals'
})
export class DecimalsPipe implements PipeTransform {

  transform(number: number): string {
    let decimal = Math.floor(parseFloat((number % 1).toFixed(2)) * 100);
    if( decimal === 0 ) return '00';
    if( decimal < 10 ) return `0${ decimal }`;
    return decimal.toString();
  }

}