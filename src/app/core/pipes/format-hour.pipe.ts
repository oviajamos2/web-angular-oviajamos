import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatHour'
})
export class FormatHourPipe implements PipeTransform {

  transform(time: string): string {
    return moment(time, ['HH:mm']).format('h:mm A');
  }

}
