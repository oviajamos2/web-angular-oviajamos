import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate4'
})
export class FormatDate4Pipe implements PipeTransform {

  transform(date: Date): string {
    let response = moment(date);
    return `${ response.format('dddd').substr(0, 3) } ${ response.format('DD') } ${ response.format('MMMM').substr(0, 3) }.`.toUpperCase();
  }

}
