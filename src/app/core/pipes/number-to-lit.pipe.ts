import { Pipe, PipeTransform } from '@angular/core';
import numeroALetras from '../helpers/numberToLit';

@Pipe({
  name: 'numberToLit'
})
export class NumberToLitPipe implements PipeTransform {

  transform(number: number): string {
    return numeroALetras(Math.floor(number));
  }

}