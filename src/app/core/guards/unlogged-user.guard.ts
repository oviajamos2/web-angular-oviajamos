import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';

import { User } from '../http/user';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UnloggedUserGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.authService.loadedUser) {
      if(this.authService.loggedUser) {
        this.router.navigate(['/dashboard']);
        return false;
      } else {
        return true;
      }
    }
    return new Observable<boolean>((observer) => {
      this.authService.loggedUser$.subscribe( (loggedUser: User) => {
        if(loggedUser) {
          this.router.navigate(['/dashboard']);
          observer.next(false);
        } else {
          observer.next(true);
        }
        observer.complete();
      });
    });
  }
}
