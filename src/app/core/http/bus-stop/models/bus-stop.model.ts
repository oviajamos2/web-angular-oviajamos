export interface BusStop {
    busStopId?: number;
    name: string;
    address: string;
    imageUrl: string;
}