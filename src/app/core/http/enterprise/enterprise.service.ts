import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Enterprise } from './';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {
  resource = '/company';
  recoveredEnterprise$ = new Subject();

  constructor(
    private http: HttpClient
  ) {}

  create(data: Enterprise): Observable<Enterprise> {
    const href = `${this.resource}`;
    return this.http.post<Enterprise>(href, data);
  }

  patch(id: number, data: Enterprise): Observable<Enterprise> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Enterprise>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<Enterprise>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    const href = `${this.resource}`;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    return this.http.get<PaginationResponse<Enterprise>>(href, { params: searchParams });
  }

  get(id: number): Observable<Enterprise> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Enterprise>(href);
  }

  delete(id: number): Observable<Enterprise> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Enterprise>(href);
  }

  recover(id: number): Observable<Enterprise> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Enterprise>(href);
  }
}
