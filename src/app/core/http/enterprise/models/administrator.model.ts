import { DocumentTypeEnum, User } from "../../user";

export interface Administrator {
  administratorId?: number;
  documentType: DocumentTypeEnum;
  documentNumber: string;
  user: User;
}
