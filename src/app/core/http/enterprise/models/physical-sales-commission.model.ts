import { SalesCommissionBase, CompanyInvoiceTypeEnum } from '../';

export interface PhysicalSalesCommission extends SalesCommissionBase {
  physicalSalesCommissionId?: number;
  invoiceType: CompanyInvoiceTypeEnum;
}
