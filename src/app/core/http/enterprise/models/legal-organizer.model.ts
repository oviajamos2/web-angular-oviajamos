import { DocumentTypeEnum } from 'src/app/core/http/user';

export interface LegalOrganizerModel {
  firstName: string;
  lastName: string;
  documentType: DocumentTypeEnum;
  documentNumber: string;
  phoneNumber: string;
  address: string;
  legalOrganizerId?: number;
}
