import { CommissionTypeEnum } from '../';
import { Enterprise } from 'src/app/core/http/enterprise';

export interface SalesCommissionBase {
  value: number;
  type: CommissionTypeEnum;
  company?: Enterprise;
}
