export enum OnlineSalesCommissionTypeEnum {
  INCREMENTABLE_DECREMENTABLE = 'incremento/decremento',
  SALES_GOAL = 'meta de venta',
}
