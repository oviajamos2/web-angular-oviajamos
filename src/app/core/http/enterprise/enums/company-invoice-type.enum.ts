export enum CompanyInvoiceTypeEnum {
  ELECTRONIC_BILLING = 'Facturación electrónica',
  COMPUTERIZED_BILLING = 'Facturación computarizada'
}
