export enum CompanyStatusEnum {
  ACTIVE = 'Activa',
  INACTIVE = 'Inactiva',
  INCOMPLETE_RECORD = 'Registro incompleto'
}
