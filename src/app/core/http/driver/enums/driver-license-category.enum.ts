export enum DriverLicenseCategoryEnum {
  MOTORCYCLIST = 'M - Motociclista',
  PARTICULAR = 'P - Particular',
  PROFESSIONAL_A = 'A - Profesional',
  PROFESSIONAL_B = 'B - Profesional',
  PROFESSIONAL_C = 'C - Profesional',
  MOTORIST = 'T - Motorista',
}