export interface ReferenceContact {
  referenceContactId?: number;
  fullName: string;
  phone: string;
  address: string;
  deletedAt?: Date;
}