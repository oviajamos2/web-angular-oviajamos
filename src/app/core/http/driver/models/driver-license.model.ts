import { DriverLicenseCategoryEnum } from '../';
import { BloodTypeEnum } from '../';
import { GenderEnum } from '../';

export interface DriverLicense {
  driverLicenseId?: number;
  gender: GenderEnum;
  birthAt: Date;
  nationality: string;
  emitAt: Date;
  expireAt: Date;
  bloodType: BloodTypeEnum;
  useGlasses: boolean;
  useHeadphones: boolean;
  licenseNumber: number;
  category: DriverLicenseCategoryEnum;
}