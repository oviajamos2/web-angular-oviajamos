import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { Bus } from './';

@Injectable({
  providedIn: 'root'
})
export class BusService {
  resource = '/bus';
  recoveredBus$ = new Subject();
    
  constructor( private http: HttpClient ) {}

  create(data: Bus): Observable<Bus> {
    const href = `${this.resource}`;
    return this.http.post<Bus>(href, data);
  }

  patch(id: number, data: Bus): Observable<Bus> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Bus>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<Bus>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Bus>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<Bus> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Bus>(href);
  }

  delete(id: number): Observable<Bus> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Bus>(href);
  }

  recover(id: number): Observable<Bus> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Bus>(href);
  }
}