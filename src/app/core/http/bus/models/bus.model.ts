import { BusBrand } from './bus-brand.model';
import { BusPropietary } from './bus-propietary.model';
import { BusStatusEnum } from '../';
import { BusTypeEnum } from '../';
import { Driver } from 'src/app/core/http/driver';
import { SeatingScheme } from '../../seating-scheme';
import { Enterprise } from '../../enterprise';

export interface Bus {
  busId?: number;
  imageUrl: string;
  type: BusTypeEnum;
  busBrand?: BusBrand;
  licensePlate: string;
  seatingScheme: SeatingScheme;
  status: BusStatusEnum;
  busPropietary: BusPropietary;
  driver: Driver;
  company?: Enterprise;
  showDetail?: boolean;
}