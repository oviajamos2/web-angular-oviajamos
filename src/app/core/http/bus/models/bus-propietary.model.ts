export interface BusPropietary {
  busPropietaryId?: number;
  name: string;
  lastName: string;
  phone: number;
  address: string;
}