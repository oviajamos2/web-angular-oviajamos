export enum SeatingSchemeEnum {
  TEN = "10",
  TWELVE = "12",
  FOURTEEN = "14",
}