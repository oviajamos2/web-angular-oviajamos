export enum BusTypeEnum {
  TRUFI = 'Trufi',
  MINIVAN = 'Minivan',
  SURUBI = 'Surubi',
  BUS = 'Bus',
}