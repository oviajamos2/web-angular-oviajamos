import { PaymentMethod } from './payment-method.model';
import { Ticket } from '../../ticket';
import { Person } from '../../person';

export interface TicketBuyer {
  person: Person
  buyerName?: string;
  documentNumber?: string;
  ticketsPaid: Ticket[];
  paymentMethod?: PaymentMethod;
}
