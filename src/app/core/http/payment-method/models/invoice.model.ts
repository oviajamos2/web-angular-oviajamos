import { PaymentMethod } from "../";

export interface Invoice {
  externalId: number;
  rawData: string;
  invoiceDate: string;
  invoiceNumber: number;
  authorizationNumber: string;
  state: string;
  clientNit: string;
  businessName: string;
  total: string;
  subtotal: string;
  controlCode: string;
  literalAmount: string;
  qrControlCode?: string;
  paymentMethod?: PaymentMethod;
  invoiceId?: number;
}