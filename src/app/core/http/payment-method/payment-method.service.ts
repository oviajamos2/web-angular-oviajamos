import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentMethod } from './';
import { Observable } from 'rxjs';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';

@Injectable({
  providedIn: 'root'
})
export class PaymentMethodService {
  resource = '/paymentMethod';
    
  constructor( private http: HttpClient ) {}

  create(data: PaymentMethod): Observable<PaymentMethod> {
    const href = `${this.resource}`;
    return this.http.post<PaymentMethod>(href, data);
  }

  patch(id: number, data: PaymentMethod): Observable<PaymentMethod> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<PaymentMethod>(href, data);
  }

  getAll(): Observable<PaginationResponse<PaymentMethod>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<PaymentMethod>>(href, {});
  }

  get(id: number): Observable<PaymentMethod> {
    const href = `${this.resource}/${id}`;
    return this.http.get<PaymentMethod>(href);
  }

  getByTicketId(ticketId: number): Observable<PaymentMethod> {
    const href = `${this.resource}/ticket/${ticketId}`;
    return this.http.get<PaymentMethod>(href);
  }
}