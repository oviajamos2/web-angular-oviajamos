import { OfficeTypeEnum } from '..';
import { Dosage } from '../../dosage';

export interface Office {
  officeId?: number;
  imageUrl?: string;
  name: string;
  department: string;
  address: string;
  type: OfficeTypeEnum;
  dosage: Dosage;
  company?: number;
  companyId?: number;
  busStop?: number;
  busStopId?: number;
}
