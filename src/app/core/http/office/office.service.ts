import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Office } from './';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {
  resource = '/office';
  recoveredOffice$ = new Subject();

  constructor(private http: HttpClient) {
  }

  create(data: Office): Observable<Office> {
    const href = `${this.resource}`;
    return this.http.post<Office>(href, data);
  }

  patch(id: number, data: Office): Observable<Office> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Office>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<Office>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Office>>(href, {
      params: searchParams
    });
  }

  get(id: number): Observable<Office> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Office>(href);
  }

  update(id: number, data: Office): Observable<Office> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Office>(href, data);
  }

  delete(id: number): Observable<Office> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Office>(href);
  }

  recover(id: number): Observable<Office> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Office>(href);
  }
}
