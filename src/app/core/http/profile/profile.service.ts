import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Profile } from './';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  resource = '/profile';
  recoveredProfile$ = new Subject();

  constructor(private http: HttpClient) {
  }

  create(data: Profile): Observable<Profile> {
    const href = `${this.resource}`;
    return this.http.post<Profile>(href, data);
  }

  getAll(term?: string): Observable<PaginationResponse<Profile>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Profile>>(href, {
      params: { search: (term) ? term : '' }
    });
  }

  get(id: number): Observable<Profile> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Profile>(href);
  }

  update(id: number, data: Profile): Observable<Profile> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Profile>(href, data);
  }

  delete(id: number): Observable<Profile> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Profile>(href);
  }

  recover(id: number): Observable<Profile> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Profile>(href);
  }
}
