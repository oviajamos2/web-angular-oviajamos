export interface Profile {
    profileId?: number;
    name: string;
    address: string;
    imageUrl: string;
}
