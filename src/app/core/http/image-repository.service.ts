import { Inject, Injectable } from '@angular/core';
import {
  PutObjectCommand,
  PutObjectCommandOutput,
  S3Client,
} from '@aws-sdk/client-s3';
import { from, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Utils } from '../helpers/utils';

import {
  ImageRepositoryUploadOptions,
  IMAGE_REPOSITORY_TOKEN,
  RepositoryToken,
} from './image-repository.token';

@Injectable({
  providedIn: 'root',
})
export class ImageRepositoryService extends S3Client {
  constructor(
    @Inject(IMAGE_REPOSITORY_TOKEN) private repositoryToken: RepositoryToken
  ) {
    super(repositoryToken.config);
  }

  /**
   * This method is an alias of a `uploadFile` into promise
   * @param file the file to upload
   * @param partialOptions the value to overwrite default options
   * @return a string or null
   */
  uploadDigital(
    file: File,
    partialOptions?: Partial<ImageRepositoryUploadOptions>
  ): Promise<string> {
    return this.uploadFile(file, partialOptions).toPromise();
  }

  /**
   * Upload file to DigitalOcean Spaces
   * @param file the file to upload
   * @param partialOptions the value to overwrite default options
   * @return a string or null
   */
  uploadFile(
    file: File,
    partialOptions?: Partial<ImageRepositoryUploadOptions>
  ): Observable<string> {
    const options = { ...this.repositoryToken.options, ...partialOptions };
    const fileName = this.handleFileNames(file, options);
    const putCommand = new PutObjectCommand({
      Body: file,
      Bucket: options.bucket,
      Key: fileName,
      ACL: options.acl,
    });
    const location = this.generateLocation(fileName);

    return from(this.send(putCommand)).pipe(
      map(this.mapCommand(location)),
      catchError(() => of(null))
    );
  }

  private handleFileNames(
    file: File,
    options: Partial<ImageRepositoryUploadOptions>
  ): string {
    let fileName = file.name;
    if (options.uniqueName) {
      const uuid = Utils.uuidv4();
      const [originalFileName, ext] = file.name.split('.');
      const uniqueName = ext ? `${uuid}.${ext}` : `${uuid}`;
      fileName = uniqueName;
    }
    return fileName;
  }

  private generateLocation(fileName: string): string {
    return `${this.repositoryToken.endpoints.edge}/${encodeURIComponent(
      fileName
    )}`;
  }

  private mapCommand(location: string): (value: any) => string {
    return ({ $metadata }: PutObjectCommandOutput) =>
      $metadata?.httpStatusCode === 200 ? location : null;
  }
}
