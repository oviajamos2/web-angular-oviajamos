import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginationResponse } from 'src/app/core/services/base-service/pagination-response.interface';
import { EnterpriseSettings } from '.';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseSettingsService {
  resource = '/company-settings';

  constructor(
    private http: HttpClient
  ) {}

  create(data: EnterpriseSettings): Observable<EnterpriseSettings> {
    const href = `${this.resource}`;
    return this.http.post<EnterpriseSettings>(href, data);
  }

  patch(id: number, data: EnterpriseSettings): Observable<EnterpriseSettings> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<EnterpriseSettings>(href, data);
  }

  getAll(): Observable<PaginationResponse<EnterpriseSettings>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<EnterpriseSettings>>(href, {});
  }

  get(id: number): Observable<EnterpriseSettings> {
    const href = `${this.resource}/${id}`;
    return this.http.get<EnterpriseSettings>(href);
  }

  delete(id: number): Observable<EnterpriseSettings> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<EnterpriseSettings>(href);
  }

  recover(id: number): Observable<EnterpriseSettings> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<EnterpriseSettings>(href);
  }
}
