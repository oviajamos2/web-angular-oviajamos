import { Enterprise } from 'src/app/core/http/enterprise';
import { BankAccount } from '..';

export interface EnterpriseSettings {
  changeType?: number;
  contactPhoneNumber?: string;
  email?: string;
  reserveTimeLimit?: Date|string;
  daysToEnableOnlineSales?: number;
  timeToDisableSales?: Date|string;
  companySettingsId?: number;
  nit?: string;
  businessName?: string;
  company?: Enterprise;
  accounts?: BankAccount[];
}
