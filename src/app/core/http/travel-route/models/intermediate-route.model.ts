import { BusStop } from 'src/app/core/http/bus-stop';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

export interface IntermediateRoute {
  intermediateRouteId?: number;
  source: BusStop;
  destination: BusStop;
  timeEstimation: Date | string | NgbTimeStruct;
  deletedAt?: Date;
}
