export interface Event {
  eventId?: number;
  imageUrl?: string;
  date?: string;
  name?: string;
  description?: string;
}