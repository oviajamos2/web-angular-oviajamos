export enum DepartureTypeEnum {
  RECURRENT = 'Recurrente',
  EVENTUAL = 'Eventual',
  EXPRESS = 'Express',
}
