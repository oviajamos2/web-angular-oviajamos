export enum BusFloorTypeEnum {
  I = 'Primer piso',
  II = 'Segundo piso',
  III = 'Tercer piso',
}
