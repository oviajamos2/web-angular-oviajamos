import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { DepartureSearchParams } from './';
import { Departure } from './';

@Injectable({
  providedIn: 'root'
})
export class DepartureService {
  resource = '/departure';
  recoveredDeparture$ = new Subject();
    
  constructor( private http: HttpClient ) {}

  create(data: Departure): Observable<Departure> {
    const href = `${this.resource}`;
    return this.http.post<Departure>(href, data);
  }

  patch(id: number, data: Departure): Observable<Departure> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Departure>(href, data);
  }

  getAll(searchParams?: DepartureSearchParams): Observable<PaginationResponse<Departure>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Departure>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<Departure> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Departure>(href);
  }

  delete(id: number): Observable<Departure> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Departure>(href);
  }

  recover(id: number): Observable<Departure> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Departure>(href);
  }
}
