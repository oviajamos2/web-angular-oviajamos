import { Departure } from "src/app/core/http/departure";
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

export interface DepartureDate {
  departureDateId?: number;
  startDate?: Date;
  departureTime: Date | string | NgbTimeStruct;
  endDate?: Date;
  departure: Departure;
}
