import { Departure } from "src/app/core/http/departure";

export interface DepartureContractor {
  departureContractorId?: number;
  name: string;
  phoneNumber: string;
  departure: Departure;
}
