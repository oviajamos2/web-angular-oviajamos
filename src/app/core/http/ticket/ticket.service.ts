import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ticket } from './';
import { Observable } from 'rxjs';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  resource = '/ticket';
    
  constructor( private http: HttpClient ) {}

  create(data: Ticket): Observable<Ticket> {
    const href = `${this.resource}`;
    return this.http.post<Ticket>(href, data);
  }

  patch(id: number, data: Ticket): Observable<Ticket> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Ticket>(href, data);
  }

  getAll(): Observable<PaginationResponse<Ticket>> {
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Ticket>>(href, {});
  }

  get(id: number): Observable<Ticket> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Ticket>(href);
  }

  chagePosition(ticketSourceId: number, ticketDestinationId: number): Observable<Ticket> {
    const href = `${this.resource}/changePosition`;
    return this.http.post<Ticket>(href, {
      ticketSourceId,
      ticketDestinationId
    });
  }

  getQr(id: number): Observable<string> {
    const href = `/ticketBuyer/${id}/qrCode`;
    return this.http.get<string>(href);
  }
}