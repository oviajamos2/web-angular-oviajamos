
import { BusFloorTypeEnum } from '../../departure';
import { Departure } from '../../departure';
import { TicketBuyer } from '../../payment-method';
import { TicketStatusEnum } from '../';
import { Person } from 'src/app/core/http/person';

export interface Ticket {
  ticketId?: number;
  position: number;
  busFloor: BusFloorTypeEnum;
  status: TicketStatusEnum;
  person: Person;
  passengerName?: string;
  documentNumber?: string;
  departure: Departure;
  ticketBuyer?: TicketBuyer;
  phone?: number;
  amount?: number;
}
