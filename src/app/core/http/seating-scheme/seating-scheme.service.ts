import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { SeatingScheme } from './';

@Injectable({
  providedIn: 'root'
})
export class SeatingSchemeService {
  resource = '/seatingScheme';
  recoveredSeatingScheme$ = new Subject();
    
  constructor( private http: HttpClient ) {}

  create(data: SeatingScheme): Observable<SeatingScheme> {
    const href = `${this.resource}`;
    return this.http.post<SeatingScheme>(href, data);
  }

  patch(id: number, data: SeatingScheme): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<SeatingScheme>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<SeatingScheme>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<SeatingScheme>>(href, {
      params: searchParams
    });
  }

  get(id: number): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}`;
    return this.http.get<SeatingScheme>(href);
  }

  delete(id: number): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<SeatingScheme>(href);
  }

  recover(id: number): Observable<SeatingScheme> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<SeatingScheme>(href);
  }
}