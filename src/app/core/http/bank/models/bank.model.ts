export interface Bank {
  name: string;
  bankId?: number;
  showDetail?: boolean;
}
