import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { Bank } from './';

@Injectable({
  providedIn: 'root'
})
export class BankService {
  resource = '/bank';
  recoveredBank$ = new Subject();
    
  constructor( private http: HttpClient ) {}

  create(data: Bank): Observable<Bank> {
    const href = `${this.resource}`;
    return this.http.post<Bank>(href, data);
  }

  patch(id: number, data: Bank): Observable<Bank> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Bank>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<Bank>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Bank>>(href, { params: searchParams });
  }

  get(id: number): Observable<Bank> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Bank>(href);
  }

  delete(id: number): Observable<Bank> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Bank>(href);
  }

  recover(id: number): Observable<Bank> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Bank>(href);
  }
}
