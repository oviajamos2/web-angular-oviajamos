export enum ShippingStatusEnum {
  PAYED = 'Pagado',
  TO_BE_PAYED = 'Por pagar',
  QUOTE = 'Cotizacion',
}
