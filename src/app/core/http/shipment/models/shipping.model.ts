import { User } from '../../user/models/user.model';
import { ShippingStatusEnum } from '../enums/shipping-status.enum';
import { Package } from './package.model';
import { Enterprise } from '../../enterprise/models/enterprise.model';
import { Person } from '../../person/models/person.model';

export interface Shipping {
  senderId?: number;
  receiverId?: number;
  originOfficeId: number;
  destinationOfficeId: number;
  packages?: Package[];
  totalPrice: number;
  comment: string;
  status: ShippingStatusEnum;
  shippingId?: number;
  sellerId?: number;
  seller?: User;
  companyId?: number;
  company?: Enterprise;
  sender?: Person;
  receiver?: Person;
  checked?: boolean;
}
