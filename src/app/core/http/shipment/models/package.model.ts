import { Shipping } from "./shipping.model";

export interface Package {
  quantity: number;
  description: string;
  isFragile: boolean;
  weight: number;
  price: number;
  shipping?: Shipping;
}
