import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ITEMS_PER_PAGE } from 'src/app/shared/constants';
import { PaginationResponse } from '../../services/base-service/pagination-response.interface';
import { Shipping } from './models/shipping.model';

@Injectable({
  providedIn: 'root'
})
export class ShipmentService {
  
  resource = '/shipping';
    
  constructor( private http: HttpClient ) {}

  create(data: Shipping): Observable<Shipping> {
    const href = `${this.resource}`;
    return this.http.post<Shipping>(href, data);
  }

  patch(id: number, data: Shipping): Observable<Shipping> {
    const href = `${this.resource}/${id}`;
    return this.http.patch<Shipping>(href, data);
  }

  getAll(searchParams?: any, unpaginated: boolean = false): Observable<PaginationResponse<Shipping>> {
    if(!searchParams.page) searchParams.page = 1;
    searchParams.limit = ITEMS_PER_PAGE;
    if(unpaginated) {
      delete searchParams.page;
      delete searchParams.limit;
    }
    const href = `${this.resource}`;
    return this.http.get<PaginationResponse<Shipping>>(href, {
      params: <any>searchParams
    });
  }

  get(id: number): Observable<Shipping> {
    const href = `${this.resource}/${id}`;
    return this.http.get<Shipping>(href);
  }

  delete(id: number): Observable<Shipping> {
    const href = `${this.resource}/${id}`;
    return this.http.delete<Shipping>(href);
  }

  recover(id: number): Observable<Shipping> {
    const href = `${this.resource}/${id}/recover`;
    return this.http.delete<Shipping>(href);
  }
}