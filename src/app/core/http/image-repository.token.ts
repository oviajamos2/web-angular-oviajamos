import { InjectionToken } from '@angular/core';
import { S3ClientConfig } from '@aws-sdk/client-s3';

export interface ImageRepositoryUploadOptions {
  bucket: string;
  uniqueName?: boolean;
  acl?: string
}

export interface RepositoryToken {
  config: S3ClientConfig;
  endpoints: {
    origin: string;
    edge?: string;
    subdomain?: string;
  };
  options: ImageRepositoryUploadOptions
}

export const IMAGE_REPOSITORY_TOKEN = new InjectionToken<RepositoryToken>('IMAGE_REPOSITORY_TOKEN');
