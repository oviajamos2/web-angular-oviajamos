import { AuxiliarContact } from '../../user';

export class TicketHolder {
  constructor(
    public id: number,
    public photo: string,
    public names: string,
    public secondNames: string,
    public docType: string,
    public docNumber: string,
    public cellphone: number,
    public address: string,
    public countryPhoneCode?: string,
    public password?: string,
    public confirmPassword?: string,
    public contacts?: AuxiliarContact[],
    public showDetail?: boolean,
    public showMoreDetail?: boolean,
    public showOptions?: boolean
  ) {}
}