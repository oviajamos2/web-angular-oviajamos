import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { User } from './';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  resource = '/auth';
  resourceUser = '/user'

  constructor(private http: HttpClient) {}

  getProfile(): Observable<User> {
    const href = `${this.resource}`;
    return this.http.get<User>(href, {});
  }
  
  getUser(id:number): Observable<User> {
    const href = `${this.resourceUser}/${id}`;
    return this.http.get<User>(href, {});
  }
}
