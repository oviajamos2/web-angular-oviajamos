import { ReferenceContact } from './reference-contact.model';
import { DocumentTypeEnum } from 'src/app/core/http/user';
import { User } from '../../user';

export class OfficeAdmin {
  user: User;
  referenceContact: ReferenceContact[];
  documentType: DocumentTypeEnum;
  documentNumber: string;
  showOptions?: boolean;
  showDetail?: boolean;
  secretaryId?: number;
  showMoreDetail?: boolean;
}
