import { Office } from '../../office';


export interface Dosage {
  gloss: string;
  economicActivity: string;
  invoiceNumber: number;
  authorizationNumber: number;
  emissionTimeLimit: Date|string;
  dosageKey?: string;
  office: Office;
  dosageId?: number;
}
