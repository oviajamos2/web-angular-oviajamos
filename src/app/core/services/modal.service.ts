import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor( private modalService2: NgbModal ) {}

  public open(component, classes?: string, backdrop: boolean | 'static' = true): NgbModalRef {
    return this.modalService2.open(component, {
      backdrop,
      keyboard: true,
      backdropClass: '',
      modalDialogClass: `modal-dialog-centered ${ classes }`,
      animation: true,
      scrollable: false,
    });
  }
}
