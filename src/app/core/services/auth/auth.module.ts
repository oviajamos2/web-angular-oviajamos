import { ModuleWithProviders, NgModule } from '@angular/core';
import {
  AngularFireModule,
  FIREBASE_OPTIONS,
  FIREBASE_APP_NAME,
} from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RouterModule } from '@angular/router';
import { AuthService } from './auth.service';
import { AuthOptions, AUTH_CONFIG } from './auth-options.token';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FirebaseTokenInterceptor } from './auth.interceptor';

@NgModule({
  imports: [AngularFireModule, AngularFireAuthModule, RouterModule],
})
export class AuthModule {
  static forRoot(options: AuthOptions): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        { provide: AUTH_CONFIG, useValue: options.authConfig },
        { provide: FIREBASE_OPTIONS, useValue: options.firebaseOptions },
        { provide: FIREBASE_APP_NAME, useValue: options.nameOrConfig },
        { provide: HTTP_INTERCEPTORS, useClass: FirebaseTokenInterceptor, multi: true },
      ],
    };
  }
}
