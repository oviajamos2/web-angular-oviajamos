import { Inject, Injectable, Optional } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { catchError, mergeMap } from 'rxjs/operators';
import { AuthConfig, AUTH_CONFIG } from './auth-options.token';

@Injectable()
export class FirebaseTokenInterceptor implements HttpInterceptor {
  constructor(
    private afAuth: AngularFireAuth,
    @Optional() @Inject(AUTH_CONFIG) private authConfig?: AuthConfig
  ) {}

  isSkipUrl(url: string): boolean {
    if (url.indexOf('oauthCallback') > -1) {
      return true;
    }
    if (this.authConfig?.ignorePath) {
      let mathIgnorePath: boolean;
      for (const path of this.authConfig.ignorePath) {
        if (url.indexOf(path) > -1) {
          mathIgnorePath = true;
          break;
        }
      }
      return mathIgnorePath;
    }
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.afAuth.idToken.pipe(
      mergeMap((token: any) => {
        if (this.isSkipUrl(request.url)) {
          return next.handle(request);
        }
        if (token) {
          request = request.clone({
            setHeaders: { Authorization: `Bearer ${token}` },
          });
        }
        return next.handle(request);
      }),
    );
  }
}
