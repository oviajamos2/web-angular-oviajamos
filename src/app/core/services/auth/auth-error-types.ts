export interface AuthError {
  code: string;
  message: string;
}

export const AuthErrorTypes = {
  wrongPassword: 'auth/wrong-password',
  tooManyRequests: 'auth/too-many-requests',
  disabledOvjAccount: 'auth/disabledOvjAccount'
};