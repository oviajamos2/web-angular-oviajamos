import { InjectionToken } from '@angular/core';
import { FirebaseOptions, FirebaseAppConfig } from '@angular/fire';

export interface AuthConfig {
  defaultPath?: string;
  ignorePath?: string[];
}

export interface AuthOptions {
  authConfig?: AuthConfig;
  firebaseOptions: FirebaseOptions;
  nameOrConfig?: string | FirebaseAppConfig;
}

export const AUTH_CONFIG = new InjectionToken<AuthConfig>('AuthConfig');
