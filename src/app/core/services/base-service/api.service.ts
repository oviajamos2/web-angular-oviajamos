import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiServiceConfigToken, ApiServiceConfig } from './api-service-config';
import { ApiServiceOptions } from './api-service-options';
import { ErrorResponseDto } from './dto/error-response.dto';
import { DeleteItemResponseDto } from './dto/delete-item-response.dto';

const AUTH_TOKEN_KEY = 'auth_token';
const AUTH_REFRESH_TOKEN_KEY = 'auth_refresh_token';

@Injectable()
export class ApiService {
	baseUrl: string;
	authHeader = 'Authentication';
	authToken: string;
	authTokenHeader: string;
	refreshAuthToken: string;
	defaultQueryParams = {};

	constructor(
		@Inject(ApiServiceConfigToken) private config: ApiServiceConfig,
		public http: HttpClient
	) {
		this.initConfig();
		this.initAuth();
	}

	private initConfig(): void {
		this.baseUrl = this.config.baseUrl;
		this.config.authPrefix = this.config.authPrefix || 'Bearer';
		this.authHeader = this.config.authHeader || this.authHeader;
	}

	private initAuth(): void {
		const storedAuthToken = localStorage.getItem(AUTH_TOKEN_KEY);
		const storedAuthRefreshToken = localStorage.getItem(AUTH_REFRESH_TOKEN_KEY);
		if (storedAuthToken) {
			this.setAuthToken(storedAuthToken, storedAuthRefreshToken);
		}
	}

	public removeAuth(): void {
		localStorage.removeItem(AUTH_TOKEN_KEY);
		localStorage.removeItem(AUTH_REFRESH_TOKEN_KEY);
	}

	public setAuthToken(authToken: string, refreshAuthToken?: string): void {
		this.authToken = authToken || null;
		this.refreshAuthToken = refreshAuthToken || null;

		let newAuthTokenHeader;
		if (authToken) {
			newAuthTokenHeader = this.getTokenResource(authToken);
		} else {
			newAuthTokenHeader = null;
		}
		this.authTokenHeader = newAuthTokenHeader;

		this.storeAuth();
	}

	private storeAuth(): void {
		localStorage.setItem(AUTH_TOKEN_KEY, this.authToken);
		localStorage.setItem(AUTH_REFRESH_TOKEN_KEY, this.refreshAuthToken);
	}

	public getTokenResource(authToken: string): string {
		return `${this.config.authPrefix} ${authToken}`;
	}

	public getRefreshToken(): string {
		return this.refreshAuthToken;
	}

	public getItem<T>(
		resource: string,
		options: ApiServiceOptions = {}
	): Observable<T> {
		const resourceUrl = this.createResourceUrl(resource);
		return this.http.get<T>(resourceUrl, {
			headers: this.getHeaders(options),
			params: this.parseQueryParams(options),
		});
	}

	public getCollection<T>(
		resource: string,
		options: ApiServiceOptions = {}
	): Observable<T[] | ErrorResponseDto> {
		const resourceUrl = this.createResourceUrl(resource);
		return this.http.get<T[]>(resourceUrl, {
			headers: this.getHeaders(options),
			params: this.parseQueryParams(options),
		});
	}

	public createItem<T>(
		resource: string,
		body: any = {},
		options: ApiServiceOptions = {}
	): Observable<T> {
		const resourceUrl = this.createResourceUrl(resource);
		return this.http.post<T>(resourceUrl, body, {
			headers: this.getHeaders(options),
			params: this.parseQueryParams(options),
		});
	}

	public updateItem<T>(
		resource: string,
		body: any = {},
		options: ApiServiceOptions = {}
	): Observable<T> {
		const resourceUrl = this.createResourceUrl(resource);
		return this.http.patch<T>(resourceUrl, body, {
			headers: this.getHeaders(options),
			params: this.parseQueryParams(options),
		});
	}

	public deleteItem<T>(
		resource: string,
		options: ApiServiceOptions = {}
	): Observable<DeleteItemResponseDto | ErrorResponseDto> {
		const resourceUrl = this.createResourceUrl(resource);
		return this.http.delete<DeleteItemResponseDto | ErrorResponseDto>(
			resourceUrl,
			{
				headers: this.getHeaders(options),
				params: this.parseQueryParams(options),
			}
		);
	}

	private createResourceUrl(resource: string): string {
		return `${this.baseUrl}/${resource}`;
	}

	public getHeaders(options: any): any {
		const headers = {
			'Content-Type': 'application/json',
			Accept: 'application/json',
		};
		if (options.isRefreshToken){
			return new HttpHeaders(headers);
		}
		this.handleAuthentication(headers);
		return new HttpHeaders(headers);
	}

	private handleAuthentication(headers: any): void {
		if (this.authTokenHeader) {
			headers[this.authHeader] = this.authTokenHeader;
		}
	}

	private parseQueryParams(options): any {
		const { queryParams } = options;
		return Object.assign({}, this.defaultQueryParams, queryParams || {});
	}
}