import { Observable, of } from 'rxjs';
import { PaginationMeta, PaginationResponse } from './pagination-response.interface';
import { CrudServiceBase } from './crud-service-base';
import { ApiServiceOptions } from './api-service-options';
import { map } from 'rxjs/operators';

export interface PaginationFeature<ItemType, Service> {
	nextPage: number;
	service: Service;
	canNextPage: () => boolean;
	resetPagination: (limit: number) => void;
	updateMetadata: (meta: PaginationMeta) => void;
	getNextItems(options: ApiServiceOptions): Observable<ItemType[]>;
}

export class PaginationHelper <Service extends CrudServiceBase<ItemType> = any, ItemType = any>
	implements PaginationMeta, PaginationFeature<ItemType, Service>
{
	currentPage: number;
	itemCount: number;
	itemsPerPage: number;
	totalItems: number;
	totalPages: number;
	private limit: number;
	private emptyItems: Observable<ItemType[]>;

	constructor(public service: Service, itemsPerPage?: number) {
		this.initPagination(itemsPerPage);
	}

	private initPagination(itemsPerPage: number = 50): void {
		this.limit = itemsPerPage;
		this.currentPage = 1;
		this.itemsPerPage = itemsPerPage;
		this.totalItems = null;
		this.emptyItems = of<ItemType[]>([]);
	}

	resetPagination(): void {
		this.currentPage = 1;
		this.itemsPerPage = this.limit;
		this.totalItems = null;
	}

	get nextPage(): number {
		return this.currentPage + 1;
	}

	canNextPage(): boolean {
		return this.nextPage <= this.totalPages || this.totalItems === null;
	}

	updateMetadata({currentPage, itemCount, itemsPerPage, totalItems, totalPages}: PaginationMeta): void {
		this.currentPage = currentPage;
		this.itemCount = itemCount;
		this.itemsPerPage = itemsPerPage;
		this.totalItems = totalItems;
		this.totalPages = totalPages;
	}

	getNextItems(options: ApiServiceOptions = {}): Observable<ItemType[]> {
		const { queryParams } = options;
		options.queryParams = {
			...queryParams,
			page: this.totalItems ? this.nextPage : this.currentPage,
			limit: this.itemsPerPage,
		};
		return this.canNextPage()
			? this.service.getMany(options, this)
					.pipe(
						map(({ items }: PaginationResponse<ItemType>) => items),
					)
			: this.emptyItems;
	}
}

