import { ApiQueryParams } from './api-query-params';

export interface ApiServiceOptions {
	queryParams?: ApiQueryParams;
	isRefreshToken?: boolean;
}
