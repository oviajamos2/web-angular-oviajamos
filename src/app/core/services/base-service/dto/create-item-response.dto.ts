export interface CreateItemResponseDto {
	id: string;
	message: string;
	code: number;
}
