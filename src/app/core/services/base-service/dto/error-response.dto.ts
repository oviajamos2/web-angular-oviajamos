export interface ErrorResponseDto {
	statusCode: number;
	message: string | string[];
	error: string;
}
