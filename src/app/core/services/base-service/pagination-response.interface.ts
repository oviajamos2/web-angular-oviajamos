export interface PaginationMeta {
	currentPage: number;
	itemCount: number;
	itemsPerPage: number;
	totalItems: number;
	totalPages: number;
}

interface PaginationLinks {
	first: string;
	last: string;
	next: string;
	previous: string;
}

export interface PaginationResponse<T> {
	items: T[];
	meta: PaginationMeta;
	links: PaginationLinks;
}
