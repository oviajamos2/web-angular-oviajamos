export interface NotificationData {
  type: 'delete'|'save'|'conError'|'serverError'|'recovered'|'alert';
  color: 'success'|'info'|'warning'|'error'|'default';
  objectType?: 'office'|'enterprise'|'officeAdmin'|'bus'|'busStop'|'travelRoute'|'driver'|'ticketHolder'|'departure'|'seatingScheme'|'bank';
  objectId?: number;
  id?: string;
  recovered?: boolean;
  message?: string;
  link?: [string, string];
};