import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { NotificationData } from 'src/app/core/services/notification/notificationData.interface';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public notifications$ = new Subject<NotificationData>();

  constructor() {}

  notify(notificationData: NotificationData) {
    this.notifications$.next(notificationData);
  }
}