import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

import { Departure, DepartureSearchParams, DepartureService } from '../../http/departure';
import { Ticket } from '../../http/ticket';
import { LocalStorageService } from '../local-storage.service';
import { PaymentMethod, PaymentMethodService, PaymentMethodTypeEnum } from '../../http/payment-method';
import { TicketStatusEnum } from 'src/app/core/http/ticket';
import { PaymentMethodKindEnum } from 'src/app/core/http/payment-method';

type PurchaseData = {
  departureSearchParams: DepartureSearchParams;
  selectedDeparture: Departure;
  step: number;
  ticketPurchaseForm: PaymentMethod;
}
@Injectable({
  providedIn: 'root'
})
export class TicketPurchaseService {

  departureSearchParams: DepartureSearchParams;
  filterDeparture$ = new Subject<DepartureSearchParams>();
  filteredDepartures: Departure[] = [];
  loading: boolean = false;
  loadingSteperContent: boolean = false;
  paymentError: boolean = false;
  paymentMethod: PaymentMethod;
  selectedDeparture: Departure;
  step: number = 1;
  steper$ = new Subject();
  ticketPurchaseGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private departureService: DepartureService,
    private localStorageService: LocalStorageService,
    private paymentMethodService: PaymentMethodService,
    private router: Router
  ) {
    this.buildForm();
    this.loadPurchaseDataFromStorage();
    this.initSteperSubs();
  }

  get ticketPurchaseForm() {
    return this.ticketPurchaseGroup.controls;
  }

  get departureForm() {
    return (<FormGroup>this.ticketPurchaseForm.departure).controls;
  }
  
  get ticketBuyerGroup():FormGroup {
    return <FormGroup>this.ticketPurchaseGroup.controls.ticketBuyer;
  }

  get ticketsPaidArray(): FormArray {
    return (<FormArray>(<FormGroup> this.ticketPurchaseGroup.controls.ticketBuyer).controls.ticketsPaid);
  }

  get selectedTickets(): number[] {
    let response = [];
    this.ticketsPaidArray.value.forEach( (ticket: Ticket) => {
      response.push(ticket.ticketId);
    });
    return response;
  }

  get selectedPositions(): number[] {
    let response = [];
    this.ticketsPaidArray.value.forEach( (ticket: Ticket) => {
      response.push(ticket.position);
    });
    return response;
  }

  buildForm() {
    this.ticketPurchaseGroup = this.formBuilder.group({
      departure: this.formBuilder.group({
        departureId: ['']
      }),
      type: [PaymentMethodTypeEnum.CARD_PAYMENT],
      amountPaid: [],
      description: [''],
      kind: [PaymentMethodKindEnum.QUOTABLE],
      ticketBuyer: this.formBuilder.group({
        person: this.formBuilder.group({
          name: ['', [Validators.required]],
          documentNumber: ['', [Validators.required]],
          phone: ['', [Validators.required]],
          email: ['', [Validators.email]],
        }),
        ticketsPaid: this.formBuilder.array([])
      }),
    });
  }

  addTicketForm(ticket: Ticket) {
    if(!ticket) return;
    let newPassengerDataForm = this.formBuilder.group({
      ticketId: [ticket.ticketId],
      person: this.formBuilder.group({
        name: ['', [Validators.required]],
        documentNumber: ['', [Validators.required]],
        phone: [''],
      }),
      position: [ticket.position],
      status: [TicketStatusEnum.SOLD],
      amount: [this.selectedDeparture.price],
    });
    this.ticketsPaidArray.push(newPassengerDataForm);
    this.sortTicketsPaid();
  }

  submitPayment() {
    this.departureForm.departureId.setValue(this.selectedDeparture.departureId);
    this.paymentMethodService.create(this.ticketPurchaseGroup.value).subscribe( (response: PaymentMethod) => {
      this.paymentMethod = response;
      this.resetPaymentData();
      this.clearPurchaseDataFromStorage();
      this.steper$.next(true);
      this.loading = false;
    }, (error) => {
      console.warn(error)
      this.paymentError = true;
      this.loading = false;
    })
  }

  initSteperSubs() {
    this.steper$.subscribe( value => {
      if( value ) {
        this.step++;
      } else {
        this.step--;
      }
      switch(this.step) {
        case 1:
          this.router.navigateByUrl('/client/home/compra-boleto/1');
          break;
        case 2:
          this.router.navigateByUrl('/client/home/compra-boleto/2');
          break;
        case 3:
          this.router.navigateByUrl('/client/home/compra-boleto/3');
          break;
        case 4:
          this.router.navigateByUrl('/client/home/compra-boleto/4');
          break;
        case 5:
          this.router.navigateByUrl('/client/home/compra-boleto/5');
          break;
        case 6:
          this.router.navigateByUrl('/client/home/compra-boleto/6');
          break;
        case 7:
          this.router.navigateByUrl('/client/home/compra-boleto/7');
          break;
      }
    });
  }

  reloadSelectedDeparture() {
    this.getDeparture().then( (response: Departure) => {
      if(response) {
        this.selectedDeparture = response;
        this.loadingSteperContent = false;
      } else {
        this.router.navigateByUrl('/client/home/compra-boleto/1').then(() => {
          this.loadingSteperContent = false;
        });
      }
    }).catch( (error) => {
      console.warn(error)
      this.loading = false;
    });
  }

  async getDeparture(): Promise<Departure> {
    if(!this.selectedDeparture) return null;
    return this.departureService.get(this.selectedDeparture.departureId).pipe(
      take(1)
    ).toPromise();
  }

  resetPaymentData() {
    this.buildForm();
  }

  savePurchaseDataOnStorage() {
    let purchaseData: PurchaseData = {
      departureSearchParams: this.departureSearchParams,
      selectedDeparture: this.selectedDeparture,
      step: this.step,
      ticketPurchaseForm: this.ticketPurchaseGroup.value
    };
    this.localStorageService.setItem('purchaseData', purchaseData);
  }

  loadPurchaseDataFromStorage() {
    if(!this.localStorageService.getItem<PurchaseData>('purchaseData')) return;
    let loadedPurchaseData = this.localStorageService.getItem<PurchaseData>('purchaseData');
    this.departureSearchParams = loadedPurchaseData.departureSearchParams;
    this.selectedDeparture = loadedPurchaseData.selectedDeparture;
    this.step = loadedPurchaseData.step;
    this.patchPurchaseForm(loadedPurchaseData.ticketPurchaseForm);
    this.router.navigateByUrl(`/client/home/compra-boleto/${this.step}`);
  }

  clearPurchaseDataFromStorage() {
    this.localStorageService.removeItem('purchaseData');
  }

  patchPurchaseForm(paymentMethod: PaymentMethod) {
    paymentMethod.ticketBuyer.ticketsPaid.forEach( (ticket: Ticket) => {
      let newTicketGroup = this.formBuilder.group({
        ticketId: [ticket.ticketId],
        person: this.formBuilder.group({
          name: [ticket.passengerName],
          documentNumber: [ticket.documentNumber],
          phone: ['']
        }),
        position: [ticket.position],
        status: [ticket.status],
        amount: [ticket.amount]
      });
      (<FormArray>this.ticketBuyerGroup.controls.ticketsPaid).push(newTicketGroup);
    });
    this.ticketPurchaseGroup.patchValue(paymentMethod);
  }

  patchForm(paymentMethod: PaymentMethod) {
    this.ticketPurchaseGroup.addControl('paymentMethodId',this.formBuilder.control(paymentMethod.paymentMethodId));
    this.ticketPurchaseGroup.patchValue(paymentMethod);
    paymentMethod.ticketBuyer.ticketsPaid.forEach( (ticket: Ticket)  => {
      let newPassengerDataForm = this.formBuilder.group({
        ticketId: [ticket.ticketId],
        person: this.formBuilder.group({
          name: [ticket.passengerName, [Validators.required]],
          documentNumber: [ticket.documentNumber, [Validators.required]],
          phone: [ticket.phone, Validators.required],
        }),
        position: [ticket.position],
        status: [TicketStatusEnum.SOLD],
        amount: [ticket.amount, Validators.required],
      });
      this.selectedTickets.push(ticket.ticketId);
      this.ticketsPaidArray.push(newPassengerDataForm);
      this.sortTicketsPaid();
    });
  }

  sortTicketsPaid() {
    let ticketsPaid: Ticket[] = [...this.ticketsPaidArray.value];
    if (ticketsPaid.length < 2) {
      return;
    }
    for (let i = 0; i < ticketsPaid.length; i++) {
      for (let j = 0; j < (ticketsPaid.length - i - 1); j++) {
        if (ticketsPaid[j].position > ticketsPaid[j + 1].position) {
          let tmp = {...ticketsPaid[j]};
          ticketsPaid[j] = {...ticketsPaid[j + 1]};
          ticketsPaid[j + 1] = {...tmp};
        }
      }
    }
    this.ticketsPaidArray.setValue(ticketsPaid);
  }

  backToHome() {
    this.step = 1;
    this.selectedDeparture = null;
    this.paymentMethod = null;
    this.router.navigateByUrl('/client/home');
  }
}
