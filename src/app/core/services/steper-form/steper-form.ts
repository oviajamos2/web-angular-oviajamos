export interface SteperForm {
  currentStep: number;
  completed: boolean;
  steps: number;
  lastCreatedStep?: number;
  progressSteps: ItemStep[]
}

interface ItemStep {
  stepName: string;
}