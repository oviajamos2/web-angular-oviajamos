import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { LocalStorageService } from '../local-storage.service';
import { SteperForm } from './steper-form';

type SteperData = {
  steper: SteperForm;
  stepObjects: any[];
  createdObject: any;
};

@Injectable({
  providedIn: 'root'
})
export class SteperFormService {

  saveStep$ = new Subject;
  nextStep$ = new Subject;
  steper: SteperForm;
  submitting: boolean = false;
  stepObjects = [];
  createdObject = null;

  get currentStep(): number {
    return this.steper.currentStep;
  }
  
  set currentStep(step: number) {
    if(!this.steper) return;
    this.steper.currentStep = step;
  }

  get completed(): boolean {
    return this.steper.completed;
  }

  get steps(): number {
    return this.steper.steps;
  }

  set steperValue(steper: SteperForm) {
    this.steper = steper;
  }

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService
  ) {
    this.loadFromStorage();
  }

  back(baseRoute: string) {
    if(this.currentStep > 1) {
      this.router.navigateByUrl(`${ baseRoute }/${ this.currentStep - 1 }`)
    }
  }

  save() {
    this.saveStep$.next(true);
    
  }

  next(baseRoute: string) {
    if((this.currentStep + 1) <= this.steps) {
      this.router.navigateByUrl(`${ baseRoute }/${ this.currentStep + 1 }`)
    }
  }

  toHome(baseRoute: string) {
    this.router.navigateByUrl(`${ baseRoute }`);
  }

  saveOnStorage(object?: any) {
    this.stepObjects[this.steper.currentStep-1] = object;
    this.localStorageService.setItem('steperData', {
      steper: this.steper,
      stepObjects: this.stepObjects,
      createdObject: this.createdObject
    });
  }

  loadFromStorage() {
    const data = this.localStorageService.getItem<SteperData>('steperData');
    if(data) {
      this.steper = data.steper;
      this.stepObjects = data.stepObjects;
      this.createdObject = data.createdObject;
    }
  }

  resetSteper() {
    this.steper = null;
    this.submitting = false;
    this.stepObjects = [];
    this.createdObject = null;
    this.localStorageService.removeItem('steperData');
  }
}
