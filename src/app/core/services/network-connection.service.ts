import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NetworkConnectionService {

  public netStatus: string

  get isConnected(): boolean {
    if(this.netStatus === 'online') {
      return true;
    } else if(this.netStatus === 'offline') {
      return false;
    }
  }

  constructor() {
    this.netStatus = navigator.onLine ? 'online' : 'offline';
    this.initOfflineSubs();
    this.initOnlineSubs();
  }

  initOfflineSubs() {
    fromEvent(window, 'offline').pipe(
      debounceTime(100)
    ).subscribe((event: Event) => {
      this.netStatus = event.type;
    })
  }

  initOnlineSubs() {
    fromEvent(window, 'online').pipe(
      debounceTime(100)
    ).subscribe((event: Event) => {
      this.netStatus = event.type;
    })
  }
}
