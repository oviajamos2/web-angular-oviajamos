import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './shared/components/error-page/error-page.component';
import { Page404Component } from "./shared/components";
import { NotFoundPageGuard } from './core/guards/not-found-page.guard';


const routes: Routes = [
  {
    path: '', loadChildren: () => import('./modules/admin/admin.module')
      .then(m => m.AdminModule)
  },
  {
    path: 'client', loadChildren: () => import('./modules/client/client.module')
      .then(m => m.ClientModule)
  },
  {
    path: '404',
    component: ErrorPageComponent
  },
  {
    path: 'client-404',
    component: Page404Component
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
