import { firebaseCredential } from './firebase-staging';
import { bucket, doSpacesCredentials, doSpacesEndpoints } from './do-spaces';

export const environment = {
  production: true,
  apiURL: 'https://apiovj.ml/api',
  wsUrl: 'https://apiovj.ml',
  firebase: firebaseCredential,
  storage: 'https://apiovj.ml/api/images',
  doSpaces: {
    credentials: doSpacesCredentials,
    endpoints: doSpacesEndpoints,
    bucket,
  },
};
