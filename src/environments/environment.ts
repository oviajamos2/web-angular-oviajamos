// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { firebaseCredential } from "./firebase-staging";
import { bucket, doSpacesCredentials, doSpacesEndpoints } from "./do-spaces";

export const environment = {
  production: false,
  apiURL: 'http://localhost:3000/api/v1',
  storage: 'http://localhost:3000/api/v1/images',
  wsUrl: 'ws://localhost:3000',
  // apiURL: 'https://apidev.oviajamos.com/api/v1',
  // storage: 'https://apidev.oviajamos.com/api/v1/images',
  // wsUrl: 'ws://apidev.oviajamos.com'
  firebase: firebaseCredential,
  doSpaces: {
    credentials: doSpacesCredentials,
    endpoints: doSpacesEndpoints,
    bucket,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
